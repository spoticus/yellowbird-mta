/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package acceptance;

import me.yellowbird.mta.Domain;
import me.yellowbird.mta.protocol.LineDataException;
import org.junit.Test;
import testhelper.MultiMTATestHarness;
import testhelper.SendAMessageException;

import java.util.Scanner;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * A remote agent can relay a mail object through the local MTA to a remote MTA.
 *
 * <a href="http://jira.yellowbird.me/browse/MTA-7">MTA-7</a>
 */
public class MTA7Test {

    /**
     * Build two MTAs and show that one can send a message to the second.
     * @throws java.util.concurrent.TimeoutException failure.
     */
    @Test
    public void sendmessage_and_receive() throws TimeoutException {
        MultiMTATestHarness harness = new MultiMTATestHarness("a", "b");

        try {
            harness.sendToRcpt(new Domain("foreign.com"), "a", "message text\r\n", "a@b");

            MultiMTATestHarness.Message m = harness.waitForMessage("b");

            Scanner sc = new Scanner(m.getText());

            assertNotNull(sc.findWithinHorizon("message text\r\n", 0));
            assertTrue(m.isForwardPath("a@b"));
        } catch (LineDataException e) {
            throw new RuntimeException(e);
        } catch (SendAMessageException e) {
            throw new RuntimeException(e);
        } finally {
            harness.shutdown();
        }
    }
}
