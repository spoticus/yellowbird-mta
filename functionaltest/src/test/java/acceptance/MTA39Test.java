/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package acceptance;

import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.Domain;
import me.yellowbird.mta.MailParameter;
import me.yellowbird.mta.protocol.LineData;
import me.yellowbird.mta.protocol.LineDataException;
import me.yellowbird.mta.protocol.RCPTCommandReply;
import me.yellowbird.mta.transport.ForwardPathException;
import me.yellowbird.mta.transport.MessageIdentifier;
import me.yellowbird.mta.transport.RCPTListener;
import me.yellowbird.mta.transport.SinkTransport;
import org.junit.Ignore;
import org.junit.Test;
import testhelper.MultiMTATestHarness;
import testhelper.SendAMessageException;

import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

/**
 * A returned notification is sent for undeliverable mail.
 * <p/>
 * <a href="http://jira.yellowbird.me/browse/MTA-39">MTA-39</a>
 */
public class MTA39Test {
    /**
     * Reject all attempts to send a message.
     */
    static class RejectAllAttempts implements RCPTListener {

        @Override
        public SinkTransport doRCPT(MessageIdentifier msgid, AddrSpec addrSpec, ArrayList<MailParameter> mailparams) throws ForwardPathException {
            try {
                throw new ForwardPathException(new RCPTCommandReply(RCPTCommandReply.Code.E450, new LineData("temporary")));
            } catch (LineDataException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * After the giveup interval, the MTA will generate a return notification.
     * <p/>
     * A message is sent through an MTA to a second that tries to relay to a third, which continually
     * rejects the message, until the second gives up. A return notification is sent to the originator,
     * which happens to be an address for the first MTA. The first MTA receives the message and
     * delivers the return notification locally.
     * @throws java.util.concurrent.TimeoutException
     * @throws me.yellowbird.mta.protocol.LineDataException
     * @throws testhelper.SendAMessageException
     */
    @Test
    @Ignore
    public void return_notification_after_giveup() throws SendAMessageException, LineDataException, TimeoutException {
        MultiMTATestHarness harness = new MultiMTATestHarness();

        try {

            // the originating MTA
            harness.addMTA("originator.com");

            // a relay that has a zero retry interval and giveup
            harness.addMTA("middle.com", 0, 0);

            // a relay that temporary rejects the first relay then accepts the second one
            harness.addMTA("rejector.com", new RejectAllAttempts());

            // make originator route to rejector through middle
            harness.putMailExchangeOverride("originator.com", "rejector.com", "middle.com");

            // send message through first to third.
            harness.sendToRcpt(new Domain("foreign.com"), "originator.com", "message text\r\n", "ron@rejector.com");

            MultiMTATestHarness.Message m = harness.waitForMessage("originator.com");
        } finally {
            harness.shutdown();
        }
    }
}