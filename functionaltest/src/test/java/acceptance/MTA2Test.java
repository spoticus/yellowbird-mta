/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package acceptance;

import me.yellowbird.mta.Domain;
import me.yellowbird.mta.protocol.LineDataException;
import org.junit.Assert;
import org.junit.Test;
import testhelper.HexDump;
import testhelper.MultiMTATestHarness;
import testhelper.SendAMessageException;

import java.util.Scanner;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertTrue;

/**
 * Story Acceptance Test.
 * <ul>
 * <li>"3rd-party developer creates pluggin for local delivery of mail".
 * </ul>
 * <p>
 * Demonstrate that the system will attach a pluggin that can receive mail
 * and then send a message through a running system to see that it is delivered.
 */
public class MTA2Test {
    /**
     * Test that a message can be sent over tcp to a server and delivered to a local MDA.
     * <p>
     * This test is valid because MultiMTATestHarness implements a server with the whole
     * architecture, including the protocol stack and an MDA.
     *
     * @throws TimeoutException didn't work.
     */
    @Test
    public void local_delivery_logic() throws TimeoutException {
        MultiMTATestHarness harness = new MultiMTATestHarness("a");

        try {
            harness.sendToRcpt(new Domain("foreign.com"), "a", "message text\r\n", "a@a");

            MultiMTATestHarness.Message m = harness.waitForMessage("a");

            Scanner sc = new Scanner(m.getText());
            
            if (sc.findWithinHorizon("message text\r\n", 0) == null) {
                HexDump.hexDump(m.getLinesOfData(), System.out);
                Assert.fail("did not receive message that was sent");
            }

            assertTrue(m.isForwardPath("a@a"));
        } catch (LineDataException e) {
            throw new RuntimeException(e);
        } catch (SendAMessageException e) {
            throw new RuntimeException(e);
        } finally {
            harness.shutdown();
        }
    }
}
