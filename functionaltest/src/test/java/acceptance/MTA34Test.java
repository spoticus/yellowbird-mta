/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package acceptance;

import me.yellowbird.mta.brain.Brain;
import me.yellowbird.mta.brain.XMLConfiguration;
import me.yellowbird.mta.transport.EnvelopeBusListener;
import org.junit.Assert;
import org.junit.Test;
import testhelper.NullBrainStem;
import testhelper.OnThreadHelper;

import java.io.IOException;
import java.io.InputStream;

/**
 * Application can be configured by an xml file that has a xsd file defining it's structure.
 *
 * <a href="http://jira.yellowbird.me/browse/MTA-34">MTA-34</a>
 */
public class MTA34Test {
    //listener used in configuration to prove proper configuration of brain
    public static class Listener implements EnvelopeBusListener {}

    /**
     * Show that the application can be started with parameters from an xml file.
     * @throws java.io.IOException
     */
    @Test
    public void application_configured_by_xml() throws Throwable, IOException {
        // load a brain with the configuration, assuming that the brain uses it's configuation properly
        InputStream xml = F001_001_A.class.getResourceAsStream("MTA34.xml");
        XMLConfiguration c = new XMLConfiguration(xml);

        Brain brain;

        brain = new Brain(c, new NullBrainStem());

        OnThreadHelper brainthread;

        brainthread = new OnThreadHelper(brain, "The Brain");
        brainthread.start();

        try {
            Assert.assertEquals("EHLO domain did not load properly", "localdomain",  brain.getEHLODomain().toString());
        } finally {
            brainthread.shutdown(true);
            brainthread.sync();            
        }
    }
}
