/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package acceptance;

import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.Domain;
import me.yellowbird.mta.MailParameter;
import me.yellowbird.mta.protocol.LineData;
import me.yellowbird.mta.protocol.LineDataException;
import me.yellowbird.mta.protocol.RCPTCommandReply;
import me.yellowbird.mta.transport.ForwardPathException;
import me.yellowbird.mta.transport.MessageIdentifier;
import me.yellowbird.mta.transport.RCPTListener;
import me.yellowbird.mta.transport.SinkTransport;
import org.junit.Test;
import testhelper.MultiMTATestHarness;
import testhelper.SendAMessageException;

import java.util.ArrayList;
import java.util.concurrent.TimeoutException;

/**
 * A remote agent sends a mail object to the local MTA that cannot be delivered and continuously retries at a configured interval.
 * <p/>
 * <a href="http://jira.yellowbird.me/browse/MTA-38">MTA-38</a>
 */
public class MTA38Test {
    /**
     * Reject first attempt to send a message then allow subsequent request.
     */
    static class RejectFirstAttempt implements RCPTListener {
        private boolean attempted = false;

        @Override
        public SinkTransport doRCPT(MessageIdentifier msgid, AddrSpec addrSpec, ArrayList<MailParameter> mailparams) throws ForwardPathException {
            if (!attempted) {
                attempted = true;
                try {
                    throw new ForwardPathException(new RCPTCommandReply(RCPTCommandReply.Code.E450, new LineData("temporary")));
                } catch (LineDataException e) {
                    throw new RuntimeException(e);
                }
            }

            return null;
        }
    }

    /**
     * Show that an MTA will retry a message because of retry interval.
     *
     * @throws java.util.concurrent.TimeoutException
     *
     * @throws me.yellowbird.mta.protocol.LineDataException
     *
     * @throws testhelper.SendAMessageException
     *
     */
    @Test
    public void continous_retry_forward() throws SendAMessageException, LineDataException, TimeoutException {
        MultiMTATestHarness harness = new MultiMTATestHarness();

        try {

            // a relay that has a zero retry interval and a long giveup so to make sure it tries enough times for the second relay to accept
            harness.addMTA("a.com", 0, 10);

            // a relay that temporary rejects the first relay then accepts the second one
            harness.addMTA("b.com", new RejectFirstAttempt());

            // send message through first that should eventually be received because first will retry even though rejected on first attempt
            harness.sendToRcpt(new Domain("foreign.com"), "a.com", "message text\r\n", "bob@b.com");

            MultiMTATestHarness.Message m = harness.waitForMessage("b.com");
        } finally {
            harness.shutdown();
        }
    }
}
