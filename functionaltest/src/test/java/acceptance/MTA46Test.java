/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package acceptance;

import me.yellowbird.mta.Domain;
import me.yellowbird.mta.protocol.LineDataException;
import org.junit.Assert;
import org.junit.Test;
import testhelper.MultiMTATestHarness;
import testhelper.SendAMessageException;
import testhelper.UnexpectedReplyException;

/**
 * A remote agent attempts to relay a mail object and a forward path is rejected for unsupported domain.
 *
 * <a href="http://jira.yellowbird.me/browse/MTA-46">MTA-46</a>
 */
public class MTA46Test {
    /**
     * Build an MTA and send a message to it that for a domain that it does not
     * accept either as matching MDA nor for relay through spooler.
     */
    @Test
    public void relay_rejected() throws LineDataException, SendAMessageException {
        MultiMTATestHarness harness = new MultiMTATestHarness(false, "a.com");

        try {
            try {
                harness.sendToRcpt(new Domain("foreign.com"), "a.com", "message text\r\n",  "bob@b.com");
            } catch (LineDataException e) {
                throw e;
            } catch (UnexpectedReplyException e) {
                Assert.assertTrue("was expected 550", e.getReceived().startsWith("550"));
            } catch (SendAMessageException e) {
                throw e;
            }

        } finally {
            harness.shutdown();
        }

    }
}