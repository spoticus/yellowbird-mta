/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package acceptance;

import me.yellowbird.mta.Domain;
import org.junit.Assert;
import org.junit.Test;
import testhelper.HexDump;
import testhelper.MultiMTATestHarness;

import java.util.Scanner;
import java.util.regex.Pattern;

import static org.junit.Assert.assertTrue;

/**
 * A message received by the MTA is stamped with tracing information.
 *
 * <a href="http://jira.yellowbird.me/browse/MTA-48">MTA-48</a>
 */
public class MTA48Test {
    /**
     * Build two MTAs and relay a message through one to the other.
     * Verify that the message received by the second contains the
     * tracing information for both relays.
     */
    @Test
    public void relay_through_two_and_verify_tracedata()  {
        MultiMTATestHarness harness = new MultiMTATestHarness("a.com", "b.com");

        try {
            harness.sendToRcpt(new Domain("foreign.com"), "a.com", "message text\r\n",  "bob@b.com");

            MultiMTATestHarness.Message m = harness.waitForMessage("b.com");

            Scanner sc = new Scanner(m.getText());

            // first unfolded line should be path through last mta
            String found = sc.findWithinHorizon(Pattern.compile("Received: from a.com \\(\\[\\d*\\.\\d*\\.\\d*\\.\\d*\\]\\)\r\n *by b.com \\(\\[\\d*\\.\\d*\\.\\d*\\.\\d*\\]\\); \\w{3}, \\d{1,2} \\w{3} \\d{4} \\d{2}:\\d{2}:\\d{2} [+-]\\d{4}",Pattern.DOTALL), 0);

            if (found == null) {
                HexDump.hexDump(m.getLinesOfData(), System.out);
                Assert.fail("did not receive last mta traceline");
            }

            // second unfolded line should be path through first mta
            found = sc.findWithinHorizon(Pattern.compile("Received: from foreign.com \\(\\[\\d*\\.\\d*\\.\\d*\\.\\d*\\]\\)\r\n *by a.com \\(\\[\\d*\\.\\d*\\.\\d*\\.\\d*\\]\\); \\w{3}, \\d{1,2} \\w{3} \\d{4} \\d{2}:\\d{2}:\\d{2} [+-]\\d{4}",Pattern.DOTALL), 0);

            if (found == null) {
                HexDump.hexDump(m.getLinesOfData(), System.out);
                Assert.fail("did not receive first mta traceline");
            }

            // data should follow

            if (sc.findWithinHorizon("message text\r\n", 0) == null) {
                HexDump.hexDump(m.getLinesOfData(), System.out);
                Assert.fail("did not receive message");
            }

            assertTrue(m.isForwardPath("bob@b.com"));
        } catch (Throwable e) {
            throw new RuntimeException(e);
        } finally {
            harness.shutdown();
        }

    }
}
