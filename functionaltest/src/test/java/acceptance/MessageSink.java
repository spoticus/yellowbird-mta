/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package acceptance;

import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.MailParameter;
import me.yellowbird.mta.protocol.LineOfData;
import me.yellowbird.mta.transport.*;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Envelope Bus Listener that can receive messages for testing.
 * <p>
 * Can receive messages and reports the status of the process.
 */
public class MessageSink implements EnvelopeBusListenerFactory, RCPTListener {
    private static TreeMap<String, MessageSink>created = new TreeMap<String,MessageSink>();

    public static EnvelopeBusListener createEnvelopeBusListener(String id) {
        MessageSink ms = new MessageSink();

        if (id != null)
            created.put(id, ms);

        return ms;
    }

    TreeSet<Sink>sinks = new TreeSet<Sink>();

    public static MessageSink getMessageSink(String id) {
        return created.get(id);
    }

    public Sink[] getSinks() {
        return sinks.toArray(new Sink[sinks.size()]);
    }

    public class Sink implements SinkTransport {
        boolean prepareForDataCalled = false;
        ArrayList<byte[]> alldata = new ArrayList<byte[]>();
        boolean commitCalled = false;
        boolean cancelCalled = false;

        @Override
        public void prepareForData() throws MessageDataException {
            prepareForDataCalled = true;
        }

        @Override
        public void add(LineOfData data) throws MessageDataException {
            ByteBuffer bb = data.getByteBuffer();

            byte[] b = new byte[bb.remaining()];

            System.arraycopy(bb.array(), bb.arrayOffset(), b, 0, bb.remaining());

            alldata.add(b);
        }

        @Override
        public void commit() throws MessageDataException {
            commitCalled = true;
        }

        @Override
        public void cancel() {
            cancelCalled = true;
        }

        public boolean isPrepareForDataCalled() {
            return prepareForDataCalled;
        }

        public List<byte[]> getAllData() {
            return alldata;
        }

        public boolean isCommitCalled() {
            return commitCalled;
        }
    }

    @Override
    public SinkTransport doRCPT(MessageIdentifier msgid, AddrSpec addrSpec, ArrayList<MailParameter> mailparams) throws ForwardPathException {
        Sink s = new Sink();
        sinks.add(s);
        return s;
    }
}
