/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package acceptance;

import me.yellowbird.mta.brain.Brain;
import me.yellowbird.mta.brain.ConfigurationException;
import me.yellowbird.mta.brain.InboundConnectionConfiguration;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import testhelper.*;

import java.io.File;

/**
 * Story System stores spooled messages in location configured by administrator.
 *
 * <a href="http://www.yellowbird.me/wiki/x/BoAU">Acceptance</a>
 */
public class SpoolerDirectoryTest {

    /**
     * Stores messages in designated directory.
     * <p>
     * Send one message that is stored in a configured location and verify that the file is there.
     * @throws me.yellowbird.mta.brain.ConfigurationException bad brain config.
     * @throws testhelper.SendAMessageException could not send test message.
     */
    @Test
    @Ignore
    public void configure_spooler_directory() throws Throwable, ConfigurationException, SendAMessageException {
        NullConfiguration cfg = new NullConfiguration(
                new InboundConnectionConfiguration[]{new InboundConnectionConfiguration("127.0.0.100", 25000)},
                new String[]{".*"});

        final Brain brain = new Brain(
                cfg,
                new NullBrainStem()
                );

        OnThreadHelper tm = OnThreadHelper.start(brain, "brain");

        SendAMessage clientprotocol = new SendAMessage("127.0.0.100", 25000, "me@localhost", "you@localhost", new String[]{"line one.\r\n"});
        OnThreadHelper.start(clientprotocol, "client protocol").sync();

        tm.shutdown(true);
        tm.sync();

        File msgdir = new File(cfg.getMessageDirectory());

        File[] msgfiles = msgdir.listFiles();

        Assert.assertEquals("wrong number of files in message directory", 1, msgfiles.length);

        MessageScanner sc = new MessageScanner(msgfiles[0]);

        Assert.assertTrue("text not found in message", sc.containsText("line one."));
    }

}