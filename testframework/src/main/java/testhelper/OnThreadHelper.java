/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package testhelper;

import org.hamcrest.Description;
import org.jmock.api.Action;
import org.jmock.api.Invocation;

import java.util.concurrent.TimeoutException;


/**
 * Test a Runnable.
 * <p>
 * Execute a {@link Runnable} on a thread, monitoring any exceptions that occur and providing for waiting
 * for the thread to complete or cancelling the test thread after a period of time.
 */
public class OnThreadHelper implements Runnable, Thread.UncaughtExceptionHandler {
    /**
     * Start a new thread helper.
     * <p>
     * Create a new thread helper and call the start method.
     *
     * @param runnable the runnable object.
     * @param description a description associated with the thread.
     * @return return the created helper.
     */
    public static OnThreadHelper start(Runnable runnable, String description) {
        OnThreadHelper oth = new OnThreadHelper(runnable, description);
        oth.start();
        return oth;
    }

    private final Thread t;
    private Runnable run;
    private Throwable uncaughtexception;

    /*
     * success can be controlled externally by setting this flag to true then interrupting the thread.
     */
    private boolean normalshutdown = false; //the test is being shutdown normally, everything is ok

    /**
     * Create test for runnable class.
     *
     * @param totest the runnable to test.
     */
    public OnThreadHelper(Runnable totest) {
        this(totest, "OnThreadHelper:" + totest.toString());
    }

    /**
     * Create a test thread with a description for the thread.
     * 
     * @param totest runnable to test.
     * @param description a description for the thread.
     */
    public OnThreadHelper(Runnable totest, String description) {
        this.run = totest;
        t = new Thread(this, description);
        t.setUncaughtExceptionHandler(this);
    }

    public void run() {
        run.run();
//        synchronized (this) {
//            this.notify();
//        }
    }

    /**
     * Start the test thread.
     */
    public void start() {
        t.start();
    }

    public void sync() throws Throwable {
        sync(6000);//default 6sec
    }
    
    /**
     * Wait for the test thread to complete or cancel if times-out.
     * <p>
     * The method will return with error if the thread finishes within the timeout value
     * or if the {@link #interrupt} method is called. If a runtime exception ends the thread
     * this this exception is thrown. If the timeout expires a TimeoutException is thrown.
     * 
     * @param timeout number of milliseconds for wait
     * @throws TimeoutException thrown by this method when it has to forcefully terminate the test.
     * @throws Throwable any exceptions detected during execution of the test thread.
     */
    public void sync(int timeout) throws Throwable {
        try {
            t.join(timeout);

            if (t.isAlive())
                throw new TimeoutException();

            if (uncaughtexception != null)
                throw uncaughtexception;
        } catch(InterruptedException e) {
            if (!normalshutdown)
                throw new TimeoutException();
        }

    }

    public void uncaughtException(Thread t, Throwable e) {
        uncaughtexception = e;
    }

    public void shutdown(boolean normalshutdown) {
        this.normalshutdown = normalshutdown;
        t.interrupt();
    }
    
    /**
     * A jmock action that will interrupt and end the test thread.
     * <p>
     * This is useful for test where the class under test is blocking for continuous input, but the
     * test is complete. Or a case where a class under test has been determined to me failing and
     * needs to me stopped.
     *
     * @param normalshutdown true if this is a normal shutdown and not an error
     * @return A jmock action to perform the shutdown on the test thread.
     */
    public Action interrupt(final boolean normalshutdown) {
        return new Action() {

             public void describeTo(Description description) {
                 description.appendText("does interrupt for " + run.toString());
             }

             public Object invoke(Invocation invocation) throws Throwable {
                 synchronized (this) {
                     OnThreadHelper.this.normalshutdown = normalshutdown;
                     t.interrupt();
                 }

                 return null;
             }
         };

    }

    public Runnable getRunnable() {
        return run;
    }

}

