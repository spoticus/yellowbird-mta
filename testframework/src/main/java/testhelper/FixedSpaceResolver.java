/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package testhelper;

import org.xbill.DNS.Message;
import org.xbill.DNS.Resolver;
import org.xbill.DNS.ResolverListener;
import org.xbill.DNS.TSIG;

import java.io.IOException;
import java.util.List;

/**
 * A resolver for xbill DNS that has a very specific name space.
 * <p>
 * Only dns records explicity defined to this test resolver will me found
 * by a query.
 */
public class FixedSpaceResolver implements Resolver {
    @Override
    public void setPort(int port) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setTCP(boolean flag) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setIgnoreTruncation(boolean flag) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setEDNS(int level) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setEDNS(int level, int payloadSize, int flags, List options) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setTSIGKey(TSIG key) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setTimeout(int secs, int msecs) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setTimeout(int secs) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Message send(Message query) throws IOException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Object sendAsync(Message query, ResolverListener listener) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
