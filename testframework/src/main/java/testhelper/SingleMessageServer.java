/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package testhelper;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;

/**
 * A simple server that can receive one message at a time for testing.
 * <p/>
 * A server that can receive one message at a time and provides hooks for monitoring the progress and
 * results.
 */
public class SingleMessageServer implements Runnable {

    private static final Charset ASCII = Charset.forName("ASCII");

    ServerSocketChannel serverchannel;
    private SocketChannel socketChannel;

    private String savebuffer; //incoming data not yet consumed
    private ArrayList<String> message;

    public SingleMessageServer(String host, int port) throws IOException {

        InetSocketAddress serverAddress = new InetSocketAddress(InetAddress.getByName(host), port);

        serverchannel = ServerSocketChannel.open();

        serverchannel.socket().bind(serverAddress);
    }

    /**
     * Receive one message.
     */
    @Override
    public void run() {
        try {
            waitForEmail();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public String[] getMessage() {
        return message.toArray(new String[message.size()]);
    }
    
    private void waitForEmail() throws IOException, InterruptedException, SendAMessageException {
        //reset in case we are being re-used
        savebuffer = null;
        message = new ArrayList<String>();


        socketChannel = serverchannel.accept();

        send220();

        waitForHelo();

        send250();

        waitForMail();

        send250();//we'll take anything

        waitForRcpt();

        send250();//we'll take anything

        waitForData();

        send354();

        waitForMessage();

        send250();

        waitForQuit();

        if (!socketChannel.isConnected())
            return;

        synchronized (this) {
            wait(6000);
        }

        if (socketChannel.isConnected())
            throw new RuntimeException("remote never dropped connection");
    }

    private void send(String text) throws IOException {
        ByteBuffer data = ASCII.encode(text + "\r\n");

        socketChannel.write(data);
    }

    private void send220() throws IOException {
        send("220");
    }

    private void send250() throws IOException {
        send("250");
    }

    private void send354() throws IOException {
        send("354");
    }

    private String readLine() throws IOException {
        int index;

        do {
            ByteBuffer dst = ByteBuffer.allocate(1001);

            socketChannel.read(dst);

            dst.flip();

            CharBuffer cb = ASCII.decode(dst);

            if (savebuffer == null)
                savebuffer = cb.toString();
            else
                savebuffer += cb.toString();

            index = savebuffer.indexOf("\r\n");

        } while (index == -1);

        String r = savebuffer.substring(0, index);

        savebuffer = savebuffer.substring(index + 2, savebuffer.length());

        return r;
    }

    private void waitForCommand(String command) throws IOException, SendAMessageException {
        String s = readLine();

        if (!s.matches("(?i)" + command))
            throw new SendAMessageException("waited for command " + command + " but got " + s);
    }

    private void waitForHelo() throws IOException, SendAMessageException {
        waitForCommand("helo");
    }

    private void waitForMail() throws IOException, SendAMessageException {
        waitForCommand("mail");
    }

    private void waitForRcpt() throws IOException, SendAMessageException {
        waitForCommand("rcpt");
    }

    private void waitForData() throws IOException, SendAMessageException {
        waitForCommand("data");
    }

    private void waitForQuit() throws IOException, SendAMessageException {
        waitForCommand("quit");
    }

    private void waitForMessage() throws IOException {
        String s;

        while (!(s = readLine()).equals(".")) {
            if (s.startsWith(".."))
                s = s.substring(1);

            message.add(s);
        }
    }
}
