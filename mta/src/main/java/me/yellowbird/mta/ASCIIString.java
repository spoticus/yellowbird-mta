/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta;

import java.util.regex.Pattern;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharacterCodingException;

/**
 * An ASCII String
 * <p>
 * Type safety to guarantee that a string on contains the 128 ASCII characters allowed
 * for RFC2821 commands and responses.
 * <p>
 * Instances are immutable.
 */
public class ASCIIString {
    private static final Charset ASCII = Charset.forName("ASCII");
    private static final Pattern ASCII_PATTERN = Pattern.compile("\\p{ASCII}*");

    /*
     * This class is immutable, so a reference to a string is shared between
     * here and users of this class.
     */
    private String text;

    /**
     * Construct from a java String
     *
     * @param string A string that will become this ASCIIString
     * @throws me.yellowbird.mta.ASCIIException If the string contains non-ASCII characters
     */
    public ASCIIString(String string) throws ASCIIException {
        if (!ASCII_PATTERN.matcher(string).matches())
            throw new ASCIIException("contains non-ascii characters", string);

        text = string;
    }

    /**
     * Construct from another ASCIIString
     * <p>
     * Something like a copy-constructor
     * @param asciistring ASCIIString used as source for this instance
     */
    public ASCIIString(ASCIIString asciistring) {
        text = asciistring.text;
    }

    /**
     * Construct by concatenating an ascii string with a java string
     * @param asciistring left part of new ascii string
     * @param string right part of new ascii string
     */
    public ASCIIString(ASCIIString asciistring, String string) {
        text = asciistring.text + string;
    }

    /**
     * Construct by concatenating two ascii string
     * @param leftasciistring left side of new ascii string
     * @param rightasciistring right side of new ascii string
     */
    public ASCIIString(ASCIIString leftasciistring, ASCIIString rightasciistring) {
        text = leftasciistring.text + rightasciistring.text;
    }

    /**
     * Concatenate this ASCII string with another
     * @param asciistring String appended to the value of this string.
     * @return A new ascii string
     */
    public ASCIIString concat(ASCIIString asciistring) {
        return new ASCIIString(text + asciistring.text);
    }

    /**
     * Concatenate this ASCII string with a java String
     * @param string String appended to the value of this string.
     * @return A new ASCIIString
     */
    public ASCIIString concat(String string) {
        return new ASCIIString(text + string);
    }

    /**
     * Compares ASCII String to another string
     * @param s string comparing to the value of this object
     * @return true if the ASCII characters exactly match the string characters
     */
    public boolean contentEquals(String s) {
        return text.contentEquals(s);
    }

    @Override
    public String toString() {
        return text;
    }

    /**
     * Number of bytes in string
     * @return number of bytes in string
     */
    public int length() {
        return text.length();
    }
    
    public byte[] toByteArray() {
        byte[] b = new byte[text.length()];

        int i = 0;
        for (char c : text.toCharArray())
            b[i++] = (byte) c;

        return b;
    }

    public byte[] toByteArray(byte[] b) {
        int i = 0;
        for (char c : text.toCharArray())
            b[i++] = (byte) c;

        return b;
    }

    /**
     * Decode a ByteBuffer containing ASCII codes into a String.
     *
     * @param buffer Contains ASCII code-points starting at the current position through the limit. The
     * current position is moved to the limit during the decoding, consuming the remaining available bytes
     * in the buffer.
     * @return A string mirroring the ASCII characters in the buffer.
     * @throws CharacterCodingException Thrown when the buffer contains non-ascii code-points.
     */
    public static String decode(ByteBuffer buffer) throws CharacterCodingException {
        CharBuffer cb;
        cb = ASCII.newDecoder().decode(buffer);
        return cb.toString();
    }

    /**
     * Decode a portion of a ByteBuffer containing ASCII codes into a String.
     *
     * @param buffer Contains ASCII code-points starting at the current position through the length. The
     * current position is moved forward for length bytes during the decoding.
     * @param length number of bytes from the current position to decode.
     * @return A string mirroring the ASCII characters in the buffer.
     * @throws CharacterCodingException Thrown when the buffer contains non-ascii code-points.
     */
    public static String decode(ByteBuffer buffer, int length) throws CharacterCodingException {
        ByteBuffer bb = ByteBuffer.wrap(buffer.array(), buffer.arrayOffset() + buffer.position(), length);

        CharBuffer cb;
        cb = ASCII.newDecoder().decode(bb);
        buffer.position(buffer.position() + length);
        return cb.toString();
    }

    /**
     * Match ASCII text against regex pattern and consume buffer if matches
     * <p>
     * Try to match a byte buffer against a pattern, assuming that the byte buffer contains ASCII
     * code points. If the match is successful then the buffer is consumed by moving the position
     * to the limit; matches under Matcher means matches then entire input. If no match then the
     * current position is not changed.
     *
     * @param pattern regex pattern
     * @param buffer byte buffer to use as input to the match after decoding as ASCII
     * @return true if matches.
     * @throws CharacterCodingException if the buffer contains code points not within ASCII.
     */
    public static boolean matchesThenConsume(Pattern pattern, ByteBuffer buffer) throws CharacterCodingException {
        CharBuffer cb;
        buffer.mark();
        cb = ASCII.newDecoder().decode(buffer);
        boolean r = pattern.matcher(cb).matches();
        if (!r) buffer.reset();
        return r;
    }

    /**
     * Match ASCII text against regex pattern and consume buffer
     * <p>
     * Identical to {@link #matchesThenConsume} except always consumes the buffer
     * by moving the position to the limit.
     * @param pattern Pattern to use from matching
     * @param buffer Buffer containing bytes
     * @return true when the pattern matches the buffer
     * @throws java.nio.charset.CharacterCodingException buffer contains non-ascii codes
     */
    public static boolean matchesAndConsume(Pattern pattern, ByteBuffer buffer) throws CharacterCodingException {
        CharBuffer cb;
        buffer.mark();
        cb = ASCII.newDecoder().decode(buffer);
        return pattern.matcher(cb).matches();
    }


    public boolean equals(Object o) {
        if (o instanceof ASCIIString)
            return text.equals(o);

        return super.equals(o);
    }

    public int hashCode() {
        return text.hashCode();
    }

}
