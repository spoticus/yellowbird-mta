/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.transport;

import java.nio.ByteBuffer;

/**
 * An RFC2822 Message with RFC2821 Headers being built by Inbound Transport
 * <p>
 * This is a message being built by data received from a
 * {@linkplain me.yellowbird.mta.io.DataLink inbound transport}.
 */
public interface InboundMessage {
    /**
     * Add line of data to message.
     * <p>
     * All of the data from the buffer is added to the message as a line.
     * No validation is done on the data to ensure that it only contains
     * 7-bit ascii codes and that it does not contain a cr/lf line ending
     * pair of codes.
     * <p>
     * The order that lines are added is the order they will exists in
     * the message.
     *
     * @param aline a line of ascii codes not containing a cr/lf pair.
     */
    public void add(ByteBuffer aline);
}
