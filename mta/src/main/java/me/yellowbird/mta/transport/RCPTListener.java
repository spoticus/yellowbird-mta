/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.transport;

import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.MailParameter;

import java.util.ArrayList;

/**
 * Listen to the envelope bus for RCPT commands.
 */
/**
 * Listen to the envelope bus for RCPT commands.
 * <p>
 * The mail client sends a RCPT command to name an additional email recipient. The command includes the forward-path,
 * which is the mailbox address to use for routing the email to the recipient. A listener "accepts" a recipient
 * by returning an {@linkplain SinkTransport} and only one listener can accept the recipient. The
 * EnvelopeBus enforces this rule by not signalling this event to any of the listeners after the accepting
 * listener.
 * <p>
 * The commands that come before and after this command are documented in the server state machine diagram
 * that can me found in the documentation for {@link EnvelopeBusListener}.
 */
public interface RCPTListener extends EnvelopeBusListener{
    /**
     * Provide listener the option to validate and accept forward-path.
     * <p>
     * Provides the option to validate and accept or reject the forward-path and the mail transaction.
     *
     * @param msgid identifier for the corresponding message for this forward-path
     * @param addrSpec forward-path address
     * @param mailparams esmtp parameters or null if none.
     * @return an outbound transport for receiving this message or null if no relay path can be calculated.
     * @throws me.yellowbird.mta.transport.ForwardPathException thrown when the forward-path should be rejected.
     */
    public SinkTransport doRCPT(MessageIdentifier msgid, AddrSpec addrSpec, ArrayList<MailParameter> mailparams) throws ForwardPathException;

    
}
