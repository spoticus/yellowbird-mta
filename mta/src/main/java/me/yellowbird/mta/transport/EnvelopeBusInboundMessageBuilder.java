/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.transport;

import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.MailParameter;
import me.yellowbird.mta.protocol.LineData;
import me.yellowbird.mta.protocol.RCPTCommandReply;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Build a message that will me relayed to another system.
 * <p/>
 * There is one instance for each incoming connection that can support more than one mail transaction.
 * Once started
 * the builder puts
 * every envelope header on the
 * {@linkplain EnvelopeBus envelope bus} where there is possibly only one that is shared for
 * all incoming messages. Listeners to the envelope bus populate the inbound message
 * with state information and can direct the builder to take actions, such as rejecting the
 * header or the entire message. For example, {@linkplain SinkTransport outbound transports}
 * are listeners that add themselves as final destinations for the message. It is this coupling
 * that provides for synchronous delivery.
 * <p/>
 */
//TODO add state tracking to ensure proper order
public class EnvelopeBusInboundMessageBuilder extends InboundMessageBuilder {
    private final EnvelopeBus envelopebus;
    private final MessageIdentifier msgid;

    /*
     * Multiple outbound transports can me associated with message.
     */
    private final Set<SinkTransport> outboundtransports = new HashSet<SinkTransport>();

    public EnvelopeBusInboundMessageBuilder(MessageIdentifier msgid, EnvelopeBus envelopebus) {
        this.msgid = msgid;
        this.envelopebus = envelopebus;
    }

    public void buildConnection(InetSocketAddress clientaddress) throws ConnectionException {
        envelopebus.doConnection(clientaddress);
    }

    public void buildMAIL(AddrSpec addrSpec, ArrayList<MailParameter> mailparams) throws ReversePathException {
        envelopebus.doMAIL(msgid, addrSpec, mailparams);
    }

    /**
     * Build for the RCPT command.
     *
     * The RCPT command is passed to the envelope bus, {@link EnvelopeBus#doRCPT(MessageIdentifier, me.yellowbird.mta.AddrSpec, java.util.ArrayList)},
     * that will either return a {@link SinkTransport} for the recipient or null if there are no transports that can handle the address.
     * When there are multiple recipients the EnvelopeBus may return different SinkTransport objects for each.
     * The builder accommodates this situation by maintaining a collection of SinkTransport objects that are later
     * bound together by the {@link #buildDATA} method. The collection is a unique set, so that when the
     * EnvelopeBus returns the same SinkTransport for multiple recipients, only one is kept in the collection.
     *
     * @param addrSpec The mailbox from the RCPT path
     * @param mailparams ESMTP parameters from the RCPT header or null if none
     * @throws me.yellowbird.mta.transport.ForwardPathException the request path is not allowed or valid.
     */
    public void buildRCPT(AddrSpec addrSpec, ArrayList<MailParameter> mailparams) throws ForwardPathException {
        SinkTransport o = envelopebus.doRCPT(msgid, addrSpec, mailparams);

        if (o == null)
            throw new ForwardPathException(new RCPTCommandReply(RCPTCommandReply.Code.E550, LineData.strip("Domain not allowed")));

        outboundtransports.add(o);
    }

    public SinkTransport buildDATA() throws DataException {
        envelopebus.doDATA(msgid);

        switch (outboundtransports.size()) {
            case 0:
                return null;
            case 1:
                return outboundtransports.toArray(new SinkTransport[1])[0];
            default:
                return new MultiOutboundTransport(outboundtransports);
        }
    }

}
