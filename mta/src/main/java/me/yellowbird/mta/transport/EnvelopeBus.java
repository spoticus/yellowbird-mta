/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.transport;

import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.MailParameter;

import java.net.InetSocketAddress;
import java.util.ArrayList;

/**
 * Envelope Bus Allows for Monitoring for Incoming Envelope Headers
 * <p>
 * See Listener Pattern
 * <p>
 * Logically, the envelope bus is a FIFO queue containing RFC2821 headers and commands that are associated
 * with the InboundMessageBuilder that queued the header on the bus. The bus
 * reads the headers from the queue and synchronously hands them to each bus listener.
 * The {@linkplain EnvelopeBusListener} has the option of ignoring the envelope, rejecting the envelope
 * or rejecting the message. Either rejection stops the bus from handing the header
 * to any listeners further down on the listener chain.
 * <p>
 * The implementation is a bit more optimized. Individual message interest are
 * represented by individual interfaces, such as {@linkplain MAILListener} that
 * represents an interest in the MAIL command.
 *
 * <h3>State Machine for Client Connection Correlated to Envelope Bus Behavior</h3>
 * A quasi-state diagram to show the sequence of state changes in a client conversation as
 * reflected on the envelope bus for one client exists in the architecture documentation.
 */
public class EnvelopeBus {
    private final ArrayList<ConnectionListener> connectionlisteners =
            new ArrayList<ConnectionListener>();
    private final ArrayList<MAILListener> MAILlisteners =
            new ArrayList<MAILListener>();
    private final ArrayList<RCPTListener> RCPTlisteners =
            new ArrayList<RCPTListener>();

    /**
     * Create an envelope bus with a specific set of listeners.
     *
     * @param listeners the listeners that will receive events from the bus.
     */
    public EnvelopeBus(ArrayList<EnvelopeBusListener> listeners) {
        for (EnvelopeBusListener listener : listeners) {
            if (listener instanceof ConnectionListener)
                connectionlisteners.add((ConnectionListener) listener);
            if (listener instanceof MAILListener)
                MAILlisteners.add((MAILListener) listener);
            if (listener instanceof RCPTListener)
                RCPTlisteners.add((RCPTListener) listener);
        }
    }

    /**
     * A hook for notifying the bus that a new connection has occurred.
     *
     * @param clientaddress the remote address of the client connecting to the MTA.
     * @throws ConnectionException thrown when an unrecoverable exception occurs. The connection has
     *                             not been properly registered on the bus and cannot me used further.
     */
    public void doConnection(InetSocketAddress clientaddress) throws ConnectionException {
        for (ConnectionListener listener : connectionlisteners)
            listener.doConnection(clientaddress);
    }

    /**
     * A hook for notifying the bus that a new MAIL command has been received.
     *
     * @param msgid          The unique identifier for the new message started by the MAIL command.
     * @param addrSpec The mailbox address from the MAIL command.
     * @param mailparams     The ESMTP parameters from the MAIL command.
     * @throws ReversePathException thrown when the reverse path should me rejected.
     */
    public void doMAIL(MessageIdentifier msgid, AddrSpec addrSpec, ArrayList<MailParameter> mailparams) throws ReversePathException {
        for (MAILListener listener : MAILlisteners)
            listener.doMAIL(msgid, addrSpec, mailparams);
    }

    /**
     * A hook for notifying the bus that a new RCPT command has been received.
     *
     * @param msgid          The unique identifier for the new message started by the MAIL command.
     * @param addrSpec The mailbox address from the RCPT command.
     * @param mailparams     The ESMTP parameters from the RCPT command.
     * @return Returns an {@link SinkTransport} that can relay this incoming message or null if no listener accepts the forward-path.
     * @throws ForwardPathException thrown when the forward-path should me rejected.
     */
    public SinkTransport doRCPT(MessageIdentifier msgid, AddrSpec addrSpec, ArrayList<MailParameter> mailparams) throws ForwardPathException {
        ArrayList<SinkTransport> outboundtransports = new ArrayList<SinkTransport>();

        for (RCPTListener listener : RCPTlisteners) {
            SinkTransport o = listener.doRCPT(msgid, addrSpec, mailparams);

            if (o != null)
                return o;
        }

        return null;
    }

    /**
     * A hook for notifying the bus that a DATA command has been received.
     *
     * @param msgid The unique identifier for the new message started by the MAIL command.
     * @throws DataException thrown when an unrecoverable exception occurs. The msgid is no longer
     *                       properly registered on the bus and cannot me used further.
     */
    public void doDATA(MessageIdentifier msgid) throws DataException {
        //TODO go through list of outboundtransports that use this message and do prepareForData
    }
}
