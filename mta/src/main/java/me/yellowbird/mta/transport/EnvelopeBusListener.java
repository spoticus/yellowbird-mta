/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.transport;

/**
 * Listens to the envelope bus for remote client commands, changes to the connection, and changes to the mail
 * transaction.
 * <p>
 * As remote clients send commands to the server, the server translates them into
 * command events that are put on the {@link EnvelopeBus}. EnvelopeBusListener instances are attached to an
 * envelope bus and receive these events.
 * <p>
 * For an incoming message there is a specific order dictated by RFC2821 and the system's state machine:
 * <ol>
 * <li>doConnection(...)
 * <li>doMAIL(...)
 * <li>doRCPT(...) repeated 1 or more times
 * <li>doDATA(...)
 * <li>doMAIL and doRCPT repeated 0 or more times per connection
 * </ol>
 *
 * <h3>Choosing Command Events of Interest</h3>
 * There are different listener interfaces for each of the command event types and they all extend this base
 * interface. If an implementation is only interested in the "MAIL" command then it only needs to implement
 * interface {@linkplain MAILListener}. An implementation can implement more than one of these interfaces.
 *
 * <h3>Attaching to the EnvelopeBus</h3>
 * A listener is attached to the EnvelopeBus by the {@linkplain me.yellowbird.mta.brain.Brain brain}.
 * When the system creates the envelope bus it also attaches the listeners
 * an order controlled by the {@link me.yellowbird.mta.brain.Configuration brain configuration system}.
 * The attachment order is the order that the envelope bus follows when relaying the events to the listeners.
 * <p>
 * The brain creates an instance either directly using a constructor or by factory. The
 * {@link me.yellowbird.mta.brain.EnvelopeBusListenerConfiguration} provides details.
 */
public interface EnvelopeBusListener {

}
