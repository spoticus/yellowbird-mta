/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.transport;

import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.MailParameter;

import java.util.ArrayList;

/**
 * Listen to the envelope bus for MAIL commands.
 * <p>
 * The mail client sends a MAIL command to start a new mail transaction. The command includes the reverse-path,
 * which is the mailbox address to use if the message needs to me sent back to the original sender. The
 * commands that come before and after this command are documented in the server state machine diagram
 * that can me found in the documentation for {@link EnvelopeBusListener}.
 */
public interface MAILListener extends EnvelopeBusListener {
    /**
     * Provides the option to validate and accept or reject the reverse-path and the mail transaction.
     * <p>
     * Listeners are provided with the reverse-path and esmtp parameters for a message.
     *
     * @param msgid identifier for the corresponding message for this reverse-path.
     * @param addrSpec reverse-path address.
     * @param mailparams esmtp parameters or null if none.
     * @throws me.yellowbird.mta.transport.ReversePathException thrown when the mail transaction should be rejected.
     */
    public void doMAIL(MessageIdentifier msgid, AddrSpec addrSpec, ArrayList<MailParameter> mailparams) throws ReversePathException;


}
