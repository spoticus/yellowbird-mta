/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.transport;

import me.yellowbird.mta.protocol.LineOfData;

import java.util.Collection;

/**
 * A multi-path outbound transport
 * <p/>
 * Enables sending messages to more than one outbound transport.
 */
class MultiOutboundTransport implements SinkTransport {
    private Collection<SinkTransport> outboundtransports;

    public MultiOutboundTransport(Collection<SinkTransport> outboundtransports) {
        this.outboundtransports = outboundtransports;
    }

    @Override
    public void prepareForData() throws MessageDataException {
        for (SinkTransport o : outboundtransports) {
            o.prepareForData();
        }
    }

    public void add(LineOfData data) throws MessageDataException {
        for (SinkTransport o : outboundtransports) {
            o.add(data);
        }
    }

    public void commit() throws MessageDataException {
        for (SinkTransport o : outboundtransports) {
            o.commit();
        }
    }

    @Override
    public void cancel() {
        for (SinkTransport o : outboundtransports) {
            o.cancel();
        }
    }
}
