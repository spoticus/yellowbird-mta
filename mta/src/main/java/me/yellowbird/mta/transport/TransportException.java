/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.transport;

import me.yellowbird.mta.protocol.CommandReply;

/**
 * Something is wrong with the transport of a message.
 */
public class TransportException extends Throwable {
    private CommandReply reply;

    /**
     * Construct from a command reply sent to client.
     *
     * @param reply message sent to client.
     */
    public TransportException(CommandReply reply) {
        super(reply.toLineData().toString());

        this.reply = reply;
    }

    public TransportException(CommandReply reply, Throwable cause) {
        super(reply.toLineData().toString(), cause);

        this.reply = reply;
    }

    public CommandReply getReply() {
        return reply;
    }
}
