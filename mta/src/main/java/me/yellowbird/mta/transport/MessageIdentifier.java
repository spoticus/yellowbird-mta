/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.transport;

import me.yellowbird.mta.protocol.LineData;
import me.yellowbird.mta.protocol.LineDataException;

import java.util.UUID;

/**
 * A globally unique identifier for a message
 */
public class MessageIdentifier {
    private UUID uuid;

    public static MessageIdentifier createMessageIdentifier() {
        return new MessageIdentifier(UUID.randomUUID());
    }

    private MessageIdentifier(UUID uuid) {
        this.uuid = uuid;
    }

    public String toString() {
        return uuid.toString();
    }

    public LineData toLineData() {
        try {
            return new LineData(toString());
        } catch (LineDataException e) {
            throw new RuntimeException("something changed in java.util.UUID toString");
        }
    }
}
