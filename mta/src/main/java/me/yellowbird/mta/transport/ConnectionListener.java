/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.transport;

import java.net.InetSocketAddress;

/**
 * Listen to the envelope bus for connections.
 */
public interface ConnectionListener extends EnvelopeBusListener {
    /**
     * Provide the option to validate, accept or reject a new connection.
     *
     * @param clientaddress remote address of the client wanting to send a message.
     * <red>WARNING</red> Use weak references. Your instance will last longer than this connection.
     * @throws ConnectionException thrown when the connection should me rejected.
     */
    public void doConnection(InetSocketAddress clientaddress) throws ConnectionException;

}
