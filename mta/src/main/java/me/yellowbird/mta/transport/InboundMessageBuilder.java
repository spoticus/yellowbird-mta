/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.transport;

import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.MailParameter;

import java.net.InetSocketAddress;
import java.util.ArrayList;

/**
 * An Incoming Message Builder
 * <p>
 * Refer to Builder Pattern "Builder"
 * <p/>
 * The builder partially constructs an {@linkplain InboundMessage inbound message}
 * by populating a new object with envelope headers and state information. Once the message
 * data arrives at the inbound transport, the builders job is finished and it returns the
 * partially constructed inbound message to the transport so that the transport can directly
 * fill the inbound message with the RFC2822 message.
 * <p>
 * The Builder expects the Director to provide the envelope headers first
 * and then a signal that the rfc2822 message data has arrived. The builder enforces the proper order:
 * <ol>
 * <li>Authentication token</li>
 * <li>From address</li>
 * <li>To addresses</li>
 * <li>Message</li>
 * </ol>
 * <p>
 * The builder sends envelope headers down the envelope bus for interested
 * parties to process and adds the headers to the {@linkplain InboundMessage composite "container" of a message}.
 */
/*
 * TODO replace secondary concern of envelope bus with advice.
 * TODO eliminate the builder. I see no reason that there would me another processor other than the envelope bus.
 */
public abstract class InboundMessageBuilder {
    public abstract void buildConnection(InetSocketAddress clientaddress) throws ConnectionException;

    /**
     * Build step "from" MAIL envelope header
     *
     * @param addrSpec The mailbox from the MAIL reverse-path or null if null-path
     * @param mailparams ESMTP parameters from the MAIL header or null if none
     * @throws me.yellowbird.mta.transport.ReversePathException the provided path is not allowed or valid.
     */
    abstract public void buildMAIL(AddrSpec addrSpec, ArrayList<MailParameter> mailparams) throws ReversePathException;

    /**
     * Build step "to" RCPT envelope header
     *
     * @param addrSpec The mailbox from the RCPT path
     * @param mailparams ESMTP parameters from the RCPT header or null if none
     * @throws me.yellowbird.mta.transport.ForwardPathException the request path is not allowed or valid.
     */
    abstract public void buildRCPT(AddrSpec addrSpec, ArrayList<MailParameter> mailparams) throws ForwardPathException;

    /**
     * Build step DATA envelope header.
     *
     * @return the outbound transport for the message or null if no paths exists for the message.
     * @throws me.yellowbird.mta.transport.DataException the data is not valid.
     */
    public abstract SinkTransport buildDATA() throws DataException;
}
