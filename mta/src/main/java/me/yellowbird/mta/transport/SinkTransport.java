/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.transport;

import me.yellowbird.mta.protocol.LineOfData;

/**
 * Sink of message data.
 * <p>
 * A sink transport receives data through it's {@link #add} methods.
 * When all of the data has been received the commit method is
 * called. No more data or communication will be made to the
 * sink transport after the commit.
 */
public interface SinkTransport {
    /**
     * Prepare to receive data.
     * <p>
     * Signals that data is about to arrive through the {@linkplain #add} method.
     * @throws MessageDataException thrown when the preparation fails and the transport cannot receive data.
     */
    public void prepareForData() throws MessageDataException;

    /**
     * Add another line to the message.
     *
     * @param data line of data to be added. The available buffer data will
     * be used but the current position may or may not be changed.
     * @throws MessageDataException When an internal error prohibits continuing the message.
     * The connection should be closed and this message transaction aborted.
     */
    public void add(LineOfData data) throws MessageDataException;

    /**
     * Commit the message transaction.
     * <p>
     * This is the final step in sending a message. All message data has been
     * written. The outbound transport can now complete the sending of the
     * message to it's final destination.
     * @throws MessageDataException When an internal error prohibits continuing. The
     * message will be discarded.
     */
    public void commit() throws MessageDataException;

    /**
     * Cancel a message transaction.
     * <p>
     * The message is being cancelled and the transport should cleanup all resources.
     * No other messages will be sent to the transport.
     */
    public void cancel();
}
