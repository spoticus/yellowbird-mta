/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.spooler;

import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.MailParameter;
import me.yellowbird.mta.brain.Brain;
import me.yellowbird.mta.mps.*;
import me.yellowbird.mta.protocol.LineData;
import me.yellowbird.mta.protocol.MAILCommandReply;
import me.yellowbird.mta.protocol.RCPTCommandReply;
import me.yellowbird.mta.transport.*;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * Spooler that uses file system for persistent storage of messages.
 *
 * The spooler couples an incoming message with persistent storage, the forwarding of
 * messages as will as sending DSNs to the sender and postmaster when there are errors.
 * <p/>
 * <h5>Spooler as recipient of incoming message from the envelope bus</h5>
 * <p/>
 * The spooler is attachable to the envelopebus as any other envelopebus listener
 * and will accept forward paths ({@link EnvelopeBusPlugin#doRCPT}) that it recognizes from a list of domain name patterns
 * that are part of the spooler's constructor. The same {@link me.yellowbird.mta.transport.SinkTransport} is
 * returned for the same message when it has multiple forward paths. Whoever receives
 * the SinkTransport must fill it with message data and when the commit is called
 * ({@link me.yellowbird.mta.transport.SinkTransport#commit}) the spooler will prepare the spooled message for
 * forwarding and make it available for forwarding through the ready for forwarding queue
 * ({@link #takeFileToForward}).
 * <p/>
 * <h5>Forwarding a Message</h5>
 */
public class MPSSpooler implements Spooler, Runnable {
    private static Logger logger = LoggerFactory.getLogger("spooler");
    private EnvelopeBusPlugin plugin;

    public EnvelopeBusPlugin getEnvelopeBusPlugin() {
        return plugin;
    }

    class EnvelopeBusPlugin implements MAILListener, RCPTListener {
        /*
         * Keep track of SinkTransport instances for each message between the initial state
         * of {@link #doMAIL} and {@link #doDATA}, at which time it is no longer necessary to
         * keep track.
         */
        //TODO need a concrete mechanism in the FileSinkTransport to remove these when prepareForData is called.
        private Map<MessageIdentifier, PersistentSinkTransport> spoolfiles =
                Collections.synchronizedMap(new WeakHashMap<MessageIdentifier, PersistentSinkTransport>());

         /**
          * Setup a new file to contain new message.
          *
          * @param addrSpec   reverse-path address
          * @param mailparams esmtp parameters or null if none.
          * @throws me.yellowbird.mta.transport.ReversePathException
          *          if something goes wrong with the spooler being unable to continue.
          */
         public void doMAIL(MessageIdentifier msgid, AddrSpec addrSpec, ArrayList<MailParameter> mailparams) throws ReversePathException {

             try {
                 spoolfiles.put(msgid, storage.createPersistentSinkTransport(msgid, addrSpec));
             } catch (StorageException e) {
                 throw new ReversePathException(new MAILCommandReply(MAILCommandReply.Code.E451, LineData.strip("Temporary error")), e);
             }
         }

         /**
          * Validate forward-path and associate with spool file.
          * <p/>
          * If this spooler can route for this recipient then associate the forward-path with
          * a file for containing the message.
          *
          * @param msgid      identifier for the corresponding message for this forward-path
          * @param addrSpec   forward-path address
          * @param mailparams esmtp parameters or null if none.
          * @return transport that will receive the data.
          * @throws ForwardPathException
          */
         public SinkTransport doRCPT(MessageIdentifier msgid, AddrSpec addrSpec, ArrayList<MailParameter> mailparams) throws ForwardPathException {
             for (Pattern p : domainpatterns) {
                 if (p.matcher(addrSpec.getDomain()).matches()) {
                     PersistentSinkTransport o = spoolfiles.get(msgid);

                     try {
                         o.addRecipient(addrSpec);
                     } catch (StorageException e) {
                         throw new ForwardPathException(new RCPTCommandReply(RCPTCommandReply.Code.E451, LineData.strip("Temporary error")), e);
                     }

                     return spoolfiles.get(msgid);
                 }
             }

             return null;
         }


     }

    /*
     * For incoming messages the spooler relies on the envelope bus api
     * and state machine. The incoming message id is the key in a map
     * to the outbound transport that will me used to store the message
     * data. This outbound transport writes messages to a file using
     * a format specific to this spooler. The outbound transport depends
     * on the envelope bus listener state machine for it to me initialized
     * completely. A weak reference is kept to the outbound transport
     * so that cleanup is automatic when it is no longer needed.
     */

    /*
     * For outgoing messages, the forwarder(s), which run on their own threads,
     * wait for files to show up in a queue. These files are relayed
     * to the next MTA. Files show up in the queue from the inbound
     * processing when the outboundtransport receives a commit and adds the
     * file to the queue.
     */



    // The directory where all the spool files are stored.

    // Patterns used to match against a recipient's domain. Only those that match are accepted by the spooler.
    private final ArrayList<Pattern> domainpatterns;


    private final Brain brain;

    // The storage
    private final Storage storage;

    /**
     * Construct a spooler for a directory and set of recipient matching patterns.
     *
     * @param storage Directory where spool files are stored.
     * @param domainpatterns Patterns for matching recipient domains. Only matching recipients are accepted by this spooler.
     * @param brain          The brain.
     */
    public MPSSpooler(Storage storage, ArrayList<Pattern> domainpatterns, Brain brain) throws FileFormatException, IOException, SpoolerException, StorageException {
        this.domainpatterns = domainpatterns;
        this.brain = brain;
        this.storage = storage;
        this.plugin = new EnvelopeBusPlugin();

    }

    /**
     * Requeue a spooledMessage for delivery later.
     *
     * This method exists to separate the concerns of forwarding a message to a recipient with
     * managing the lifecycle of a message, which the latter being the purpose of this method.
     * <p/>
     * Depending on the state of a spooledMessage certain action is taken:
     * <ul>
     * <li>If there are no more forwarders then DSNs are sent for undeliverable forwarders and then the spooledMessage file is deleted.
     * <li>If there are more forwarders, but the spooledMessage is past the giveup interval, then DSNs are sent for undeliverable forwarders and for giving up on the spooledMessage.
     * <li>Otherwise, the spooledMessage is queued for forwarding again at the delay interval.
     * </ul>
     *
     * @param spooledMessage spooledMessage to queue.
     */
    @Override
    public void requeueWithDelay(SpooledMessage spooledMessage) throws SpoolerException {

        try {
            if (!spooledMessage.hasMoreForwardPaths()) {
                sendErrorDSN(spooledMessage);
                storage.deleteOrLogError(spooledMessage);
            } else if (shouldGiveUp(spooledMessage)) {
                createGiveUpDSN(spooledMessage).requeueWithDelay(0, TimeUnit.MINUTES);
                storage.deleteOrLogError(spooledMessage);
            } else {
                spooledMessage.requeueWithDelay(brain.getRetryInterval(), TimeUnit.MINUTES);

            }
        } catch (StorageException e) {
            throw new SpoolerException("Unable to requeue message for future delivery", e);
        }
    }

    private SpooledMessage createGiveUpDSN(SpooledMessage spooledMessage) {
        //TODO
        return null;
    }

    private boolean shouldGiveUp(SpooledMessage spooledMessage) throws StorageException {
        if (true) return false;
        DateTime spoolDate = spooledMessage.getOriginalSpoolDate();

        DateTime expiration = spoolDate.plus(brain.getGiveUpDuration());

        DateTime now = new DateTime();

        return expiration.isAfter(now);
    }

    private void sendErrorDSN(SpooledMessage spooledMessage) {
        //TODO
        //To change body of created methods use File | Settings | File Templates.
    }

    @Override
    public SpooledMessage takeFileToForward() throws InterruptedException {
        return storage.takeFileToForward();
    }

    @Override
    public void run() {
        try {
            synchronized (this) {
                this.wait();
            }
        } catch (InterruptedException ignored) {
        }
    }
}
