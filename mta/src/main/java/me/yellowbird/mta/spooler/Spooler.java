/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.spooler;

import me.yellowbird.mta.mps.SpooledMessage;

/**
 * Spooler for Persist Messages and Asynchronous Delivery
 * <p>
 * A spooler is for asynchronous delivery of messages to an SinkTransport
 * that will deliver the message to it's final destination. A spooler is associated
 * with persistent storage so that the message will exists between executions of the
 * MTA.
 * <p>
 * The spooler provides methods for allocating
 * new storage for a message, incrementally storing messages (as when
 * they are being built), loading persisted messages and reading messages
 * incrementally to avoid loading large messages into memory (as when
 * they are transmitted).
 */
public interface Spooler {
    void requeueWithDelay(SpooledMessage spooledMessage) throws SpoolerException;

    SpooledMessage takeFileToForward() throws InterruptedException;
}
