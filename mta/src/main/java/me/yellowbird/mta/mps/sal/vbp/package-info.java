/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

/**
 Storage Abstraction Layer

 <h2>Package Specification</h2>

 Variable block files provide for storing multiple, variable length byte sequences that are
 addressable by a logical, unique index.
 are removed or altered.

 <h2>File Format V1.0</h2>

 The structure of a file is a series of blocks of varying lengths that contain bytes. A block is located by
 it's offset from the beginning of the physical file. Any implementation of physical storage must
 provide a mechanism for reading and writing bytes using this offset.

 <p>

 The beginning of the file is a file signature followed by the first block or by no blocks if the file
 contains no blocks. A block begins with a header
 followed by content. Blocks are contiguous and subsequent blocks are located by adding the current
 block offset with the block length in the block header.

 <p>

 The block header contains the length of the block, the logical block number and the length of user data.
 If the logical block number is a negative one then the block is unused and available for new block request.
 Logical block numbers do not have to be sequential or
 arranged contiguously, but must be unique.

 <table>
 <tr><th>Offset</th><th>Identifier</th><th>Meaning</th></tr>
 <tr><td>0</td><td>SIGNATURE</td><td>The file signature that contains ASCII value "mtavbf0100      "</td></tr>
 <tr><td>16</td><td>FIRSTBLOCK</td><td>The first block in the file</td></tr>
 <tr><td>16+FIRSTBLOCK.LENGTH</td><td>The next block in the file</td></tr>
 <tr><td>CALCULATEDOFFSET</td><td>Subsequent blocks in the file</td></tr>
 </table>

 <table>
 <tr><th>Offset Within Block</th><th>Identifier</th><td>Meaning</th></tr>
 <tr><td>0</td><td>BLOCKLENGTH</td><td>64-bit signed twos-compliment (java long) big-endian. The length of the block. The next block can be found by adding this length to the current block's offset</td></tr>
 <tr><td>8</td><td>LOGICALBLOCKNUMBER</td><td> 32-bit signed twos-compliment (java int)  big-endian where {x|-1,W}. The logical block number that is always positive whole number. If it has the value -1 then the block is unused.</td></tr>
 <tr><td>12</td><td>CONTENTLENGTH</td><td>64-bit signed twos-compliment (java long)  big-endian where {x|W}. The length of the content, which can be less than the length of the block.</td></tr>
 <tr><td>20</td><td>CONTENT</td><td>Bytes of data that is the content of the block.</td></tr>
 <tr><td>20+CONTENTLENGTH</td><td>EMPTYSPACE</td><td>Bytes beyond the content through the end of the block are undefined. A block may have no undefined bytes.</td></tr>
 </table>



 */
package me.yellowbird.mta.mps.sal.vbp;
