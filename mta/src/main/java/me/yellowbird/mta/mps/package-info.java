/**
 Message Persistent FileStorage
 <h2>Package Specification</h2>
 <h2>File Format</h2>
 <p>
      Data is written to the file with a specific format so that the
      original envelope data can be carried with the message data.
      The file contains lines using CRLF. The protocol requirements
      for CRLF to be line terminators protects the format. The header
      data is written in ascii and the message data is written as
      it is received in octets with CRLF appended to each line.
 </p>
 <p>
     The name of the file is random, except that the file extension
 for a file that is complete is ".v1", which provides for
 convenient identification of those files.
 </p>
 <pre>
 format version number of "1.0.1"
 long integer date spooler received message as the instant as milliseconds from the Java epoch of 1970-01-01T00:00:00Z
 message id string format
 reverse-path
 Sforward-path [one or more]
 .
 message data
 EOF
 </pre>

 The "S" in front of the forward-path denotes the status of forwarding
 to this recipient:
 <ul>
     <li>0=has not been sent to this recipient</li>
     <li>1=has been sent to this recipient</li>
     <li>2=cannot be delivered to this recipient</li>
 </ul>
 */
package me.yellowbird.mta.mps;
