/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

/**
 Storage Abstraction Layer

 <h2>Package Specification</h2>

 The storage abstraction layer provides persistent storage using the file system through an API that
 represents this storage as files that contain variable length blocks of data. A file can have any
 number of variable length blocks that are referenced by an index. The indexes do not have to be
 contiguous. A blocks size is set by the data put in the block. A block can be shrunk by replacing
 the content with data that has a shorter length. It can be extended by replacing the content with
 data that has a longer length or by appending data to the block's current content. Blocks can
 be removed from the file. The underlying efficiency of storage is not predictable when blocks
 are removed or altered.

 <p>

 Package {@link me.yellowbird.mta.mps.sal.vbp} contains the mechanism for persisting data using
 a file containing logically indexed blocks that are variable in length. The package uses a
 factory {@link me.yellowbird.mta.mps.sal.vbp.VariableBlockPersistenceFactory} for creating new persistence
 files and for finding existing persistence files. The only implementation is {@link me.yellowbird.mta.mps.sal.vbp.VBFFv10}
 that uses java.io and java.nio to persist messages in the operating system's file system.

 <p>

 Package {@link me.yellowbird.mta.mps.sal.pmq} contains the mechanism for persisting messages and
 managing a queue of persisted messages.
 */
package me.yellowbird.mta.mps.sal;
