/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.sal.vbp;

import me.yellowbird.mta.mps.StorageException;

import java.nio.ByteBuffer;

/**
 * A Multiple, Variable Length Block File.
 */
public interface VariableBlockPersistence {
    /**
     * Open file.
     *
     * If the file in the operating system does not exists then create a new one, initialize
     * and make it ready for reading and writing.
     * If the file already exists then make it ready for reading and writing of the existing content.
     *
     * @throws me.yellowbird.mta.mps.StorageException Thrown when a error is encountered in the storage system.
     * @throws StructureException When the structure of the file does not conform to the specification.
     */
    // TODO switch to an auto open/close model
    void open() throws StorageException, StructureException;

    /**
     * Close file.
     *
     * @throws me.yellowbird.mta.mps.StorageException Thrown when a error is encountered in the storage system.
     */
    // TODO switch to an auto open/close model
    void close() throws StorageException;

    boolean isOpen() throws StorageException;
    
    /**
     * Set the value of the block identified by the index.
     *
     * If the logical index does not exists then a new variable block is created
     * with the given index and value as the content. If the logical index already exists then the
     * content of the variable block is replaced with the value.
     *
     * @param logicalIndex The logical index identifying the block.
     * @param value value to store in the block.
     * @throws StorageException An internal exception has occurred.
     */
    void set(LogicalIndex logicalIndex, byte[] value) throws StorageException;

    /**
     * Get the value contained in the block identified by the index.
     *
     * The content of the variable block identified by the logic index.
     * 
     * @param logicalIndex The logical index identifying the block.
     * @return The value contained in the block.
     * @throws IndexOutOfBoundsException If the index does not exist in the file.
     * @throws StorageException An internal exception has occurred.
     * @throws ContentTooLargeException The content in the block cannot fit into a byte array.
     * @throws StructureException The structure of the variable block persistence is incorrect and the block cannot be identified.
     */
    byte[] get(LogicalIndex logicalIndex) throws IndexOutOfBoundsException, ContentTooLargeException, StorageException, StructureException;

    /**
     * Delete the persistence of this data.
     *
     * @throws StorageException An internal exception has occurred.
     */
    void delete() throws StorageException;

    /**
     * Change reday state of variable block file to ready
     *
     * @throws StorageException An internal exception has occurred.
     */
    void markReady() throws StorageException;

    /**
     * Append remaining bytes in ByteBuffer to variable block.
     *
     * Append the remaining bytes from a ByteBuffer to an existing block, increasing the block
     * size if necessary. If the block does not exists then create a new block with the bytes
     * from the ByteBuffer as {@link #set} does.
     *
     * @param logicalIndex The logical index identifying the block.
     * @param value value to append in the block.
     * @throws StorageException An internal exception has occurred.
     */
    void append(LogicalIndex logicalIndex, byte[] value) throws StorageException;

    /**
     * Get ByteBuffer containing content of block.
     *
     * The ByteBuffer has position of zero and limit the size of the content.
     *
     * @param logicalIndex The logical index identifying the block.
     * @return ByteBuffer containing content ready for reading.
     * @throws IndexOutOfBoundsException If the index does not exist in the file.
     * @throws StorageException An internal exception has occurred.
     * @throws ContentTooLargeException The content in the block cannot fit into a buffer.
     * @throws StructureException The structure of the variable block persistence is incorrect and the block cannot be identified.
     */
    ByteBuffer getByteBuffer(LogicalIndex logicalIndex) throws IndexOutOfBoundsException, StorageException, ContentTooLargeException, StructureException;
}
