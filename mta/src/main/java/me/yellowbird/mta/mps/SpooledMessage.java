/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps;

import java.util.concurrent.TimeUnit;

/**
 * A message that has been spooled.
 * <p>
 * A message that exists in storage, can be queried for attributes and forwarded.
 */
public interface SpooledMessage extends Message {

    /**
     * Requeue message for delivery later.
     *
     * @param delay amount of delay.
     * @param unit unit of measure for the delay.
     * @throws StorageException Storage error occurred.
     */
    void requeueWithDelay(long delay, TimeUnit unit) throws StorageException;

    /**
     * Create a give-up interval DSN message for this message.
     *
     * RFC2821 6.1 requirement.
     *
     * @return A SpooledMessage that can be routed back to the sender.
     * @throws StorageException Storage error occurred.
     */
//    SpooledMessage createGiveUpDSN() throws StorageException;

    /**
     * Delete spooled message persistence.
     */
    void delete() throws StorageException;
}
