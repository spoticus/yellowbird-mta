/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.sal.vlp;

import me.yellowbird.mta.mps.StorageException;

import java.io.File;

/**
 * A directory where files can be created.
 */
public class FileDirectory extends File {
    /**
     * {@inheritDoc}
     *
     * The path referenced must be a writable directory.
     *
     * @param pathname A path to a directory.
     */
    public FileDirectory(String pathname) throws StorageException {
        super(pathname);

        if (!isDirectory())
            throw new StorageException("Path is not a directory:" + pathname);

        if (!canWrite())
            throw new StorageException("Cannot write to the directory:" + pathname);

        if (!canRead())
            throw new StorageException("Cannot read from the directory:" + pathname);
    }

    /**
     * Creates a FileDirectory from the absolute path of the file.
     *
     * @param directory A File referencing a directory for the storage of the files.
     * @throws me.yellowbird.mta.mps.StorageException When the directory is unacceptable.
     */
    public FileDirectory(File directory) throws StorageException {
        this(directory.getAbsolutePath());
    }
}
