/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.sal.vbp;

/**
 * Index to a Block.
 */
public interface LogicalIndex {
    static enum PreDefined implements LogicalIndex {
        UNUSED(-1);

        private int index;

        PreDefined(int index) {
            this.index = index;
        }

//        public LogicalIndex getLogicalIndex() {
//            return index;
//        }

        @Override
        public int getIndex() {
            return index;
        }
    }

//    public LogicalIndex(int index) {
//        this.index = index;
//    }

//    static final int FREEBLOCKLOGICALINDEX = -1;
//    private int index;

    /**
     * Get the logical index number of the block.
     *
     * @return The logical block number that is always a positive whole number.
     */
    public int getIndex();

//    public LogicalIndex add(int skipvalue) {
//        return new LogicalIndex(index + skipvalue);
//    }
}
