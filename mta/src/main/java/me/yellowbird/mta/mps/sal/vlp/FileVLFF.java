/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.sal.vlp;

import me.yellowbird.mta.mps.StorageException;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Storage using java.io, java.nio and File class.
 */
public class FileVLFF implements VariableLengthPersistenceFactory {
    private FileDirectory directory;

    /**
     * Create a storage manager using a directory for containing files.
     *
     * @param directory The directory that will be used to store the files.
     */
    public FileVLFF(FileDirectory directory) {
        this.directory = directory;
    }

    @Override
    public VariableLengthPersistence createVariableLengthFile() throws StorageException {
        try {
            return new FileVLF(File.createTempFile("mta","vlp", directory));
        } catch (IOException e) {
            throw new StorageException("unable to create variable length file", e);
        }
    }

    @Override
    public List<VariableLengthPersistence> getIsReady() throws StorageException {
        FileFilter ff = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isFile() && pathname.canRead() && pathname.canWrite()
                        && pathname.getName().endsWith(FileVLF.READYFILESUFFIX);
            }
        };

        File[] files = directory.listFiles(ff);

        ArrayList<VariableLengthPersistence> r = new ArrayList<VariableLengthPersistence>(files.length);

        for (File f : files) {
            r.add(new FileVLF(f));
        }

        return r;
    }
}