/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.vbm;

import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.mps.*;
import me.yellowbird.mta.mps.sal.vbp.ConstantIndex;
import me.yellowbird.mta.mps.sal.vbp.VBFv10;
import me.yellowbird.mta.mps.sal.vbp.VariableBlockPersistence;
import me.yellowbird.mta.mps.sal.vbp.VariableBlockPersistenceFactory;
import me.yellowbird.mta.protocol.LineData;
import me.yellowbird.mta.protocol.LineOfData;
import me.yellowbird.mta.protocol.MessageDataCommandReply;
import me.yellowbird.mta.transport.MessageDataException;
import me.yellowbird.mta.transport.MessageIdentifier;
import org.joda.time.Instant;

import java.nio.ByteBuffer;

/**
 * Variable Block Persistence containing a new message that can be written.
 * <p/>
 * See the package documentation for a description of the file.
 * <p/>
 * A new file is created to support the SinkTransport interface. After
 * file creation, the file can me written to by the SinkTransport#add
 * method.
 * <p/>
 * When the message has been completely received it is marked as ready and
 * queued for forwarding.
 */

public class VBPSinkTransport implements PersistentSinkTransport {
    private VariableBlockPersistence vbp;
    private int forwardPathCount = 0;
//    private VariableBlockPersistenceFactory storage;
    private PersistedMessageQueue queue;

    /**
     * Create a new file that can me written to through the SinkTransport
     * interface.
     *
     * @param storage  corresponding storage controlling this message transport.
     * @param msgid    globally unique message identifier.
     * @param addrSpec mailbox address from the MAIL command.
     */
    public VBPSinkTransport(PersistedMessageQueue queue, VariableBlockPersistenceFactory storage, MessageIdentifier msgid, AddrSpec addrSpec) throws StorageException {

        this.queue = queue;
//        this.storage = storage;

        this.vbp = storage.createVariableBlockPersistence();

        vbp.open();

        vbp.set(BlockType.FORMAT, VBFv10.makeBytes("1.0.2"));

        long ts = new Instant().getMillis();
        vbp.set(BlockType.RECEIVEDATE, ByteBuffer.allocate(8).putLong(ts).array());

        vbp.set(BlockType.MSGID, msgid.toLineData().toByteArray());

        vbp.set(BlockType.REVERSEPATH, addrSpec.toLineData().toByteArray());
    }

    /**
     * Add data to message file.
     *
     * @param data line of data to me added.
     * @throws me.yellowbird.mta.transport.MessageDataException
     *          thrown when adding data fails. This is unrecoverable.
     */
    public void add(LineOfData data) throws MessageDataException {
        try {
            ByteBuffer bb = data.getByteBuffer();

            byte[] value = new byte[bb.remaining()];

            System.arraycopy(bb.array(), bb.arrayOffset(), value, 0, value.length);

            //TODO optimization to pass ByteBuffer all the way to vlf
            vbp.append( new ConstantIndex(BlockType.FIRSTFORWARDPATH.getIndex() + forwardPathCount), value);
        } catch (StorageException e) {
            throw new MessageDataException(new MessageDataCommandReply(MessageDataCommandReply.Code.E451, LineData.strip("Temporary error")), e);
        }
    }

    public void addRecipient(AddrSpec addrSpec) throws StorageException {
        byte[] b = addrSpec.toLineData().toByteArray();

        byte[] c = new byte[b.length + 1];

        c[0] = '0';

        System.arraycopy(b, 0, c, 1, b.length);

        vbp.set(new ConstantIndex(BlockType.FIRSTFORWARDPATH.getIndex() + forwardPathCount++), c);

        ByteBuffer bb = ByteBuffer.allocate(4);

        bb.putInt(forwardPathCount);

        vbp.set(BlockType.FORWARDPATHCOUNT, bb.array());
    }

    /**
     * Write mark to message file to indicate the beginning of the message data.
     *
     * @throws me.yellowbird.mta.transport.MessageDataException
     *          thrown when unrecoverable error occurs adding recipient.
     */
    public void prepareForData() throws MessageDataException {
    }

    /**
     * Close message file.
     *
     * @throws me.yellowbird.mta.transport.MessageDataException
     *          thrown when unrecoverable error occurs while closing.
     */
    public void commit() throws MessageDataException {
        try {
            vbp.close();

            vbp.markReady();
        } catch (StorageException e) {
            throw new MessageDataException(new MessageDataCommandReply(MessageDataCommandReply.Code.E451, LineData.strip("Temporary error")), e);
        }


//        try {
        queue.relay(new TimedDelayedMessage(new VBPSpooledMessage(queue, vbp)));
//        } catch (IOException e) {
//            throw new MessageDataException(new MessageDataCommandReply(MessageDataCommandReply.Code.E451, LineData.strip("Temporary error")), e);
//        } catch (FileFormatException e) {
//            throw new MessageDataException(new MessageDataCommandReply(MessageDataCommandReply.Code.E451, LineData.strip("Temporary error")), e);
//        }

//        file = null;
    }

    @Override
    public void cancel() {
        try {
            vbp.close();
            vbp.delete();
        } catch (StorageException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

//        file.delete();
    }

//    public void add(byte[] data) {

    //To change body of created methods use File | Settings | File Templates.
//    }
}