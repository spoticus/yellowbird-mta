/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.sal.vlp;

import me.yellowbird.mta.mps.StorageException;

import java.nio.ByteBuffer;

/**
 * An auto-sizing variable length file of bytes.
 *
 * A file must be opened via the {@linkplain #open} method before manipulating the content then
 * closed via the {@linkplain #close} method to free system resources when it's no longer needed.
 */
public interface VariableLengthPersistence {
    /**
     * Get length of file.
     *
     * @return The length of the file which also represents the number of bytes in the file.
     */
//    long getLength();

    /**
     * Get current byte offset.
     *
     * @return The current offset that relative operators use for reading and writing bytes.
     */
//    long getCurrentOffset();

    /**
     * Open file.
     *
     * If the file in the operating system does not exists then create a new one with zero length content
     * and make it ready for reading and writing.
     * If the file already exists then make it ready for reading and writing of the existing content.
     *
     * @throws me.yellowbird.mta.mps.StorageException Thrown when a error is encountered in the storage system.
     */
    void open() throws StorageException;

    /**
     * Close file.
     * 
     * @throws me.yellowbird.mta.mps.StorageException Thrown when a error is encountered in the storage system.
     */
    void close() throws StorageException;

    /**
     * Put bytes at a location.
     *
     * All bytes from the bytes parameter are written to storage starting at the offset parameter.
     * Existing data in the storage starting at the offset are replaced. The file is extended if the
     * offset + the byte array length is greater than the current file length.
     *
     * @param offset offset location where the first byte is written as a whole number. The remaining bytes are written in sequence from this offset.
     * @param bytes bytes to be written.
     * @throws me.yellowbird.mta.mps.StorageException Thrown when a error is encountered in the storage system.
     * @throws IllegalArgumentException If the offset is negative.
     */
    void put(long offset, byte[] bytes) throws IllegalArgumentException, StorageException;

    /**
     * Get bytes from a location.
     *
     * Read bytes from the file starting at the offset parameter and store the data in the byte array starting at index 0.
     * The entire array is filled with bytes, unless the end of the file is reached before the array is is full.
     * In this situation an UnderflowException is thrown and the value of the bytes in the array file should be considered
     * unknown.
     *
     * @param offset offset location in storage where the first byte is read. The remaining bytes are read in sequence from this offset.
     * @param bytes array where bytes read from storage are written.
     * @throws me.yellowbird.mta.mps.StorageException Thrown when a error is encountered in the storage system.
     * @throws UnderflowException when an underflow condition occurs because insufficient data exists in the file to fill the byte array.
     */
    void get(long offset, byte[] bytes) throws StorageException, UnderflowException;

    /**
     * Determine if the file is empty.
     *
     * @return true if the file contains no data, meaning it has a zero length.
     * @throws me.yellowbird.mta.mps.StorageException Thrown when a error is encountered in the storage system.
     */
    boolean isEmpty() throws StorageException;

    /**
     * Delete the persistence of this data.
     * @throws me.yellowbird.mta.mps.StorageException Thrown when a error is encountered in the storage system.
     */
    void delete() throws StorageException;

    /**
     * Change ready state of variable block file to ready
     *
     * @throws StorageException An internal exception has occurred.
     */
    void markReady() throws StorageException;

    /**
     * Copy bytes from one location to another.
     *
     * The bytes in sequence from the src offset are copied to the dst offset. There is no guarantee
     * that overlapping ranges of src and dst will be copied successfully.
     * 
     * @param src offset of first byte to copy.
     * @param dst offset of destination for first byte copied.
     * @param length number of bytes to copy.
     * @throws me.yellowbird.mta.mps.StorageException Thrown when a error is encountered in the storage system.
     * @throws UnderflowException when an underflow condition occurs because insufficient data exists in the file to fill the byte array.
     */
    void copy(long src, long dst, long length) throws StorageException, UnderflowException;

    /**
     * Get ByteBuffer wrapping storage at offset for a length.
     *
     * @param offset offset location in storage where the first byte is read. The remaining bytes are read in sequence from this offset.
     * @param length number of bytes from offset to including in ByteBuffer.
     * @return ByteBuffer ready for reading.
     * @throws me.yellowbird.mta.mps.StorageException Thrown when a error is encountered in the storage system.
     */
    ByteBuffer getByteBuffer(long offset, long length) throws StorageException;

    boolean isOpen();
}
