/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps;

import me.yellowbird.mta.mps.sal.vbp.LogicalIndex;

/**
 * Names the type content for blocks in VariableBlockPersistence.
 *
 * @see me.yellowbird.mta.mps for description of message file format.
 */
public enum BlockType implements LogicalIndex {
    FORMAT(0),
    RECEIVEDATE(1),
    MSGID(2),
    REVERSEPATH(3),
    FORWARDPATHCOUNT(4),
    FIRSTFORWARDPATH(5);

    private int index;

//    private LogicalIndex blockNumber;

    BlockType(int index) {
        this.index = index;
//        this.blockNumber = new LogicalIndex(blockNumber);
    }

//    public LogicalIndex getIndex() {
//        return blockNumber;
//    }

    @Override
    public int getIndex() {
        return index;
    }
}
