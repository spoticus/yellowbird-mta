/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.sal.vbp;

import me.yellowbird.mta.mps.StorageException;
import me.yellowbird.mta.mps.sal.vlp.VariableLengthPersistence;
import me.yellowbird.mta.mps.sal.vlp.VariableLengthPersistenceFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Storage using java.io, java.nio and File class.
 */
public class VBFFv10 implements VariableBlockPersistenceFactory {
    private VariableLengthPersistenceFactory vlffactory;

    public VBFFv10(VariableLengthPersistenceFactory vlffactory) {
        this.vlffactory = vlffactory;
    }

    @Override
    public VariableBlockPersistence createVariableBlockPersistence() throws StorageException {
        return new VBFv10(vlffactory.createVariableLengthFile());
    }

    @Override
    public ArrayList<VariableBlockPersistence> getIsReady() throws StorageException {
        List<VariableLengthPersistence> a = vlffactory.getIsReady();

        ArrayList<VariableBlockPersistence> r = new ArrayList<VariableBlockPersistence>(a.size());

        for (VariableLengthPersistence b : a)
            r.add(new VBFv10(b));

        return r;
    }
}
