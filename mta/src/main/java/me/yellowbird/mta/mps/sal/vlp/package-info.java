/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

/**
 Variable Length File

 <h2>Package Specification</h2>

 Provides for abstracting a "file" that contains bytes. The length of the file is equal to the
 content length. The bytes are addressed linearly as with an array of bytes, where the first byte is referenced by an index
 of zero. The last byte is referenced by an index of "file length" - 1. The persistent mechanism
 provides for automatic re-indexing as bytes are inserted, appended and removed.
 */
package me.yellowbird.mta.mps.sal.vlp;
