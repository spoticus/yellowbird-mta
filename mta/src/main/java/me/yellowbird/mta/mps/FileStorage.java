/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps;

import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.mps.sal.vbp.VBFFv10;
import me.yellowbird.mta.mps.sal.vbp.VariableBlockPersistence;
import me.yellowbird.mta.mps.sal.vbp.VariableBlockPersistenceFactory;
import me.yellowbird.mta.mps.sal.vlp.FileDirectory;
import me.yellowbird.mta.mps.sal.vlp.FileVLFF;
import me.yellowbird.mta.mps.vbm.VBPSinkTransport;
import me.yellowbird.mta.mps.vbm.VBPSpooledMessage;
import me.yellowbird.mta.spooler.DelayedMessage;
import me.yellowbird.mta.transport.MessageIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.DelayQueue;

/**
 * Persistent FileStorage.
 */
public class FileStorage implements Storage {
    private static Logger logger = LoggerFactory.getLogger("mps");

    private final File spooldirectory;
    private final VariableBlockPersistenceFactory storageManager;

    private PersistedMessageQueue queue = new PersistedMessageQueue() {
        private DelayQueue<DelayedMessage> readyforforward = new DelayQueue<DelayedMessage>();

        @Override
        public void relay(DelayedMessage delayedMessage) {
            readyforforward.put(delayedMessage);
        }

        @Override
        public DelayedMessage take() throws InterruptedException {
            return readyforforward.take();
        }
    };

    public FileStorage(File spooldirectory) throws StorageException {
        this.spooldirectory = spooldirectory;
        this.storageManager = new VBFFv10(new FileVLFF(new FileDirectory(spooldirectory)));//TODO abstract this construction somehow. pick a pattern.
        scanSpoolDirectory();
    }

    @Override
    public PersistentSinkTransport createPersistentSinkTransport(MessageIdentifier msgid, AddrSpec addrSpec) throws StorageException {
//        return new FileSinkTransport(this, spooldirectory, msgid, addrSpec);
        return new VBPSinkTransport(queue, storageManager, msgid, addrSpec);
    }

    @Override
    @Deprecated
    public void deleteOrLogError(SpooledMessage spooledMessage) {
//        FileMessageSource f = (FileMessageSource) spooledMessage;//TODO this goes away when we starting pushing files to forwarder.

//        deleteOrLogError(f.getFile());

        try {
            spooledMessage.delete();
        } catch (StorageException e) {
            logger.error("Could not delete spool file " + spooledMessage.toString());
        }
    }

    private void deleteOrLogError(File file) {
        try {
        if (!file.delete())
            logger.error("Could not delete spool file " + file);
        } catch(RuntimeException e) {
            logger.error("Could not delete spool file " + file);
        }
    }

    private void scanSpoolDirectory() throws StorageException {
        //files with the right suffix
//        FileFilter ff = new FileFilter() {
//            @Override
//            public boolean accept(File pathname) {
//                return pathname.isFile() && pathname.canRead() && pathname.canWrite()
//                        && pathname.getName().endsWith(FileSinkTransport.READYFILESUFFIX);
//            }
//        };
//
//        File[] files = spooldirectory.listFiles(ff);

        ArrayList<VariableBlockPersistence> z = storageManager.getIsReady();

//        for (File file : files) {
//            try {
//                readyforforward.put(new TimedDelayedMessage(new FileMessageSource(this, file)));
//            } catch (IOException e) {
//                throw new StorageException("Could not read a spooled message", e);
//            } catch (FileFormatException e) {
//                logger.warn("Message file found in spool directory when not expected (" + file.getName() + ")");
//            }
//        }

        for (VariableBlockPersistence vbp : z) {
            queue.relay(new TimedDelayedMessage(new VBPSpooledMessage(queue, vbp)));

        }
    }

    /**
     * Relay a file in the spooler format.
     *
     * @param message to be relayed.
     */
    public void relay(DelayedMessage message) {
        queue.relay(message);
    }

    @Override
    public SpooledMessage takeFileToForward() throws InterruptedException {
        /*
         * Always return a value and never pass out the file specific exceptions.
         * Don't scatter concerns. Sometime in the future this will change to a push
         * to the forwarder rather than a take, and this will seem more natural.
         */
        while (true) {
            try {
                return queue.take().getSpooledMessage();
            } catch (IOException e) {
                logger.error("Unable to create message source. Not requeuing.", e);
            } catch (FileFormatException e) {
                logger.error("Invalid file format. Unable to create message source. Not requeuing.", e);
            }
        }
    }
}
