/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

/**
 Variable Block Persisted Message


 <h2>New File Format Using VariableBlockPersistence</h2>
 <table>
 <tr><th>Block</th><th>Content</th></tr>
 <tr><td>0</td><td>Message format constant "1.0.2"</td></tr>
 <tr><td>1</td><td>64-bit 2-comp (long) integer date spooler received message as the instant as milliseconds from the Java epoch of 1970-01-01T00:00:00Z</td></tr>
 <tr><td>2</td><td>message id as a string</td></tr>
 <tr><td>3</td><td>reverse-path</td></tr>
 <tr><td>4</td><td>32-bit 2-comp (int) forward-path count</td></tr>
 <tr><td>5 through number of forward-paths</td><td>forward-path structure of Sforward-path</td></tr>
 <tr><td>5 + forward-path count</td><td>message data (see below)</td></tr>
 </table>

 In addition a file can be in the not ready state, ready state and corrupt format state. The underlying storage mechanism
 is responsible for providing this state and for search for files based on their state.
 
 <h5>Forward-Path Structure</h5>
 A forward-path is stored in a variable block in a specific format:

 <pre>
 Sforward-path
 </pre>

The "S" in front of the forward-path denotes the status of forwarding
 to this recipient:
 <ul>
     <li>0=has not been sent to this recipient</li>
     <li>1=has been sent to this recipient</li>
     <li>2=cannot be delivered to this recipient</li>
 </ul>
 
 <h5>Message Data</h5>
 The message data is stored in a variable block as received from the originator, without any interpretation including
 CRLF line endings or encoding. It is stored as 8-bit bytes and headers in the message must identify any
 special encoding different than the standard 7-bit encoding that is the minimum required to be supported
 by the SMTP protocol.
 */
package me.yellowbird.mta.mps.vbm;
