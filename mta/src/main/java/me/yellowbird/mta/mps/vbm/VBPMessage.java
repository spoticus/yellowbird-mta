/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.vbm;

import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.InvalidMailBoxException;
import me.yellowbird.mta.mps.BlockType;
import me.yellowbird.mta.mps.Message;
import me.yellowbird.mta.mps.StorageException;
import me.yellowbird.mta.mps.sal.vbp.ConstantIndex;
import me.yellowbird.mta.mps.sal.vbp.LogicalIndex;
import me.yellowbird.mta.mps.sal.vbp.VariableBlockPersistence;
import me.yellowbird.mta.protocol.ClientProtocolException;
import me.yellowbird.mta.transport.SourceTransport;
import org.joda.time.DateTime;

import java.io.IOException;
import java.lang.ref.PhantomReference;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * A message in a VariableBlockPersistence.
 */
public class VBPMessage implements Message {
    private final VariableBlockPersistence vbp;

    /*
     * A VBF has a single open/close state, but we have need for auto
     * opening/closing from multiple method invocations and an open/close
     * under getSourceTransport that may stay open beyond intermediate open/closes.
     * Manage auto open/close by tracking open and close request.
     *
     * Also, track every VBPMessage in a weak reference so that VBPs left
     * open by accident are automatically cleaned up.
     */
    //TODO reorg, move out, do something else, this is no good, cuz could be multi-ref to vbp outside of this class, so need to auto-close elsewhere
    private int opencount = 0;

    static private ReferenceQueue<VBPMessage> dead = new ReferenceQueue<VBPMessage>();
    static private HashMap<PhantomReference<VBPMessage>, VariableBlockPersistence> linkToDeadVBP = new HashMap<PhantomReference<VBPMessage>, VariableBlockPersistence>();
    static private HashMap<VariableBlockPersistence, PhantomReference<VBPMessage>> linkToPhantom = new HashMap<VariableBlockPersistence, PhantomReference<VBPMessage>>();

    static {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (true) {
                        Reference<? extends VBPMessage> phantom = dead.remove();

                        VariableBlockPersistence deadVBP = linkToDeadVBP.get(phantom);

                        try {
                            if (deadVBP.isOpen())
                                deadVBP.close();//make sure it's closed
                        } catch (StorageException e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                    }
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt(); //set flag
                }
            }
        }, "VBPMessage Auto Cleanup").start();
    }

    private void open() throws StorageException {
        synchronized (vbp) {
            if (opencount == 0) {
                vbp.open();

                if (linkToPhantom.get(vbp) == null) {// only one phantom per vbp needed
                    PhantomReference<VBPMessage> phantom = new PhantomReference<VBPMessage>(this, dead);
                    linkToDeadVBP.put(phantom, vbp);
                    linkToPhantom.put(vbp, phantom);
                }
            }

            ++opencount;
        }
    }

    private void close() throws StorageException {
        synchronized (vbp) {
            --opencount;

            if (opencount == 0) {
                vbp.close();

                //TODO remove phantom, still ok, but sloppy to leave it there.
            }

        }

    }

    /**
     * Create message from existing VariableBlockPersistence.
     *
     * @param variableBlockPersistence persistence containing message.
     */
    public VBPMessage(VariableBlockPersistence variableBlockPersistence) {
        this.vbp = variableBlockPersistence;

    }

    @Override
    public AddrSpec getReversePath() throws StorageException {
        open();

        try {
            return new AddrSpec(vbp.get(BlockType.REVERSEPATH));
        } catch (InvalidMailBoxException e) {
            throw new StorageException("Could not parse mailbox", e);
        } finally {
            close();
        }
    }

    @Override
    public AddrSpec[] getUndeliveredForwardPaths() throws StorageException {
        Map<LogicalIndex,byte[]> r = getForwardPaths();

        ArrayList<AddrSpec> list = new ArrayList<AddrSpec>();

        for (byte[] spath : r.values()) {
            if (spath[0] == '0') {
                try {
                    list.add(new AddrSpec(Arrays.copyOfRange(spath, 1, spath.length)));
                } catch (InvalidMailBoxException e) {
                    throw new StorageException("Could not parse mailbox", e);
                }
            }
        }

        return list.toArray(new AddrSpec[list.size()]);
    }

    private Map<LogicalIndex, byte[]> getForwardPaths() throws StorageException {
        open();

        try {
            HashMap<LogicalIndex, byte[]> r = new HashMap<LogicalIndex, byte[]>();

            int i = ByteBuffer.wrap(vbp.get(BlockType.FORWARDPATHCOUNT)).getInt();

            for (int j = 0; j < i; ++j) {
                ConstantIndex index = new ConstantIndex(BlockType.FIRSTFORWARDPATH.getIndex() + j);

                r.put(index, vbp.get(index));
            }

            return r;
        } finally {
            close();
        }
    }

    @Override
    public SourceTransport getSourceTransport() throws StorageException {
        return new SourceTransport() {

            @Override
            public ByteBuffer getMessageByteBuffer() throws IOException {
                try {
                    open();

                    //TODO for now, depending on auto-close, need some way to close deterministically, prolly w/ tracking transport or adding to interface.
                    return vbp.getByteBuffer(getDataLogicalIndex());
                } catch (StorageException e) {
                    throw new IOException(e);
                }

            }
        };
    }

    private LogicalIndex getDataLogicalIndex() throws StorageException {
        return new ConstantIndex(BlockType.FIRSTFORWARDPATH.getIndex() + getForwardPathCount());
    }

    private int getForwardPathCount() throws StorageException {
        return ByteBuffer.wrap(vbp.get(BlockType.FORWARDPATHCOUNT)).getInt();
    }

    @Override
    public void markForwardPathSent(AddrSpec forwardpath) throws StorageException {
        markForwardPath(forwardpath, (byte) '1');
    }

    private void markForwardPath(AddrSpec forwardpath, byte status) throws StorageException {
        Map<LogicalIndex,byte[]> r = getForwardPaths();

        for (LogicalIndex index : r.keySet()) {
            byte[] spath = r.get(index);

            try {
                AddrSpec a = new AddrSpec(Arrays.copyOfRange(spath, 1, spath.length));

                if (forwardpath.sameAs(a)) {
                    spath[0] = status;

                    open();

                    vbp.set(index, spath);

                    close();

                    return;
                }
            } catch (InvalidMailBoxException e) {
                throw new StorageException("Could not parse mailbox", e);
            }

        }

    }

    @Override
    public void markForwardPathUndeliverable(AddrSpec forwardpath, ClientProtocolException cause) throws StorageException {
        markForwardPath(forwardpath, (byte) '2');
    }

    @Override
    public DateTime getOriginalSpoolDate() throws StorageException {
        open();

        try {
            long instant = ByteBuffer.wrap(vbp.get(BlockType.RECEIVEDATE)).getLong();

            return new DateTime(instant);

        } finally {
            close();
        }
    }

    @Override
    public boolean hasMoreForwardPaths() throws StorageException {
        return getUndeliveredForwardPaths().length > 0;
    }

    public byte[] getData() throws StorageException {
        open();

        try {
            return vbp.get(new ConstantIndex(BlockType.FIRSTFORWARDPATH.getIndex() + getForwardPathCount()));
        } finally {
            close();
        }
    }

    public void delete() throws StorageException {
        vbp.delete();
    }
}
