/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.sal.vlp;

import me.yellowbird.mta.mps.StorageException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Variable Length File using Java File, io and nio to store a file in the operating system's file system.
 * <p/>
 * <bold>WARNING</bod>
 * This bad boy is not thread safe.
 */
public class FileVLF implements VariableLengthPersistence {
    static final String READYFILESUFFIX = "rdy";

    private File file;
    private FileChannel channel;

    /**
     * Create with the name of a file.
     * <p/>
     * The file identifies the underlying storage. However, as {@link File} denotes, the actual
     * file in the operating system's storage may not exists. No operations causing I/O are
     * performed on this file during construction of this object.
     *
     * @param file The file that identifies the name of the operating system's file containing the variable length file content.
     */
    FileVLF(File file) {
        this.file = file;
    }

//    @Override
//    public long getLength() {
//        return file.length();
//    }

//    @Override
//    public long getCurrentOffset() {
//        return 0;  //To change body of implemented methods use File | Settings | File Templates.
//    }

    @Override
    public void open() throws StorageException {
        RandomAccessFile raf;

        try {
            raf = new RandomAccessFile(file, "rw");
        } catch (FileNotFoundException e) {
            throw new StorageException("unable to open file", e);
        }

        channel = raf.getChannel();
    }

    @Override
    public void close() throws StorageException {
        if (channel != null) {
            try {
                channel.close();
            } catch (IOException e) {
                throw new StorageException("could not close file", e);
            } finally {
                channel = null;
            }
        }
    }

    @Override
    public void put(long offset, byte[] bytes) throws StorageException {
        try {
            channel.position(offset);

            channel.write(ByteBuffer.wrap(bytes));
        } catch (IOException e) {
            throw new StorageException("could not write bytes", e);
        }

    }

    @Override
    public void get(long offset, byte[] bytes) throws StorageException, UnderflowException {
        try {
            channel.position(offset);

            int count = channel.read(ByteBuffer.wrap(bytes));

            if (count != bytes.length)
                throw new UnderflowException();
        } catch (IOException e) {
            throw new StorageException("could not write bytes", e);
        }

        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isEmpty() throws StorageException {
        if (channel != null)
            try {
                return channel.size() == 0;
            } catch (IOException e) {
                throw new StorageException("could not get size from channel", e);
            }

        return file.length() == 0;
    }

    @Override
    public void delete() throws StorageException {
        if (!file.delete())
            throw new StorageException("unable to delete underlying file:" + file.getName());
    }

    @Override
    public void markReady() throws StorageException {
        File newname = new File(file.getParentFile(), file.getName()+READYFILESUFFIX);

        if (!file.renameTo(newname))
            throw new StorageException("could not mark file as ready");

        file = newname;
    }

    @Override
    public void copy(long src, long dst, long length) throws StorageException, UnderflowException {
        try {
            ByteBuffer buffer = ByteBuffer.allocate(4096);

            while(length > 0) {
                int count = channel.read(buffer, src);

                if (count < length && count < 4096)
                    throw new UnderflowException();

                buffer.flip();

                if (length < 4096) { //don't overwrite other data, we're done basically
                    buffer.limit((int) length);
                }

                channel.write(buffer, dst);

                length -= count;
                src += count;
                dst += count;
                buffer.clear();
            }

//            channel.transferTo(src, length, channel); can't copy to same channel, but this would be cool.
        } catch (IOException e) {
            throw new StorageException("could not copy bytes", e);
        }
    }

    @Override
    public ByteBuffer getByteBuffer(long offset, long length) throws StorageException {
        try {
            return channel.map(FileChannel.MapMode.READ_WRITE, offset, length);
        } catch (IOException e) {
            throw new StorageException("could not map channel", e);
        }
    }

    @Override
    public boolean isOpen() {
        return channel != null && channel.isOpen();

    }
}
