/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps;

import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.protocol.ClientProtocolException;
import me.yellowbird.mta.transport.SourceTransport;
import org.joda.time.DateTime;

/**
 * A Message.
 */
public interface Message {

    /**
     * Get the reverse-path for the message.
     *
     * @return The reverse-path from the original MAIL command.
     * @throws me.yellowbird.mta.mps.StorageException Thrown when the reverse-path cannot be retrieved from the spooled message.
     */
    AddrSpec getReversePath() throws StorageException;

    /**
     * Get forward paths not delivered.
     *
     * @return The addresses of forward paths that have not been delivered the message or an empty array if none left.
     * @throws me.yellowbird.mta.mps.StorageException Thrown when the forward paths cannot be retrieved from the spooled message.
     */
    AddrSpec[] getUndeliveredForwardPaths() throws StorageException;

    /**
     * Get a SourceTransport for data in this message.
     *
     * @return A SourceTransport where the next byte is the first byte of data in the message.
     * @throws me.yellowbird.mta.mps.StorageException Thrown when the {@link me.yellowbird.mta.transport.SourceTransport} cannot be created from the spooled message.
     */
    SourceTransport getSourceTransport() throws StorageException;

    /**
     * Mark forward path as being delivered.
     *
     * @param forwardpath the forward path delivered.
     * @throws me.yellowbird.mta.mps.StorageException Thrown when unable to mark the forward path. The state of the mark is unknown.
     */
    void markForwardPathSent(AddrSpec forwardpath) throws StorageException;

    /**
     * Mark forward path as being undeliverable.
     *
     * @param forwardpath the forward path where the message cannot be delivered.
     * @param cause exception containing cause for failure.
     * @throws me.yellowbird.mta.mps.StorageException Thrown when unable to mark the forward path. The state of the mark is unknown.
     */
    void markForwardPathUndeliverable(AddrSpec forwardpath, ClientProtocolException cause) throws StorageException;

    /**
     * Return the original date when the message was spooled.
     *
     * @return The date when the message was received by the spool system.
     * @throws me.yellowbird.mta.mps.StorageException Thrown when the date cannot be retrieved from the spooled message.
     */
    DateTime getOriginalSpoolDate() throws StorageException;

    /**
     * Are there more forward paths not delivered.
     * @return true if this message has not been delivered to all forward paths.
     * @throws me.yellowbird.mta.mps.StorageException Thrown when the state of the forward paths cannot be retrieved from the spooled message.
     */
    boolean hasMoreForwardPaths() throws StorageException;
}
