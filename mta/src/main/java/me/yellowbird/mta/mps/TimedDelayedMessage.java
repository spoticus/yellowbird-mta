/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps;

import me.yellowbird.mta.spooler.DelayedMessage;

import java.util.Date;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * A message to be forwarded after a delay expires.
 */
public class TimedDelayedMessage implements DelayedMessage {
    private final SpooledMessage message;
    private final long created = new Date().getTime();
    private final long delay;
    private final TimeUnit unit;

    /**
     * Add message to forward with no delay.
     *
     * @param message the message to forward.
     */
    public TimedDelayedMessage(SpooledMessage message) {
        this.message = message;
        this.delay = 0;
        this.unit = TimeUnit.SECONDS;
    }

    /**
     * Add message to forward with delay with specific time unit.
     *
     * @param message the message to forward.
     * @param delay amount of delay.
     * @param unit unit of measure for the delay.
     */
    public TimedDelayedMessage(SpooledMessage message, long delay, TimeUnit unit) {
        this.message = message;
        this.delay = delay;
        this.unit = unit;
    }

    @Override
    public long getDelay(TimeUnit unit) {
        long remmilli = this.unit.toMillis(delay) - (new Date().getTime() - created);

        return unit.convert(remmilli, TimeUnit.MILLISECONDS);
    }

    @Override
    public int compareTo(Delayed o) {
        long diff = getDelay(TimeUnit.MILLISECONDS) - o.getDelay(TimeUnit.MILLISECONDS);

        if (diff < 0)
            return -1;

        if (diff > 0)
            return 1;

        return 0;
    }

    public SpooledMessage getMessage() {
        return message;
    }

    @Override
    public SpooledMessage getSpooledMessage() {
        return message;
    }
}
