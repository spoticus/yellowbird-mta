/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.sal.vbp;

import me.yellowbird.mta.mps.StorageException;
import me.yellowbird.mta.mps.sal.vlp.UnderflowException;
import me.yellowbird.mta.mps.sal.vlp.VariableLengthPersistence;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * A Variable Block File using format version 1.0.
 *
 * @see {me.yellowbird.mta.mps.sal}
 */
public class VBFv10 implements VariableBlockPersistence {
    private static final long FIRSTBLOCK = 16;
    private static final int BLOCKHEADERLENGTH = 20;

    private final VariableLengthPersistence vlf;

    /**
     * Open an existing or create a new Variable Block File.
     * <p/>
     * Creates a Variable Block File for a {@linkplain File}.
     *
     * @param vlf File containing variable block file.
     */
    public VBFv10(VariableLengthPersistence vlf) {
        this.vlf = vlf;
    }

    @Override
    public void set(LogicalIndex index, byte[] value) throws StorageException {
        BlockInfo bh = walkIndexToBlockHeader(index);

        if (bh == null) {
            allocate(index, value);
        } else if (bh.getBlockLength()-BLOCKHEADERLENGTH >= value.length) {
            assign(bh, index, value);
        } else {
            reAllocate(bh, value);
        }
    }

    /*
     * Re-allocate the block, assigning the value to a new block, and freeing up old block for re-use.
     */

    private void reAllocate(BlockInfo tooSmallBlock, byte[] newValue) throws StorageException {
        BlockInfo newBlock = walkFreeBlocksToBlockHeader(newValue.length);

        newBlock.setBlockLength(newValue.length + BLOCKHEADERLENGTH);
        newBlock.setLogicalIndex(tooSmallBlock.getLogicalIndex());
        newBlock.setContentLength(newValue.length);

        tooSmallBlock.setLogicalIndex(LogicalIndex.PreDefined.UNUSED);

        vlf.put(tooSmallBlock.getBlockOffset(), tooSmallBlock.getBytes());

        vlf.put(newBlock.getBlockOffset(), newBlock.getBytes());

        vlf.put(newBlock.getBlockOffset()+BLOCKHEADERLENGTH, newValue);
    }

    /*
     * Assign the block the value assuming the block is large enough to contain the value.
     */

    private void assign(BlockInfo reuseBlock, LogicalIndex newIndex, byte[] newValue) throws StorageException {
        reuseBlock.setLogicalIndex(newIndex);
        reuseBlock.setContentLength(newValue.length);

        vlf.put(reuseBlock.getBlockOffset(), reuseBlock.getBytes());

        vlf.put(reuseBlock.getBlockOffset()+BLOCKHEADERLENGTH, newValue);
    }

    /*
     * Allocate space for a new block and assign value assuming logic index value is unique.
     */

    private void allocate(LogicalIndex index, byte[] value) throws StorageException {
        BlockInfo bi = walkFreeBlocksToBlockHeader(value.length);

        if (bi.getBlockLength() == 0) { //writing new block to end of file
            bi.setBlockLength(value.length + BLOCKHEADERLENGTH);
            bi.setLogicalIndex(index);
            bi.setContentLength(value.length);
        } else {
            bi.setLogicalIndex(index);
            bi.setContentLength(value.length);
        }

        vlf.put(bi.getBlockOffset(), bi.getBytes());

        vlf.put(bi.getBlockOffset()+BLOCKHEADERLENGTH, value);
    }

    @Override
    public byte[] get(LogicalIndex index) throws IndexOutOfBoundsException, StorageException, ContentTooLargeException, StructureException {
        BlockInfo bi = walkIndexToBlockHeader(index);

        if (bi == null)
            throw new IndexOutOfBoundsException("index not found:" + index.getIndex());

        if (bi.getContentLength() > Integer.MAX_VALUE)
            throw new ContentTooLargeException("block content size:" + bi.getContentLength());

        byte[] b = new byte[(int) bi.getContentLength()];

        try {
            vlf.get(bi.getBlockOffset() + BLOCKHEADERLENGTH, b);
        } catch (UnderflowException e) {
            throw new StructureException("value of block does not exists in file", e);
        }

        return b;
    }

    @Override
    public void append(LogicalIndex logicalIndex, byte[] value) throws StorageException {
        /*
         * If block does not exists then allocate as a new block.
         */
        BlockInfo bi = walkIndexToBlockHeader(logicalIndex);

        if (bi == null) {
            allocate(logicalIndex, value);

            return;
        }

        /*
         * Expand block if necessary. append data.
         */
        if (bi.getFreeLength() < value.length) {
            try {
                bi = expand(bi, value.length - bi.getFreeLength());
            } catch (UnderflowException e) {
                throw new StructureException("could not properly expand block, headers probably corrupt", e);
            }
        }

        vlf.put(bi.getFreeOffset(), value);

        bi.setContentLength(bi.getContentLength() + value.length);
        vlf.put(bi.getBlockOffset(), bi.getBytes());
    }

    @Override
    public ByteBuffer getByteBuffer(LogicalIndex logicalIndex) throws IndexOutOfBoundsException, StorageException, ContentTooLargeException, StructureException {
        BlockInfo bi = walkIndexToBlockHeader(logicalIndex);

        if (bi == null)
            throw new IndexOutOfBoundsException("index not found:" + logicalIndex.getIndex());

//        try {
            return vlf.getByteBuffer(bi.getBlockOffset() + BLOCKHEADERLENGTH, bi.getContentLength());
//        } catch (UnderflowException e) {
//            throw new StructureException("could not properly expand block, headers probably corrupt", e);
//        }
    }

    private BlockInfo expand(BlockInfo tooSmallBlock, long additionalSize) throws StorageException, UnderflowException {
        if (tooSmallBlock.isLastBlock()) {
            tooSmallBlock.setBlockLength(tooSmallBlock.getBlockLength() + additionalSize);

            vlf.put(tooSmallBlock.getBlockOffset(), tooSmallBlock.getBytes());

            return tooSmallBlock;
        } else {
            BlockInfo newBlock = walkFreeBlocksToBlockHeader(tooSmallBlock.getContentLength() + additionalSize);

            newBlock.setBlockLength(tooSmallBlock.getContentLength() + additionalSize + BLOCKHEADERLENGTH);
            newBlock.setLogicalIndex(tooSmallBlock.getLogicalIndex());
            newBlock.setContentLength(tooSmallBlock.getContentLength());

            tooSmallBlock.setLogicalIndex(LogicalIndex.PreDefined.UNUSED);

            vlf.put(tooSmallBlock.getBlockOffset(), tooSmallBlock.getBytes());

            vlf.put(newBlock.getBlockOffset(), newBlock.getBytes());

            vlf.copy(tooSmallBlock.getBlockOffset()+BLOCKHEADERLENGTH, newBlock.getBlockOffset()+BLOCKHEADERLENGTH, tooSmallBlock.getContentLength());

            return newBlock;
        }
    }

    @Override
    public void delete() throws StorageException {
        vlf.delete();
    }

    @Override
    public void markReady() throws StorageException {
        vlf.markReady();
    }

    /*
     * Walk through all the indexes until the one identified is found or return null if it doesn't exist.
     */

    private BlockInfo walkIndexToBlockHeader(LogicalIndex index) throws StorageException {
        long currentblock = FIRSTBLOCK;

        do {
            byte[] blockdata = new byte[BLOCKHEADERLENGTH];

            try {
                vlf.get(currentblock, blockdata);
            } catch (UnderflowException e) {
                return null;
            }

            BlockInfo bi = new BlockInfo(currentblock, blockdata);

            if (bi.getLogicalIndex().getIndex() == index.getIndex()) {
                return bi;
            }

            currentblock += bi.getBlockLength();
        } while (true);
    }

    /*
     * Walk through all blocks until a free one is found that can hold a size of data.
     */
    private BlockInfo walkFreeBlocksToBlockHeader(long size) throws StorageException {
        long currentblock = FIRSTBLOCK;

        do {
            byte[] blockdata = new byte[BLOCKHEADERLENGTH];

            try {
                vlf.get(currentblock, blockdata);
            } catch (UnderflowException e) {
                return new BlockInfo(currentblock);//at end of file
            }

            BlockInfo bi = new BlockInfo(currentblock, blockdata);

            if (bi.getLogicalIndex() == LogicalIndex.PreDefined.UNUSED && bi.getBlockLength()-BLOCKHEADERLENGTH <= size)
            return bi;

            currentblock += bi.getBlockLength();
        } while (true);
        
    }

    @Override
    public void open() throws StorageException, StructureException {
        vlf.open();

        if (vlf.isEmpty()) {
            vlf.put(0, makeBytes("mtavbf0100      "));
            return;
        }

        byte[] signature = new byte[16];

        try {
            vlf.get(0, signature);
        } catch (UnderflowException e) {
            throw new StructureException("signature missing");
        }

        if (Arrays.equals(signature, makeBytes("mtavbf0100      ")))
            return;

        throw new StructureException("signature does not match");

    }

    @Override
    public void close() throws StorageException {
        vlf.close();
    }

    @Override
    public boolean isOpen() throws StorageException {
        return vlf.isOpen();
    }

    /**
     * Block information.
     */
    public class BlockInfo extends BlockHeader {
        private long blockOffset;

        /**
         * Create block information.
         *
         * @param blockOffset Offset to beginning of block.
         * @param blockdata Header bytes.
         */
        public BlockInfo(long blockOffset, byte[] blockdata) {
            super(blockdata);

            this.blockOffset = blockOffset;
        }

        /**
         * Create block information for end of file.
         *
         * @param blockOffset Offset to beginning of block.
         */
        public BlockInfo(long blockOffset) {
            super(0, LogicalIndex.PreDefined.UNUSED, 0);

            this.blockOffset = blockOffset;
        }

        public long getBlockOffset() {
            return blockOffset;
        }

//        public BlockHeader getBlockHeader() {
//            return blockHeader;
//        }

//        public void setBlockHeader(BlockHeader blockHeader) {
//            this.blockHeader = blockHeader;
//        }

        public boolean isLastBlock() throws StorageException {
            // see if there is another byte after this block

            byte[] b = new byte[1];

            try {
                vlf.get(getBlockOffset() + getBlockLength(), b);
            } catch (UnderflowException e) {
                return true;
            }

            return false;
        }

        /**
         * Total capacity of content area.
         *
         * @return The number of bytes that exists in the content area of the block, regardless of content length.
         */
        public long getCapacity() {
            return getBlockLength() - BLOCKHEADERLENGTH;
        }

        /**
         * Get offset to content.
         *
         * @return Offset from beginning of vlp to content.
         */
        public long getContentOffset() {
            return getBlockOffset() + BLOCKHEADERLENGTH;
        }

        /**
         * Length of free space in block.
         *
         * @return The number of bytes unused between the end of the content and the end of the block.
         */
        public long getFreeLength() {
            return getBlockLength() - BLOCKHEADERLENGTH - getContentLength();
        }

        /**
         * Get offset to free space in block.
         *
         * @return The offset from beginning of vlp to the first free byte after the content.
         */
        public long getFreeOffset() {
            return getBlockOffset() + BLOCKHEADERLENGTH + getContentLength();
        }
    }

    /**
     * A block header.
     */
    static public class BlockHeader {
        private long blockLength;
        private LogicalIndex logicalIndex;
        private long contentLength;

        /**
         * Construct from header bytes in persistent storage format.
         *
         * @param blockdata Header bytes.
         */
        public BlockHeader(byte[] blockdata) {
            ByteBuffer bb = ByteBuffer.wrap(blockdata);

            blockLength = bb.getLong();
            logicalIndex = new ConstantIndex(bb.getInt());
            contentLength = bb.getLong();
        }

        /**
         * Construct a header with specific index and length.
         *
         * @param logicalIndex Logical index for block.
         * @param contentLength Length of content that will be in block.
         */
        public BlockHeader(long blockLength, LogicalIndex logicalIndex, long contentLength) {
            this.blockLength = blockLength;
            this.logicalIndex = logicalIndex;
            this.contentLength = contentLength;
        }

//        public int getIndex() {
//            return logicalIndex;
//        }

        public long getBlockLength() {
            return blockLength;
        }

        public void setBlockLength(long blockLength) {
            this.blockLength = blockLength;
        }


        public long getContentLength() {
            return contentLength;
        }

        public void setLogicalIndex(LogicalIndex logicalIndex) {
            this.logicalIndex = logicalIndex;
        }

        public LogicalIndex getLogicalIndex() {
            return logicalIndex;
        }

        public void setContentLength(long contentLength) {
            this.contentLength = contentLength;
        }

//        public void setLogicalIndex(LogicalIndex index) {
//            this.logicalIndex = index.getIndex();
//        }

        public byte[] getBytes() {
            byte[] b = new byte[BLOCKHEADERLENGTH];

            ByteBuffer bb = ByteBuffer.wrap(b);

            bb.putLong(blockLength);
            bb.putInt(logicalIndex.getIndex());
            bb.putLong(contentLength);

            return b;
        }
    }

    /**
     * Convert String to supported external byte format.
     *
     * @param s The string converted to the external byte format.
     * @return The string in the external format.
     */
    static public byte[] makeBytes(String s) {
        try {
            return s.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

}
