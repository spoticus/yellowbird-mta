/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.vbm;

import me.yellowbird.mta.mps.PersistedMessageQueue;
import me.yellowbird.mta.mps.SpooledMessage;
import me.yellowbird.mta.mps.StorageException;
import me.yellowbird.mta.mps.TimedDelayedMessage;
import me.yellowbird.mta.mps.sal.vbp.VariableBlockPersistence;

import java.util.concurrent.TimeUnit;

/**
 * A spooled message stored in a variable block persistence.
 */
public class VBPSpooledMessage extends VBPMessage implements SpooledMessage {
    private PersistedMessageQueue persistedMessageQueue;

    /**
     * Create message from existing VariableBlockPersistence.
     *
     * @param variableBlockPersistence persistence containing message.
     */
    public VBPSpooledMessage(PersistedMessageQueue persistedMessageQueue, VariableBlockPersistence variableBlockPersistence) {
        super(variableBlockPersistence);

        this.persistedMessageQueue = persistedMessageQueue;
    }

    @Override
    public void requeueWithDelay(long delay, TimeUnit unit) throws StorageException {
        persistedMessageQueue.relay(new TimedDelayedMessage(this, delay, unit));
    }

    @Override
    public void delete() throws StorageException {
        super.delete();
    }

//    @Override
//    public SpooledMessage createGiveUpDSN() {
//        return null;  //To change body of implemented methods use File | Settings | File Templates.
//    }

}
