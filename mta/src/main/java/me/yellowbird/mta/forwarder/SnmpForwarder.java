/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.forwarder;

import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.brain.Brain;
import me.yellowbird.mta.mps.SpooledMessage;
import me.yellowbird.mta.mps.StorageException;
import me.yellowbird.mta.protocol.ClientProtocol;
import me.yellowbird.mta.protocol.ClientProtocolException;
import me.yellowbird.mta.spooler.Spooler;
import me.yellowbird.mta.spooler.SpoolerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Sends a message to the next MTA.
 * <p/>
 * A forwarder request a message from the spooler. When one is available it is
 * forwarded to the next relay for the a forward path that has not received the
 * message.
 * <p/>
 * The spooler is given back the message after the forwarder is finished forwarding
 * the message or if the forward could not be completed.
 */
public class SnmpForwarder implements Runnable {
    Logger logger = LoggerFactory.getLogger("forwarder");

    private Spooler spooler;
    private Brain brain;

    /**
     * Create a forwarder associated with a file spooler.
     *
     * @param spooler The spooler that has dominion over the spool directory containing
     *                the files this forwarder will transmit.
     * @param brain   The brain.
     */
    public SnmpForwarder(Spooler spooler, Brain brain) {
        this.spooler = spooler;
        this.brain = brain;
    }

    /**
     * Wait for files to come available in the spooler and send them.
     */
    public void run() {
        while (true) {
            SpooledMessage file;

            try {

                file = spooler.takeFileToForward();

            } catch (InterruptedException e) {
                logger.warn("Forwarder has been interrupted and is shutting down");
                Thread.currentThread().interrupt();
                return;
            }

            try {
                try {

                    process(file);

                    spooler.requeueWithDelay(file);

                } catch (IOException e) {
                    logger.error("A spool file could not be read for an operating system problem and will be re-queued:" + file, e);
                    spooler.requeueWithDelay(file);
                } catch (InterruptedException e) {
                    logger.warn("Forwarder has been interrupted and is shutting down");
                    Thread.currentThread().interrupt();
                    return;
                } catch (RuntimeException e) {
                    logger.error("Unexpected error, requeuing message", e);
                    spooler.requeueWithDelay(file);
                } catch (StorageException e) {
                    logger.error("A problem with message and will be re-queued:" + file, e);
                    spooler.requeueWithDelay(file);
                }
            } catch (SpoolerException e) {
                logger.error("Unable to requeue the message, giving up on this message:" + file, e);
            }

        }
    }


    /**
     * Send spooledMessage to next forward-path
     *
     * @param spooledMessage the spooledMessage to forward.
     * @throws java.io.IOException  thrown when IO error in process local spooledMessage file.
     * @throws InterruptedException assume process is being aborted.
     * @throws me.yellowbird.mta.mps.StorageException
     *                              a problem with the spooledMessage was encountered.
     */
    private void process(SpooledMessage spooledMessage) throws IOException, InterruptedException, StorageException {
        AddrSpec reversepath = spooledMessage.getReversePath();

        for (AddrSpec forwardpath : spooledMessage.getUndeliveredForwardPaths()) {

            ClientProtocol protocol = brain.createClientProtocol(reversepath, forwardpath);

            try {
                protocol.send(spooledMessage.getSourceTransport());
                spooledMessage.markForwardPathSent(forwardpath);
            } catch (ClientProtocolException e) {
                if (!e.recoverable())
                    spooledMessage.markForwardPathUndeliverable(forwardpath, e);
                else
                    logger.warn(e.getMessage());//normal process event, no stack trace
            }
        }

    }
}
