/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta;

import me.yellowbird.mta.protocol.LineData;
import me.yellowbird.mta.protocol.LineDataException;

import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * An RFC2822 Address Specification
 * <p/>
 * A RFC2822 3.4 describes the format of addresses allowing for groups of addresses
 * and other information such as a display name. Section 3.4.1 describes an addr-spec, which
 * is the identifier for a mailbox and is used for routing. This class represents
 * that mailbox identifier, the addr-spec.
 */

public class AddrSpec {
    //RFC2821,RFC2822
    //group1=localpart
    //group2=domain
    static final Pattern MAILBOX_PATTERN = Pattern.compile(
            "(?:   #Mailbox\n" +
                    "(   #Local part\n" +
                    "(?:                                 #dot-string\n" +
                    "(?:(?<!<|:)\\.)?                     #dot separates atext after first\n" +
                    "[a-zA-Z0-9\\#!$%&'*+/=?^_`\\{|}~-]+    #atext\n" +
                    ")+                                  #end dot-string\n" +
                    "|                                   #or\n" +
                    "(?:                                 #quoted-string\n" +
                    "\"\n" +
                    "(?:                                                    #qcontent\n" +
                    "[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x7f\\x21\\x23-\\x5b\\x5d-\\x7e] #qtext\n" +
                    "|                                                      #or\n" +
                    "(?:\\\\[\\x00-\\x7f])                                      #quoted-pair\n" +
                    ")+\n" +
                    "\"\n" +
                    ")                                   #end quoted-string\n" +
                    ")    #end Local part\n" +
                    "@\n" +
                    "(           #domain\n" +
                    "(?:[a-zA-Z0-9][a-zA-Z0-9-]*(?:\\.[a-zA-Z0-9][a-zA-Z0-9-]+)*)      #sub-domains\n" +
                    "|                                         #or\n" +
                    "(?:\\[\\d{1,3}\\.[^\\]]+\\])                   #IPv4-address-literal (prefix match only)\n" +
                    "|                                         #or\n" +
                    "(?:\\[IPv6:[^\\]]+\\])                       #IPv6-address-literal (prefix match only)\n" +
                    "|                                         #or\n" +
                    "(?:\\[[a-zA-Z0-9-]*[a-zA-Z0-9]:[^\\]]+\\])   #General-address-literal\n" +
                    ")             #end domain\n" +
                    ")   #end Mailbox\n",
            Pattern.COMMENTS
    );

    private final Matcher mailboxparts;
    private final boolean nulladdress;

    /**
     * Construct from a string containing a valid addr-spec.
     *
     * @param mailbox string containing addr-spec. if the string length is zero then construct a null address.
     * @throws InvalidMailBoxException the format of the string is not a mailbox address.
     */
    public AddrSpec(String mailbox) throws InvalidMailBoxException {
        if (mailbox.length() == 0) {
            mailboxparts = null;
            nulladdress = true;

        } else {
            mailboxparts = MAILBOX_PATTERN.matcher(mailbox);

            if (!mailboxparts.matches())
                throw new InvalidMailBoxException(mailbox);

            nulladdress = false;
        }
    }

    /**
     * Construct from byte array containing ascii valid addr-sec.
     *
     * @param mailbox array containing addr-spec. if the array length is zero then construct a null address.
     * @throws InvalidMailBoxException the format of the string is not a mailbox address.
     */
    public AddrSpec(byte[] mailbox) throws InvalidMailBoxException {
        if (mailbox.length == 0) {
            mailboxparts = null;
            nulladdress = true;

        } else {
            String s = null;
            try {
                s = ASCIIString.decode(ByteBuffer.wrap(mailbox));
            } catch (CharacterCodingException e) {
                throw new InvalidMailBoxException(mailbox);
            }

            mailboxparts = MAILBOX_PATTERN.matcher(s);

            if (!mailboxparts.matches())
                throw new InvalidMailBoxException(s);

            nulladdress = false;
        }
    }

    public String getLocalPart() {
        if (nulladdress)
            return "";

        return mailboxparts.group(1);
    }

    public String getDomain() {
        if (nulladdress)
            return "";

        return mailboxparts.group(2);
    }

    public LineData toLineData() {
        try {
            if (nulladdress)
                return new LineData("");

            return new LineData(mailboxparts.group(0));
        } catch (LineDataException e) {
            throw new RuntimeException("regex in this class is not match ascii only");
        }
    }

    /**
     * Determine if this contains an address that is the same as another.
     *
     * Lexically compares the address contained by this object with the another.
     *
     * @param addr The address to compare with this address.
     * @return true if both are the null address or if both are not the null address and the address parts are lexically identical.
     */
    public boolean sameAs(AddrSpec addr) {
        if (nulladdress || addr.nulladdress)
            return nulladdress && addr.nulladdress;

        return mailboxparts.group(0).equals(((AddrSpec) addr).mailboxparts.group(0));

    }

    public boolean isNullAddress() {
        return nulladdress;
    }
}
