/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.protocol;


import me.yellowbird.mta.protocol.LineData;

import java.util.ArrayList;
import java.util.Iterator;
import java.nio.ByteBuffer;

/**
 * An SMTP reply requiring multiple lines
 */
public class MultiLineReply implements Iterable<LineData> {
    private ArrayList<LineData[]> replies = new ArrayList<LineData[]>();

    public MultiLineReply(LineData replycode, LineData message) {
        replies.add(new LineData[]{replycode, message});
    }

    public void add(LineData replycode, LineData message) {
        replies.add(new LineData[]{replycode, message});
    }

    /**
     * Return number of replies
     * @return number of replies
     */
    public int size() {
        return replies.size();
    }

    public Iterator<LineData> iterator() {
        return new Iterator<LineData>() {
            private int next=0;

            public boolean hasNext() {
                return next < replies.size();
            }

            public LineData next() {
                LineData[] data = replies.get(next++);

                if (next < replies.size())
                    return data[0].concat("-").concat(data[1]);

                return data[0].concat(" ").concat(data[1]);
            }

            public void remove() {

            }
        };
    }

    public byte[] toByteArray() {

        int size = 0;

        for (LineData d : this)
            size += d.transmitLength();

        ByteBuffer data = ByteBuffer.allocate(size);

        for (LineData d : this)
            data.put(d.toTransmitByteArray());

        return data.array();//allocation exactly right size 
    }
}
