/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.protocol;

import me.yellowbird.mta.ASCIIString;


/**
 * An exception in RFC2821 line data
 */
public class LineDataException extends Throwable {
    /**
     * Construct from message and string that has a problem.
     *
     * @param message the detail message.
     * @param asciistring the string having a problem.
     */
    public LineDataException(String message, ASCIIString asciistring) {
        super(message);
    }

    /**
     * Construct from message and string that has a problem.
     *
     * @param message the detail message.
     * @param string the string having a problem.
     */
    public LineDataException(String message, String string) {
        super(message);
    }

    /**
     * Construct from message and byte array containing problem.
     *
     * @param message the detail message.
     * @param data array containg data having a problem.
     * @param offset location in array where problem was encountered.
     */
    public LineDataException(String message, byte[] data, int offset) {
        super(message);
    }
}
