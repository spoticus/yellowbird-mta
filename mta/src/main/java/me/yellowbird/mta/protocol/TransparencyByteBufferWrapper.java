/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.protocol;

import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.io.IOException;

/**
 * Wrapper to add transparency to message data in a ByteBuffer.
 * <p/>
 * According to RFC2821, lines being with a period (".") must me modified to make the period
 * transparent to the protocol, since this indicates the end of the message to the protocol.
 */
public class TransparencyByteBufferWrapper implements ReadableByteChannel {

    private final ByteBuffer src;

    private final ByteBuffer transparentized;
    private final ByteBuffer copytarget;

    private final ByteBuffer eom;

    public TransparencyByteBufferWrapper(ByteBuffer src) {
        this.src = src;
        transparentized = ByteBuffer.allocate(12000);//there is a reason
        copytarget = ByteBuffer.wrap(transparentized.array(), transparentized.arrayOffset(), transparentized.limit() - 2000);

        transparentized.position(transparentized.limit());//force init copy

        eom = ByteBuffer.wrap(new byte[] {'.','\r','\n'});
    }

    @Override
    public int read(ByteBuffer dst) throws IOException {
        if (transparentized.remaining() == 0)
            transparentize();

        if (transparentized.remaining() == 0) {
            if (eom.remaining() == 0) {
                return -1;//no more data
            } else {
                int p = eom.position();
                dst.put(eom);//write eom till it's all gone
                return eom.position() - p;
            }
        }

        int i = dst.position();

        dst.put(transparentized);

        return dst.position() - i;
    }

    //never call unless transparentized has nothing remaining
    private void transparentize() {
        if (src.remaining() == 0)
            return;

        copytarget.clear();

        copytarget.put(src);
        copytarget.flip();

        byte[] a = copytarget.array();
        int b = copytarget.arrayOffset();
        int c = copytarget.arrayOffset() + copytarget.limit();

        for (int i = b; i < c; ++i) {
            if (a[i] == '\r' && a[i+1] == '\n' && a[i+2] == '.' && a[i+3] == '\r' && a[i+4] =='\n') {
                System.arraycopy(a, i+3, a, i+4, c-i+3);
                a[i+3] = '.';
                ++c;
            }
        }

        transparentized.position(0);
        transparentized.limit(c - b);
    }

    @Override
    public boolean isOpen() {
        return true;
    }

    @Override
    public void close() throws IOException {
    }
}