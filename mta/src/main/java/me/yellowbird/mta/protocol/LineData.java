/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.protocol;

import me.yellowbird.mta.ASCIIString;

import java.util.regex.Pattern;

/**
 * Data used in a command or message data
 * <p>
 * A line is zero or more ascii data characters terminted by CRLF. Lines are the basic transmission unit
 * in smtp. This class represents the data portion of a line and does not contain the CRLF pair. Instances
 * may represent all of a line's data or just a portion. The purpose of this class is to guarantee that
 * data for a line is ASCII and does not contain embedded CRLF pairs.
 * 
 * @see <a href="http://tools.ietf.org/html/rfc2821#section-2.3.7">Lines</a> 
 */
public class LineData extends ASCIIString {
    private static final Pattern CRLF_PATTERN = Pattern.compile("\\r\\n");
    private static final Pattern NOT_ASCII_PATTERN = Pattern.compile("[^\\p{ASCII}]");

    /**
     * Strip invalid characters from string and convert to LineData.
     *<p>
     * A convenience for converting string constants to LineData.
     *
     * @param string non-ascii characters and embedded CRLF pairs are removed from this.
     * @return LineData from stripped input string.
     */
    public static LineData strip(String string) {
        String s = CRLF_PATTERN.matcher(string).replaceAll("");

        s = NOT_ASCII_PATTERN.matcher(s).replaceAll("");

        return new LineData(s, true);
    }

    private LineData(String s, boolean marker) {
        super(s);
    }

    /**
     * Construct from String containing data characters.
     * <p>
     * The string must contain characters that can map to ascii and must not contain
     * CRLF.
     * @param string the line data characters.
     * @throws me.yellowbird.mta.ASCIIException if the content of the string contains non-ascii characters or
     * CRLF pair.
     * @throws LineDataException When the string contains an embedded CRLF pair
     */
    public LineData(String string) throws LineDataException {
        super(string);

        if (CRLF_PATTERN.matcher(string).find())
            throw new LineDataException("String contains embedded CRLF pair", string);
        
    }

    public LineData(ASCIIString asciistring) throws LineDataException {
        super(asciistring);

        if (CRLF_PATTERN.matcher(asciistring.toString()).find())
            throw new LineDataException("String contains embedded CRLF pair", asciistring);
    }

    public LineData(LineData linedata, String string) {
        super(linedata, string);
    }

    public LineData(LineData leftlinedata, LineData rightlinedata) {
        super(leftlinedata, rightlinedata);
    }

    /**
     * Concatenate this LineData string with a java String
     * @param string String appended to the value of this string.
     * @return A new LineData
     */
    public LineData concat(String string) {
        return new LineData(this, string);
    }

    public LineData concat(LineData linedata) {
        return new LineData(this, linedata);
    }

    public byte[] toTransmitByteArray() {
        byte[] b = new byte[length() + 2];
        toByteArray(b);
        b[b.length-2] = '\r';
        b[b.length-1] = '\n';

        return b;
    }

    public int transmitLength() {
        return length() + 2;
    }

}
