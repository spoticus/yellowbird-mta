/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.protocol;

import me.yellowbird.mta.ASCIIString;
import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.brain.Brain;
import me.yellowbird.mta.io.DataLink;
import me.yellowbird.mta.io.DataLinkProblem;
import me.yellowbird.mta.io.Protocol;
import me.yellowbird.mta.io.ProtocolAsynchronizer;
import me.yellowbird.mta.nio.ChamillionSocketTransport;
import me.yellowbird.mta.transport.SourceTransport;
import org.xbill.DNS.TextParseException;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * RFC2821 Client-side protocol for relaying message to external MTA.
 *
 * A thread enters the send method and controls the transmission of data
 * to the remote client. When the thread needs a response it waits for
 * the data, which comes from a separate through through the DataLink
 * provided after the initial connection. This is perfectly satisfactory
 * behavior for snmp, which is a hand-shaking protocol.
 * <p>
 * Cancellation from lower on the protocol stack, through the DataLink, is
 * a bit tricky. The ClientProtocol may be in one of three states:
 *
 * <ul>
 * <li>It is busy sending data or thinking about sending data.
 * <li>It is waiting for data from the DataLink by blocking on a queue.
 * <li>It's already given up control of the thread and returned from the send method.
 * </ul>
 *
 * The DataLink sends data through a queue and this is the only place
 * in the ClientProtocol where there is blocking. In the case of an
 * exception that requires the ClientProtocol to abort, the DataLink sets
 * an exception message then puts on the queue zero-length data. If
 * the ClientProtocol thread is blocking on the queue then it will continue.
 * <p>
 * The ClientProtocol looks to this exception message before and after blocking
 * and at other strategic times, but always the effect is to quit. In this way
 * the ClientProtocol is thread safe and the lower level protocol, through
 * the DataLink, able to abort the ClientProtocol. As a final note, if the
 * ClientProtocol has already aborted -- it has returned from the send
 * method -- this activity will have no negative impact on the thread, since
 * ClientProtocol instances are never reused.
 */
public class ClientProtocol implements Protocol {
    public static void send() {}

    private final AddrSpec reversepath;
    private final AddrSpec forwardpath;
    private final Protocol proxy;
    private final Brain brain;
    private boolean sentalreadyused = false;//has the send method already been called.

    private ArrayList<ClientProtocolException> failurereasons = new ArrayList<ClientProtocolException>();
    private DataLink dataLink;
    private LinkedBlockingQueue<ByteBuffer> replies = new LinkedBlockingQueue<ByteBuffer>();// replies from MTA

    private boolean asyncInterrupt = false;//the datalink is request shutdown of protocol stack
    private DataLinkProblem asyncExceptionProblem = null;
    private ByteBuffer savebuffer = null;//left-over data from remote client, not fully a line. normal compacted and ready for more.
    private SourceTransport transport;

    private enum State {
        CONNECTWAIT,
        BANNERWAIT,
        HELORESPONSEWAIT,
        MAILRESPONSEWAIT,
        RCPTRESPONSEWAIT,
        DATARESPONSEWAIT,
        DISCONNECT,
        MSGDATARESPONSEWAIT
    }

    private State state;

    public ClientProtocol(AddrSpec reversepath, AddrSpec forwardpath, ProtocolAsynchronizer ioasynchronizer, Brain brain) {
        this.reversepath = reversepath;
        this.forwardpath = forwardpath;
        proxy = ioasynchronizer.createProxy(this);
        this.brain = brain;
    }

    public void send(SourceTransport transport) throws ClientProtocolException, InterruptedException {
        this.transport = transport;

        if (sentalreadyused)
            throw new ClientProtocolException(true);
        sentalreadyused = true;

        List<InetSocketAddress> mxqueue;
        try {
            mxqueue = brain.getMX(forwardpath.getDomain());
        } catch (TextParseException e) {
            throw new ClientProtocolException(false);
        }

        for (InetSocketAddress addr : mxqueue) {

            try {
                send(transport, addr);
                return;//yeah, it worked
            } catch (ClientProtocolException e) {
                failurereasons.add(e);
            }
        }

        //none of the mx's could be used.
        if (failurereasons.size() == 0)
            throw new ClientProtocolException(false, "could not find a mail exchanger (MX) for domain");

        //unroll if only one
        if (failurereasons.size() == 1)
                throw failurereasons.get(0);

        //one of the exceptions must be recoverable in order to return recoverable, else not.
        for (ClientProtocolException e : failurereasons)
            if (e.recoverable())
                throw new ClientProtocolException(true, failurereasons);

        throw new ClientProtocolException(false, failurereasons);
    }

    private void send(SourceTransport transport, InetSocketAddress sa) throws ClientProtocolException, InterruptedException {
        ChamillionSocketTransport t = brain.getBestChamillionSocketTransport(sa.getAddress());

        state = State.CONNECTWAIT;
        asyncInterrupt = false;
        asyncExceptionProblem = null;

        try {
            t.connectClient(sa, proxy);
        } catch (IOException e) {
            throw new ClientProtocolException(true, e);
        }

        try {
            boolean sent;
            do {
                ByteBuffer bb = replies.take();//TODO add timeout

                if (asyncInterrupt) {
                    if (asyncExceptionProblem != null)
                        throw new ClientProtocolException(true, asyncExceptionProblem);
                    throw new ClientProtocolException(true);
                }

                sent = processReply(bb);
            } while(!sent);
        } catch (CharacterCodingException e) {
            throw new ClientProtocolException(true, e);
        } catch (LineDataException e) {
            throw new ClientProtocolException(true, e);
        }


    }

    @Override
    public void asyncConnected(DataLink dataLink) {
        this.dataLink = dataLink;//TODO should this be weak

        state = State.BANNERWAIT;
    }

    @Override
    public void asyncClosed() {
        asyncInterrupt = true;
        replies.add(ByteBuffer.wrap(new byte[0]));
    }

    @Override
    public void asyncInputShutdown() {
        asyncInterrupt = true;
        replies.add(ByteBuffer.wrap(new byte[0]));
    }

    @Override
    public void asyncData(ByteBuffer bytes) {
        replies.add(bytes); //TODO is this safe to keep bytebuffer
    }

    @Override
    public void asyncException(DataLinkProblem problem) {
        asyncExceptionProblem = problem;
        asyncInterrupt = true;
        replies.add(ByteBuffer.wrap(new byte[0]));
    }

    private boolean processReply(ByteBuffer bb) throws CharacterCodingException, ClientProtocolException, LineDataException, InterruptedException {
        if (state == State.DISCONNECT)
            return false;

        joinFragment(bb);

        if (state == State.CONNECTWAIT)
            return false;

        String line;
        while ((line = getLine()) != null) {

            switch (state) {
                case BANNERWAIT:
                    bannerWait(line);
                    break;
                case HELORESPONSEWAIT:
                    heloWait(line);
                    break;
                case MAILRESPONSEWAIT:
                    mailWait(line);
                    break;
                case RCPTRESPONSEWAIT:
                    rcptWait(line);
                    break;
                case DATARESPONSEWAIT:
                    dataWait(line);
                    break;
                case MSGDATARESPONSEWAIT:
                    msgdataWait(line);
                    return true;
            }
        }

        return false;
    }

    private void bannerWait(String line) throws ClientProtocolException, LineDataException, InterruptedException {
        if (line.startsWith("220")) {
            state = State.HELORESPONSEWAIT;
            LineData d = new LineData(new ASCIIString("EHLO ").concat(brain.getEHLODomain()));
            dataLink.sendToDataLink( ByteBuffer.wrap(d.toTransmitByteArray()) );
        } else {
            LineData d = new LineData("QUIT");
            dataLink.sendToDataLink( ByteBuffer.wrap(d.toTransmitByteArray()) );
            throw new ClientProtocolException(!line.startsWith("5"), "remote returned response to banner wait:" + line);
        }
    }

    private void heloWait(String line) throws ClientProtocolException, LineDataException, InterruptedException {
        if (line.startsWith("250-")) {
            return;
        }

        if (line.startsWith("250")) {
            state = State.MAILRESPONSEWAIT;
            LineData d = new LineData("MAIL FROM:<" + reversepath.toLineData() + ">");
            dataLink.sendToDataLink(ByteBuffer.wrap(d.toTransmitByteArray()));
        } else {
            LineData d = new LineData("QUIT");
            dataLink.sendToDataLink( ByteBuffer.wrap(d.toTransmitByteArray()) );
            throw new ClientProtocolException(!line.startsWith("5"), "remote returned response to helo wait:" + line);
        }
    }

    private void mailWait(String line) throws LineDataException, ClientProtocolException, InterruptedException {
        if (line.startsWith("250")) {
            state = State.RCPTRESPONSEWAIT;
            LineData d = new LineData("RCPT TO:<" + forwardpath.toLineData() + ">");
            dataLink.sendToDataLink(ByteBuffer.wrap(d.toTransmitByteArray()));
        } else {
            LineData d = new LineData("QUIT");
            dataLink.sendToDataLink( ByteBuffer.wrap(d.toTransmitByteArray()) );
            throw new ClientProtocolException(!line.startsWith("5"), "remote returned response to mail wait:" + line);
        }
    }

    private void rcptWait(String line) throws ClientProtocolException, LineDataException, InterruptedException {
        if (line.startsWith("250")) {
            state = State.DATARESPONSEWAIT;
            LineData d = new LineData("DATA");
            dataLink.sendToDataLink(ByteBuffer.wrap(d.toTransmitByteArray()));
        } else {
            LineData d = new LineData("QUIT");
            dataLink.sendToDataLink( ByteBuffer.wrap(d.toTransmitByteArray()) );
            throw new ClientProtocolException(!line.startsWith("5"), "remote returned response to rcpt wait:" + line);
        }
    }

    private void dataWait(String line) throws ClientProtocolException, LineDataException, InterruptedException {
        if (line.startsWith("354")) {
            state = State.MSGDATARESPONSEWAIT;
            sendMessageData();
        } else {
            LineData d = new LineData("QUIT");
            dataLink.sendToDataLink( ByteBuffer.wrap(d.toTransmitByteArray()) );
            throw new ClientProtocolException(!line.startsWith("5"), "remote returned response to data wait:" + line);
        }
    }

    private void sendMessageData() throws ClientProtocolException {
        try {
            dataLink.sendToDataLink(new TransparencyByteBufferWrapper(transport.getMessageByteBuffer()));
        } catch (IOException e) {
            throw new ClientProtocolException(true, e);
        } catch (InterruptedException e) {
            throw new ClientProtocolException(true, e);
        }
    }

    private void msgdataWait(String line) throws ClientProtocolException, LineDataException, InterruptedException {
        if (line.startsWith("250")) {
            state = State.DISCONNECT;
            LineData d = new LineData("QUIT");
            dataLink.sendToDataLink( ByteBuffer.wrap(d.toTransmitByteArray()) );
        } else {
            LineData d = new LineData("QUIT");
            dataLink.sendToDataLink( ByteBuffer.wrap(d.toTransmitByteArray()) );
            throw new ClientProtocolException(!line.startsWith("5"), "remote returned response to message wait:" + line);
        }
    }

    private void joinFragment(ByteBuffer bb) {
        if (savebuffer == null) {
            savebuffer = bb;
            return;
        }

        if (savebuffer.remaining() >= bb.remaining()) {
            savebuffer.put(bb);
            savebuffer.flip();
            return;
        }

        savebuffer.limit(savebuffer.position());
        savebuffer.position(0);

        ByteBuffer nb = ByteBuffer.allocate(savebuffer.remaining() + bb.remaining());

        nb.put(savebuffer);
        nb.put(bb);
        nb.flip();

        savebuffer = nb;
    }

    private String getLine() throws CharacterCodingException {
        //find crlf
        byte[] a = savebuffer.array();
        int b = savebuffer.position() + savebuffer.arrayOffset();
        int c;
        int z = b + savebuffer.remaining();

        for(c=b; c < z-1; ++c) {
            if (a[c] == '\r' && a[c+1] == '\n') {
                return ASCIIString.decode(savebuffer, c-b+2);
            }
        }

        //no line found, prepare buffer to receive more data
        savebuffer.compact();
        return null;
    }
}
