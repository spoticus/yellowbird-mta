/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.protocol;

/**
 * Command Replies
 * <p>
 * Contains enum for command replies valid for each command.
 * <p>
 * @see <a href="http://tools.ietf.org/html/rfc2821#section-4.3.2">Lines</a>
 */
public class CommandReply {
    private final LineData code;
    private final LineData message;

    public CommandReply(LineData code, LineData message) {
        this.code = code;
        this.message = message;
    }

    public CommandReply(LineData code, String message) {
        this.code = code;

        if (message != null)
            this.message = LineData.strip(message);
        else
            this.message =  null;
    }

    public LineData toLineData() {
        if (message == null)
            return code;
        return code.concat(" " + message);
    }
}
