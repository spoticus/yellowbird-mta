/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.protocol;

import me.yellowbird.mta.*;
import me.yellowbird.mta.brain.Brain;
import me.yellowbird.mta.io.DataLink;
import me.yellowbird.mta.io.DataLinkProblem;
import me.yellowbird.mta.io.Protocol;
import me.yellowbird.mta.transport.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * RFC2821 Server-side Protocol
 * <p/>
 * Implements the server-side of the protocol. The class is a state machine. It receives
 * byte buffers, parses commands and changes the state.
 * <pre>
 *   Transport
 *       |
 *   ServerProtocol
 *       | (raw bytes)
 *   <assemble lines>
 *       | (lines)
 *   <interpret commands>
 *       | (cmd + parm)
 *   InboundMessageBuilder
 * </pre>
 * <p/>
 * Plays the role of "Director" for the {@link InboundMessageBuilder}
 */
public class ServerProtocol implements Protocol {
    private static final Logger logger = LoggerFactory.getLogger("protocol");

    private static final Charset ASCII = Charset.forName("ASCII");

    /*
     * patterns are package-private for unit testing. "friend" would help.
     * tolerate trailing spaces according to 4.1.1
     * For commands with parameters these matchers create one group with
     * the string following the command with spaces trimmed front and back.
     * Other patterns are applied according to the specific command. This
     * is done so that a command can me identified even if the parameters
     * might me incorrect, which is fitting for the way the protocol should
     * behave.
     *
     */

    /*
     * Path pattern only extracts the command and the parameters as a string
     */
    static final Pattern CMD_PARM_PATTERN = Pattern.compile("^([^ ]+) *([^ ].*?)? *$");

    /*
     * The path (reverse-path and forward-path) pattern extracts the mailbox from the path and the esmtp parameters as a whole string.
     * group2 exists if it's a null path, otherwise group3 contains the path. group4 contains any esmtp parameters or
     * is null.
     * RFC2821 4.1.2
     */
    static final int FORWARDORREVERSE = 1;
    static final int THEPATH = 2;
    static final int ESMTPPARAMETERS = 3;

    static final Pattern PATH_PATTERN = Pattern.compile(
            "((?i:from:)|(?i:to:)){1}+\\ *\n" +
                    "<     #Path start\n" +
                    "(     #Begin Path capture\n" +
                    "[^>]*\n" +
                    ")     #End Path capture\n" +
                    ">     #end of Path\n" +
                    "(?:\n" +
                    "\\ +([^\\ ]+)\n" +
                    ")?"
            , Pattern.COMMENTS);

    // domain from helo/ehlo command or null if not yet received
    private String ehlodomain;

    protected static enum State {
        ENTRY,  //entry
        CONN,   //connected
        READY,  //ready for transaction
        TRANSACTION,    //in mail transaction
        DATA,    //receiving message
        QUITWAIT    //waiting for quit
    }

    private final Brain brain;
    private DataLink dataLink;
    private final InboundMessageBuilder mb;
    private SinkTransport outboundtransport = null;

    public ServerProtocol(Brain brain, InboundMessageBuilder messagebuilder) {
        this.brain = brain;
//        this.dataLink = dataLink;
        this.mb = messagebuilder;
    }

    protected State state = State.ENTRY;

    private ByteBuffer line_buffer = ByteBuffer.allocate(1001);//RFC max line size

    public void asyncConnected(DataLink dataLink) {
        this.dataLink = dataLink;

        try {
            connection();
        } catch (LineDataException e) {
            //TODO should this me reported
            dataLink.abortDataLink();
        } catch (InterruptedException e) {
            dataLink.abortDataLink();
        }
    }

    public void asyncClosed() {
        //TODO propogate cancel maybe to outboundtransport.
        // not anything special to me done
    }

    public void asyncInputShutdown() {
        //oops, not good
        dataLink.abortDataLink();
    }

    private void connection() throws LineDataException, InterruptedException {
        if (!(state == State.ENTRY))
            throw new StateMachineException(state, State.ENTRY);

        if (!brain.isMTAReadyForNewConnections()) {
            state = State.QUITWAIT;
            enqueue("554 MTA not ready");
            return;
        }

        try {
            mb.buildConnection(dataLink.getRemoteInetSocketAddress());
        } catch (ConnectionException e) {
            state = State.QUITWAIT;
            //TODO use reply command from exception but make it type-safe, cuz only 554 is allowed by protocol std.
            enqueue("554 connection rejected");
            return;
        }

        state = State.CONN;
        try {
            sendToTransport(new LineData(new ASCIIString("220 ").concat(brain.getWelcomeMessage())));
        } catch (ASCIIException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Raw data coming into server.
     * <p/>
     * In the ISO model this is in the presentation layer and is very applicable in the SMTP
     * protocol. The incoming stream may me encrypted and needs decryption. It also must me
     * broken into lines, terminated by CRLF.
     *
     * @param indata bytes. All bytes are consumed from the buffer, changing the position.
     */
    public void asyncData(ByteBuffer indata) {

        try {
            if (logger.isDebugEnabled()) {
                logger.debug(ASCII.newDecoder().decode(indata.duplicate()).toString());
            }
        } catch (CharacterCodingException e) {
            logger.debug("Could not decode indata");
        }
        /*
         * If not in operational state then cannot receive data.
         * This can only happen when logic is broken.
         */
        //TODO non-runtime exception
        if (state == State.ENTRY)
            throw new StateMachineException(state, State.ENTRY);

        /*
         * If in data state then use shortcut, optimized handling of
         * the incoming data. Else, it has to me broken up into lines
         * that can me parsed as commands by the protocol.
         */
        try {
            while (state == State.DATA && copyAndKeepCRLF(indata)) {
                line_buffer.flip();
                receiveData(line_buffer);
                line_buffer.clear();

            }
        } catch (LineDataException e) {
            dataLink.abortDataLink();
        } catch (MessageDataException e) {
            dataLink.abortDataLink();
        } catch (BufferOverflowException e) {
            line_buffer.clear();
            try {
                enqueue("500 ");
            } catch (LineDataException e1) {

            } catch (InterruptedException e1) {
                dataLink.abortDataLink();
            }
            state = State.QUITWAIT;
            return;
        } catch (InterruptedException e) {
            dataLink.abortDataLink();
        }

        try {
            /*
            * Buffer up till we have a full line.
            */
            try {
                while (copyAndDropCRLF(indata)) {
                    line_buffer.flip();
                    line();
                    line_buffer.clear();
                }
            } catch (BufferOverflowException e) {
                //TODO limitations by current state
                line_buffer.clear();
                indata.position(indata.limit());//consume all bytes
                try {
                    enqueue("500 command line too long");
                } catch (InterruptedException e1) {
                    dataLink.abortDataLink();
                }
            } catch (CharacterCodingException e) {
                try {
                    enqueue("500 invalid ascii characters");
                } catch (InterruptedException e1) {
                    dataLink.abortDataLink();
                }
            } catch (InterruptedException e) {
                dataLink.abortDataLink();
            }
        } catch (LineDataException e) {
            dataLink.abortDataLink();
        } catch (MessageDataException e) {
            dataLink.abortDataLink();
        }
//        }
    }

    public void asyncException(DataLinkProblem problem) {
    }


    /**
     * Protocol interpretation of the incoming data.
     * <p/>
     * In the ISO model, this is the application layer, where the lines are interpreted
     * according to the current state and changing the state according to the interpretation.
     *
     * @throws CharacterCodingException
     * @throws LineDataException
     * @throws MessageDataException
     * @throws InterruptedException
     */
    private void line() throws CharacterCodingException, LineDataException, MessageDataException, InterruptedException {

        CharBuffer cb;

        cb = ASCII.newDecoder().decode(line_buffer);

        Matcher m = CMD_PARM_PATTERN.matcher(cb);

        if (!m.matches()) {
            enqueue("500 Unrecognized command");
            return;
        }

        String cmd = m.group(1);

        String parm = null;
        if (m.groupCount() > 1)
            parm = m.group(2);

        switch (state) {
            case QUITWAIT:
                if (cmd.equalsIgnoreCase("QUIT")) {
                    doQUIT(parm);
                } else {
                    enqueue("501 QUIT required");
                }
                break;

            case CONN:
                if (cmd.equalsIgnoreCase("RSET")) {
                    enqueue("250 OK");
                    break;
                }
                if (cmd.equalsIgnoreCase("NOOP")) {
                    enqueue("250 OK");
                    break;
                }
                if (cmd.equalsIgnoreCase("VRFY")) {
                    enqueue("550 Not available");
                    break;
                }
                if (cmd.equalsIgnoreCase("EXPN")) {
                    enqueue("550 Not available");
                    break;
                }
                if (cmd.equalsIgnoreCase("HELP")) {
                    enqueue("214 No help available");
                    break;
                }
                if (cmd.equalsIgnoreCase("VRFY")) {
                    enqueue("550 unable");
                    break;
                }
                if (cmd.equalsIgnoreCase("EXPN")) {
                    enqueue("550 unable");
                    break;
                }

                if (cmd.equalsIgnoreCase("HELO")) {
                    doHELO(parm);
                    break;
                }
                if (cmd.equalsIgnoreCase("EHLO")) {
                    doEHLO(parm);
                    break;
                }
                if (cmd.equalsIgnoreCase("QUIT")) {
                    doQUIT(parm);
                    break;
                }
                enqueue("500 Unrecognized command");
                break;

            case READY:
                if (cmd.equalsIgnoreCase("RSET")) {
                    enqueue("250 OK");
                    break;
                }
                if (cmd.equalsIgnoreCase("VRFY")) {
                    enqueue("550 Not available");
                    break;
                }
                if (cmd.equalsIgnoreCase("EXPN")) {
                    enqueue("550 unable");
                    break;
                }
                if (cmd.equalsIgnoreCase("HELP")) {
                    enqueue("214 No help available");
                    break;
                }
                if (cmd.equalsIgnoreCase("NOOP")) {
                    enqueue("250 OK");
                    break;
                }
                if (cmd.equalsIgnoreCase("HELO")) {
                    doHELO(parm);
                    break;
                }
                if (cmd.equalsIgnoreCase("EHLO")) {
                    doEHLO(parm);
                    break;
                }
                if (cmd.equalsIgnoreCase("MAIL")) {
                    doMAIL(parm);
                    break;
                }
                if (cmd.equalsIgnoreCase("QUIT")) {
                    doQUIT(parm);
                    break;
                }
                enqueue("500 Unrecognized command");
                break;

            case TRANSACTION:
                if (cmd.equalsIgnoreCase("RCPT")) {
                    doRCPT(parm);
                    break;
                }
                if (cmd.equalsIgnoreCase("RSET")) {
                    state = State.READY;
                    enqueue("250 OK");
                    break;
                }
                if (cmd.equalsIgnoreCase("VRFY")) {
                    enqueue("550 Not available");
                    break;
                }
                if (cmd.equalsIgnoreCase("EXPN")) {
                    enqueue("550 unable");
                    break;
                }
                if (cmd.equalsIgnoreCase("HELP")) {
                    enqueue("214 No help available");
                    break;
                }
                if (cmd.equalsIgnoreCase("NOOP")) {
                    enqueue("250 OK");
                    break;
                }
                if (cmd.equalsIgnoreCase("HELO")) {
                    doHELO(parm);
                    break;
                }
                if (cmd.equalsIgnoreCase("EHLO")) {
                    doEHLO(parm);
                    break;
                }
                if (cmd.equalsIgnoreCase("QUIT")) {
                    doQUIT(parm);
                    break;
                }
                if (cmd.equalsIgnoreCase("DATA")) {
                    doDATA(parm);
                    break;
                }
                enqueue("500 Unrecognized command");

            default:
                throw new StateMachineException(state);
        }
    }

    /*
     * DATA for Transaction state
     */

    private void doDATA(String parm) throws LineDataException, MessageDataException, InterruptedException {
        if (parm != null) {
            enqueue("501 invalid parameter");
            return;
        }

        try {
            outboundtransport = mb.buildDATA();

            if (outboundtransport == null) {
                enqueue("451 internal routing error");
                state = State.READY;
                return;
            }
        } catch (DataException e) {
            enqueue("503");
            return;
        } catch (Throwable t) {
            enqueue("451 internal error");
            state = State.READY;
            return;
        }

        try {
            outboundtransport.prepareForData();
        } catch (Throwable t) {
            enqueue("451 internal error");
            state = State.READY;
            return;
        }

        for (LineOfData lod : createTimestampHeader())
            outboundtransport.add(lod);

        state = State.DATA;
        enqueue("354 ready");

        /*
         * There is the possibility that just behind the DATA command is the
         * data, even though you might think that the client would wait for
         * the "354" reply. Just in case this happens, go ahead and process
         * any data in the input buffer.
         *
         * There is also the possibility that the the end of data indicator
         * is in the buffer, so consuming the buffer through this indicator
         * would position the buffer correctly for further processing.
         */
//        receiveData(indata);
    }

    //RFC2821 4.4

    private LineOfData[] createTimestampHeader() {
        LineOfData[] r = new LineOfData[2];

        try {
            StringBuffer sb = new StringBuffer("Received: from ");

            sb.append(createExtendedDomain(ehlodomain, dataLink.getRemoteInetAddress()));

            sb.append("\r\n");

            r[0] = new LineOfData(sb.toString());

            sb = new StringBuffer("        by ");

            sb.append(createExtendedDomain(brain.getEHLODomain().toString(), dataLink.getLocalInetAddress()));

            sb.append("; ");

            SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");

            sb.append(sdf.format(new Date()));

            sb.append("\r\n");

            r[1] = new LineOfData(sb.toString());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);//should never happen
        } catch (LineDataException e) {
            throw new RuntimeException(e);//should never happen
        }

        return r;
    }

    //RFC2821 4.4

    private String createExtendedDomain(String reporteddomainname, InetAddress address) {
        StringBuffer sb = new StringBuffer(reporteddomainname).append(" ");

        String s = brain.getPTR(address);

        sb.append("(");

        if (s != null) {
            sb.append(s).append(" ");
        }

        sb.append("[").append(address.getHostAddress());
        sb.append("])");

        return sb.toString();
    }

    /*
     * Receive data bytes from the input buffer.
     * @param data our byte buffer includes the trailing CRLF.
     */

    private void receiveData(ByteBuffer data) throws MessageDataException, LineDataException, InterruptedException {
        /*
         * Check for end of message terminal (".") and terminal transparency ("..")
         */
        if (data.remaining() == 3) {
            int b = data.arrayOffset() + data.position();
            if (data.array()[b] == '.' && data.array()[b + 1] == '\r' && data.array()[b + 2] == '\n') {
                outboundtransport.commit();
                state = State.READY;
                enqueue("250 accepted");
                return;
            }
        } else if (data.remaining() > 1) {
            if (data.array()[data.arrayOffset() + data.position()] == '.')
                data.position(data.position() + 1);
        }

        /*
         * this is actually the internal line buffer here.
         * it would be optimal if the outboundtransport consumed the data on this invocation.
         * it's possible they won't and there is not easy way to protect the line buffer.
         * duplicate the data. this is a place for optimization.
         */
        byte[] b = Arrays.copyOfRange(data.array(), data.arrayOffset(), data.arrayOffset() + data.remaining());
        outboundtransport.add(new LineOfData(ByteBuffer.wrap(b)));
    }

    /*
     * QUIT for CONN, READY, Transaction state
     */

    private void doQUIT(String parm) throws LineDataException, InterruptedException {
        if (parm != null) {
            enqueue("501 invalid parameter");
        } else {
            enqueue("221 OK");
            dataLink.sendClose();
        }
    }

    /*
     * HELO for CONN, READY, Transaction state
     */

    private void doHELO(String parm) throws LineDataException, InterruptedException {
        if (parm == null) {
            enqueue("501 missing domain");
        } else {
            ehlodomain = parm;
            state = State.READY;
            sendToTransport(new LineData(new ASCIIString("250 ").concat(brain.getEHLODomain()).concat(" ").concat(brain.getEHLOGreeting())));
        }

    }

    /*
     * EHLO for CONN, READY, Transaction state
     */

    private void doEHLO(String parm) throws LineDataException, InterruptedException {
        if (parm == null) {
            enqueue("501 missing domain");
        } else {
            ehlodomain = parm;
            state = State.READY;
            MultiLineReply r = new MultiLineReply(new LineData("250"), brain.getEHLODomain().concat(" ").concat(brain.getEHLOGreeting()));
            r.add(new LineData("250"), new LineData("HELP"));
            sendToTransport(r);
        }
    }

    private void doMAIL(String parm) throws LineDataException, InterruptedException {
        //TODO should identify that the predicate "from:" exists and throw 501 when not, but let a bad mailbox pass this test.
        Matcher m = ServerProtocol.PATH_PATTERN.matcher(parm);

        if (!m.lookingAt()) {
            enqueue("501 syntax invalid");
            return;
        }

        if (!m.hitEnd()) {
            int x = m.start();
            int y = m.end();
            enqueue("501 syntax invalid " + m.start() + "," + m.end());

            return;
        }

        if (!m.group(FORWARDORREVERSE).equalsIgnoreCase("from:")) {
            enqueue("501 syntax invalid");
            return;
        }

        try {
            mb.buildMAIL(m.group(THEPATH) != null ? new AddrSpec(m.group(THEPATH)) : null, MailParameter.factory(m.group(ESMTPPARAMETERS)));
        } catch (ReversePathException e) {
            enqueue("451");
            return;
        } catch (InvalidMailBoxException e) {
            enqueue("501 mailbox invalid");
            return;
        }

        state = State.TRANSACTION;

        enqueue("250");

    }

    private void doRCPT(String parm) throws LineDataException, InterruptedException {
        Matcher m = ServerProtocol.PATH_PATTERN.matcher(parm);

        if (!m.matches() || !m.group(FORWARDORREVERSE).equalsIgnoreCase("to:")) {
            enqueue("500 Unrecognized command");
            return;
        }

        if (m.group(THEPATH).length() == 0) {//null path not allowed on rcpt command
            enqueue("501 null path not allowed on rcpt command");
            return;
        }

        try {
            mb.buildRCPT(m.group(THEPATH) != null ? new AddrSpec(m.group(THEPATH)) : null, MailParameter.factory(m.group(ESMTPPARAMETERS)));
        } catch (ForwardPathException e) {
            enqueue(e.getReply().toLineData());
            return;
        } catch (Throwable e) {
            logger.error("unexpected exception during doRCPT", e);
            enqueue("501");//oops, something bad happened
        }

        enqueue("250");

    }

    /**
     * Copy data from the input to the line buffer drop a CRLF found or
     * until the end of the input is found.
     * <p/>
     * If the buffer is too small to accomodate all of the input data to me
     * copied then BufferOverflowException is thrown.
     * <p/>
     * If a CRLF is found then there may me remaining data in the input.
     *
     * @param indata input data to append
     * @return true if a CRLF pair was found
     * @throws java.nio.BufferOverflowException
     *          When the number of input bytes exceeds the buffers capacity
     */
    private boolean copyAndDropCRLF(ByteBuffer indata) throws BufferOverflowException {
        byte[] ba = indata.array();
//        int offset = indata.arrayOffset();
        int start = indata.arrayOffset() + indata.position();
        int end = start + indata.remaining();

        for (int i = start; i < end; ++i) {
            if (ba[i] == '\r') {
                if (i + 1 < end) {
                    if (ba[i + 1] == '\n') {
                        line_buffer.put(ba, start, i - start);
                        indata.position(i + 2 - indata.arrayOffset());
                        return true;
                    }
                }
            }
        }

        //no CRLF, append everything
        line_buffer.put(indata);

        return false;
    }

    private boolean copyAndKeepCRLF(ByteBuffer indata) throws BufferOverflowException {
        byte[] ba = indata.array();
//        int offset = indata.arrayOffset();
        int start = indata.arrayOffset() + indata.position();
        int end = start + indata.remaining();

        for (int i = start; i < end; ++i) {
            if (ba[i] == '\r') {
                if (i + 1 < end) {
                    if (ba[i + 1] == '\n') {
                        line_buffer.put(ba, start, i + 2 - start);
                        indata.position(i + 2 - indata.arrayOffset());
                        return true;
                    }
                }
            }
        }

        //no CRLF, append everything
        line_buffer.put(indata);

        return false;
    }

    private void enqueue(LineData response) throws LineDataException, InterruptedException {
        sendToTransport(response);
    }

    private void enqueue(String response) throws LineDataException, InterruptedException {
        sendToTransport(new LineData(response));
    }

    private void sendToTransport(LineData data) throws InterruptedException {
        ByteBuffer b = ByteBuffer.wrap(data.toTransmitByteArray());

        dataLink.sendToDataLink(b);
    }

    private void sendToTransport(MultiLineReply reply) throws InterruptedException {
        ByteBuffer b = ByteBuffer.wrap(reply.toByteArray());

        dataLink.sendToDataLink(b);
    }

}
