/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.protocol;

/**
 * Command reply for connection establishment.
 */
public class ConnectionCommandReply extends CommandReply {
    public enum Code {
        S220,
        E554,
        E500,
        E501,
        E421;

        public LineData toLineData() {
            try {
                return new LineData(name().substring(1, 4));
            } catch (LineDataException e) {
                throw new RuntimeException(e);
            }
        }

    }

    /**
     * Construct from one of the allowed codes.
     * @param code response code from the allowed set.
     * @param message message to go with response code.
     */
    public ConnectionCommandReply(Code code, LineData message) {
        super(code.toLineData(), message);
    }
}