/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.protocol;

import me.yellowbird.mta.io.DataLinkProblem;

import java.util.ArrayList;
import java.util.List;

/**
 * Exception in the client protocol.
 * <p>
 * This is a signal sent from the client protocol to the "application layer" to indicate
 * that some condition exists that stopped the transmission of a message. The context
 * of the failure, whether recoverable or not, is indicated by properties that the
 * protocol sets from it's interpretation of the command replies it receives from the
 * corresponding MTA and the current state of the protocol when the exception condition
 * occurred.
 *
 */
public class ClientProtocolException extends Exception {
    private final boolean recoverable;
    private final List<ClientProtocolException> failurereasons;

    public ClientProtocolException(boolean recoverable, Throwable cause) {
        super(cause);
        this.recoverable = recoverable;
        failurereasons = null;
    }

    public ClientProtocolException(boolean recoverable, List<ClientProtocolException> failurereasons) {
        super("multiple reasons for failure.");
        this.recoverable = recoverable;
        this.failurereasons = failurereasons;
    }

    public ClientProtocolException(boolean recoverable) {
        this.recoverable = recoverable;
        failurereasons = null;
    }

    public ClientProtocolException(boolean recoverable, DataLinkProblem errorreason) {
        //noinspection ThrowableResultOfMethodCallIgnored
        super(errorreason.toString(), errorreason.getThrowable());

        this.recoverable = recoverable;
        failurereasons = null;
    }

    public ClientProtocolException(boolean recoverable, String message) {
        super(message);
        this.recoverable = recoverable;
        failurereasons = null;
    }

    @Override
    public String getMessage() {
        if (failurereasons == null)
            return super.getMessage();

        StringBuffer sb = new StringBuffer();

        for (ClientProtocolException e : failurereasons) {
            if (e != this)
                sb.append("[").append(e.getMessage()).append("]");
        }

        return sb.toString();
    }

    /**
     * Is the exception condition such that the message could me retransmitted later.
     * <p>
     * This flag is a direct reflection of either the command reply first character,
     * which is a "5" for unrecoverable, an underlying problem with the communication
     * stack, routing error, DNS or other conditions that the client protocol is
     * able to determine.
     *
     * @return true if it's possible that retransmitting the message later would end in success.
     */
    public boolean recoverable() {
        return recoverable;
    }

}
