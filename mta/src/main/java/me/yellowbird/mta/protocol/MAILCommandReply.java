/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.protocol;

/**
 * Command reply for MAIL command.
 */
public class MAILCommandReply extends CommandReply {
    public enum Code {
        S250,//OK
        E552,//Requested mail action aborted: exceeded storage allocation
        E451,//Requested action aborted: error in processing
        E452,//Requested action not taken: insufficient system storage
        E550,//Requested action not taken: mailbox unavailable(eg policy,security)
        E553,//Requested action not taken: mailbox name not allowed(eg syntax)
        E503,//Bad sequence of commands
        E500,//Syntax error, command unrecognized
        E501,//Syntax error in parameters or arguments
        E421;//<domain> Service not available, closing transmission channel

        public LineData toLineData() {
            try {
                return new LineData(name().substring(1, 4));
            } catch (LineDataException e) {
                throw new RuntimeException(e);
            }
        }

    }

    /**
     * Construct from one of the allowed codes.
     *
     * @param code response code from the allowed set.
     * @param message message to go with response code.
     */
    public MAILCommandReply(Code code, LineData message) {
        super(code.toLineData(), message);
    }
}