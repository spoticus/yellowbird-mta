/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.protocol;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.regex.Pattern;

/**
 * A complete line of message data.
 * <p>
 * This is for type safety to guarantee lines conform from the time they come into the
 * system to the time they leave. Optimization of this flow is obviously needed... later.
 */
public class LineOfData {
    private static final Pattern CRLF_PATTERN = Pattern.compile("\\r\\n");
    private static final Pattern NOT_ASCII_PATTERN = Pattern.compile("[^\\p{ASCII}]");
    private ByteBuffer data;

    /**
     * Construct from ByteBuffer containing line-data
     *
     * @param data line data with distinctive EOL CRLF pair.
     * @throws LineDataException thrown when the data does not conform to line-data constraints.
     */
    public LineOfData(ByteBuffer data) throws LineDataException {
        this.data = data.slice();

        byte[]a = data.array();
        int b = data.arrayOffset() + data.position();
        int c = b + data.remaining();

        for (int i = b; i < c; ++i) {
            if (a[i] == '\r' && i < c-2 && a[i+1] == '\n')
                throw new LineDataException("data contains embedded CRLF pair", a, i);

            if (a[i] > 0x7f)
                throw new LineDataException("data contains non-ascii code", a, i);
        }

        if (c-b < 2)
            throw new LineDataException("data line missing CRLF EOL", a, b);

        if (a[c-2] != '\r' || a[c-1] != '\n')
            throw new LineDataException("data line missing CRLF EOL", a, c-2);
    }

    /**
     * Construct from a String containg line-data.
     *
     * Despite a String being unicode, only codepoints that are ASCII are allowed.
     *
     * @param data line data with distinctive EOL CRLF pair.
     * @throws LineDataException thrown when the data does not conform to line-data constraints.
     * @throws java.io.UnsupportedEncodingException the string contains codes not allowed in a line.
     */
    public LineOfData(String data) throws UnsupportedEncodingException, LineDataException {
        this(ByteBuffer.wrap(data.getBytes("US-ASCII")));
    }

    public ByteBuffer getByteBuffer() {
        return data.slice();
    }

}
