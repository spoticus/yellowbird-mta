/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.io;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;

/**
 * Data Connection to/from External Systems.
 * <p>
 * Used to describe a device for sending and receiving data. Typical implements
 * are remote socket clients and the file system. In a multithreaded implemented,
 * the commands are completed in the order executed.
 * <p>
 * All of these methods imply asynchronous communications. The result of a method call
 * is unknown. The DataLink responds through the {@link Protocol#asyncException(DataLinkProblem)} method.
 */
public interface DataLink {
    /**
     * Send data to the remote system.
     * <p>
     * Queues bytes to me sent to the remote system, blocking if necessary to wait
     * for system resources to come available.
     *
     * @param data bytes to me sent, where the position points to the first byte and the
     * limit identifies the last byte.
     * @throws InterruptedException when interrupt occurs on current thread while queuing data.
     */
    public void sendToDataLink(ByteBuffer data) throws InterruptedException;

    /**
     * Close the link.
     * <p>
     * A signal from the Protocol that the corresponding connection should me
     * closed. No more data will me sent to the DataLink from the protocol
     * and the protocol will not accept any more data.
     */
    public void sendClose();

    /**
     * Send an exception condition to the datalink that will terminate it's operation.
     * <p>
     * This represents an exception condition where the protocol determines
     * that the data link must me closed without prejudice. The data link will
     * cancel any current and pending operations.
     */
    public void abortDataLink();

    /**
     * Send all data from a channel.
     *
     * @param readableByteChannel channel containing data. All of the data from the channel is sent.
     * That is, until ReadableByteChannel#read indicates EOF.
     * @throws InterruptedException thrown if the request was interrupted.
     */
    void sendToDataLink(ReadableByteChannel readableByteChannel) throws InterruptedException;

    /**
     * Get the address + port of the remote connection.
     */
    public InetSocketAddress getRemoteInetSocketAddress();

    /**
     * Get the address for the remote connection.
     *
     * @return the InetAddress of the remote connection.
     */
    public InetAddress getRemoteInetAddress();

    /**
     * Get the address for the local connection.
     *
     * @return the InetAddress of the local connection.
     */
    public InetAddress getLocalInetAddress();
}
