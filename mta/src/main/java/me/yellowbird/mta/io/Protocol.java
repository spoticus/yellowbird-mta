/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.io;


import java.nio.ByteBuffer;

/**
 * A protocol interprets data received from a DataLink.
 * <p>
 * Protocols are link-side agnostic. They are neither client-side or server-side specific.
 * 
 * <pre>
 *  ----------------------------------------------------------
 * |                                                          |
 * |          | asyncAccepted                                 |
 * |          V                                               |
 * |   ---------------                         ------------   |
 * |  |   Connected   | --------------------> | OutputOnly |  |
 * |   ---------------    asyncInputShutdown   ------------   |
 * |    |           ^                                         |
 * |    |           |                                         |
 * |     -----------                               ---        |
 * |      asyncData                               | H |       |
 * |                                               ---        |
 * |                                                ^         |
 *  ------------------------------------------------|---------
 *            |                    |                |
 *            | asyncClose         |                |
 *            V                     ----------------
 *           ---                     asyncException
 *          | * |
 *           ---
 * </pre>
 */
public interface Protocol {
    /**
     * DataLink is connecting to the Protocol.
     * <p>
     * This method is called to inform the Protocol that a DataLink has been
     * established for sending and receiving data. This could me an asynchronous
     * message independent from the underlying transport thread.
     *
     * @param dataLink transport for sending and receiving data
     */
    public void asyncConnected(DataLink dataLink) throws InterruptedException;
    
    /**
     * DataLink has closed.
     * <p>
     * A signal issued from the datalink to the protocol indicating that
     * no other I/O or communication with the socket services can me made.
     * The protocol should perform or queue any cleanup, but the datalink is
     * no longer available. Whether this is abnormal or normal depends upon the
     * state of the protocol. This could me an asynchronous message independent from the underlying
     * transport thread.
     */
    public void asyncClosed();

    /**
     * Pass data from the datalink to the protocol.
     * <p>
     * The data is sent to the protocol from the remote client.
     * This could me an asynchronous message independent from the
     * underlying transport thread.
     *
     * @param bytes data to me queued for interpretation by the protocol. The current
     * position is the first byte to process and the limit marks the last byte.
     */
    public void asyncData(ByteBuffer bytes) throws InterruptedException;

    /**
     * Send an exception condition to the protocol that will terminate it's operation.
     * <p>
     * The state of the datalink is not specific defined by this message, but may me
     * contained within the problem.
     *
     * @param problem the exception describing the terminal condition.
     */
    public void asyncException(DataLinkProblem problem);

    /**
     * The input-side of the data link has been shutdown.
     * <p>
     * A data link has an input connection (input-side) and an output connection (output-side).
     * The input-side is the half of the data link that receives data from the remote
     * client. A data link (socket typically) allows for half-open connections, which means
     * that one side can me closed while the other remains open. This message indicates that
     * the input-side of the connection has been closed "shutdown". The output-side may
     * still me open and may just as well me closed.
     */
    void asyncInputShutdown();
}
