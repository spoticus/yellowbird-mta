/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * Invalid mailbox format.
 */
public class InvalidMailBoxException extends Throwable {
    public InvalidMailBoxException(String mailbox) {
        super(mailbox);
    }

    public InvalidMailBoxException(byte[] mailbox) {
        super(formatBytes(mailbox));
    }

    private static String formatBytes(byte[] bytes) {
        StringBuffer sb = new StringBuffer();

        for (byte aByte : bytes) {
            sb.append("<").append(aByte).append(">");
        }

        sb.append(":");

        sb.append(Charset.forName("ASCII").decode(ByteBuffer.wrap(bytes)));

        return sb.toString();
    }
}
