/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.nio;

import java.nio.channels.ReadableByteChannel;
import java.nio.ByteBuffer;
import java.io.IOException;

class ReadableByteChannelCarrier implements ByteBufferSource {
    private ReadableByteChannel readableByteChannel;
    private ByteBuffer currentbuffer = ByteBuffer.allocate(4096);
    private boolean eof = false;

    public ReadableByteChannelCarrier(ReadableByteChannel readableByteChannel) {
        this.readableByteChannel = readableByteChannel;
        currentbuffer.position(currentbuffer.limit());//force init on first read
    }

    /**
     * Return the buffer with more data from channel if it is available.
     * <p/>
     * It's possible that an empty buffer will me returned when there is actually more data
     * in the channel.
     */
    @Override
    public ByteBuffer getByteBuffer() throws IOException {
        if (currentbuffer.remaining() == 0) {
            currentbuffer.clear();
            eof = readableByteChannel.read(currentbuffer) == -1;
            currentbuffer.flip();
        }

        return currentbuffer;
    }

    /**
     * Determine if the channel is actually empty.
     *
     * @return true if the channel is at EOF.
     */
    @Override
    public boolean isEmpty() {
        return eof;
    }
}
