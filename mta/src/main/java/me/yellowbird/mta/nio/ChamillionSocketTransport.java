/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.nio;

import me.yellowbird.mta.brain.Brain;
import me.yellowbird.mta.io.DataLink;
import me.yellowbird.mta.io.DataLinkProblem;
import me.yellowbird.mta.io.Protocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.channels.spi.SelectorProvider;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * A socket nio communication class.
 * <p/>
 * A class capable of both server and client nio communications at the same time
 * per class instance.
 * <p/>
 * <h3>nio Principles</h3>
 * The principle design aspect is a "selector" thread where all operations against
 * the selector and key sets are performed. All I/O operations on the channel are
 * also done on the selector thread. Reading and writing on a channel is also
 * done half-duplex (markers exists in the code for enhancing for full duplex).
 * A channel is normally in read mode and switched to write
 * mode when data is ready to me transmitted and after the transmission set back
 * to read mode. Not leaving the channel in write mode also eliminates spin locks
 * on certain platforms.
 * <p/>
 * An important point is that each instance of ChamillionSocketTransport has one
 * selector and is a Runnable that runs on one thread.
 * <p/>
 * <h3>Server Connections</h3>
 * When an instance of ChamillionSocketTransport is asked to create a new server it
 * creates a server channel, binds the address then enqueues a request for the
 * selector thread to register this channel with the selector. When the selector
 * thread gets this request, it registers the selector with the socket. At this point
 * the selector will take over and wait for an "accept" operation. When the accept
 * arrives, a server protocol is requested from the Brain, and that protocol,
 * along with other information, is attached to the selection key. At this point,
 * the ChamillionSocketTransport sees this as just another connection for reading
 * and writing that initially has an interest set of read.
 *
 * <h3>Client Connections</h3>
 * When an instance of ChamillionSocketTransport is asked to create a new client it
 * it is being handed a client Protocol and an endpoint. A channel and socket is created
 * for the endpoint and a request to complete the connection is enqueued for the selector
 * thread. When the selector thread gets the request, it registers the selector
 * with the socket with an interest op of connect. The attachment is created with
 * the protocol provided by the initial client request that was enqueued. At this point
 * the selector will take over and wait for the connection operation. When the
 * connection operation arrives, the connection is completed and the interest op
 * is set to read. At this point, the ChamillionSocketTransport sees this as just another
 * connection for reading and writing that initially has an interest set of read.
 *
 * <h3>Selection Key, Protocols and Strong References</h3>
 *
 * The selector has a strong reference to all the Selection Keys. The Selection Key
 * has it's reference to the channel, and beyond, and also has a reference in the
 * other direction towards the Protocol. From the Protocol perspective the Selection Key,
 * specifically the attachment, looks like a DataLink, hence the connection between the
 * two. When the selection key is garbage collected, everything else must me garbage
 * collected. Strong references must not me maintained anywhere else.
 *  
 * <h3>Asynchronous Protocol Communication</h3>
 * An architecture objective of this class is to separate the socket mechanics from
 * the application protocol. The selector thread cannot get tied down by the
 * protocol's logic. The thread time must me spent moving data in and out of the
 * socket and changing it's state. Data coming from the protocol to be sent out the
 * socket comes in through the DataLink API that is the selector key's attachment.
 * A protocol calls a DataLink method on the protocols thread and the DataLink
 * class puts that data on a queue that is monitored by the selector thread. Data
 * going to the protocol from the socket goes to the Protocol API. To provide
 * separation of threads, a proxy of the Protocol is used. This proxying is provided
 * by the ProtocolAsynchronizer class. Details of it's behavior are provided in it's
 * javadoc, but generally the Asynchronizer provides a proxy of the Protocol.
 * When the selector thread sends data, it is sending it to the proxy, which puts
 * the data on a queue inside the Asynchronizer. A thread dedicated to Protocol
 * instances then reads the queue and passes the data to the real Protocol.
 * <p/>
 * Thinking through this then it is the ProtocolAsynchronizer that provides the threads that
 * run the Protocol. By using an external mechanism from the socket, the ProtocolAsynchronizer,
 * it is possible for the Brain to allocate additional threads to the Protocol
 * instances.
 *
 * <h3>Interest-Ops</h3>
 * Interest ops are only changable by the selector thread. This accomodates a serious
 * flaw (my opinion) in the mechanics of IBM's implementation of the selection process
 * that locks the interest ops while the selector is blocking.
 *
 * <h3>Connection Shutdown</h3>
 * Close operations are synchronous with other activities in the same direction.
 * For example, if there is pending data to be sent, and the protocol request
 * the outbound connection to be closed, the data is sent before the outbound connection
 * is closed.
 * <p/>
 * Abort operations are immediate and any pending operations are ignored.
 */
public class ChamillionSocketTransport implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(ChamillionSocketTransport.class);

    private static final int IOSIZE = 1460;
    private boolean shutdowncomplete;

    private enum Duplexing {
        HALF,
        FULL
    }

    private Duplexing duplexing = Duplexing.HALF;

    /**
     * Is there at least one server socket ready to receive a connect.
     *
     * @return true if there is one server socket that is ready.
     */
    public boolean isServerReady() {
        //man, i hope this is thread safe
        Set<SelectionKey> keys = selector.keys();

        SelectionKey[] k = keys.toArray(new SelectionKey[keys.size()]);

        for (SelectionKey skey : k) {
            SelectableChannel sc = skey.channel();

            if (sc instanceof ServerSocketChannel) {
                return sc.isOpen();
            }
        }

        return false;
    }

    public void disconnectServers() {
        Set<SelectionKey> keys = selector.keys();

        SelectionKey[] k = keys.toArray(new SelectionKey[keys.size()]);

        for (SelectionKey skey : k) {
            SelectableChannel sc = skey.channel();

            if (sc instanceof ServerSocketChannel) {
                try {
                    sc.close();
                } catch (IOException ignored) {
                }
            }
        }

        selector.wakeup();
        
    }


    /**
     * DataLink that is bound to a selection key.
     * <p/>
     * This DataLink is stored in a selection key attachment and provides the
     * {@link DataLink} interface that communicates with a corresponding
     * {@link Protocol}.
     * <p/>
     * The only tough part is getting past connection, where an instance
     * behaves the same for a client and server connections. The client
     * connection is a bit convoluted, because a selection key doesn't exists
     * until the connection is ready in the selection loop by the
     * {@link SelectionKey#isConnectable} method, which occurs after the
     * connection request has been made by a separate thread, which
     * includes the protocol, so this has to me saved until the afformentioned
     * key and state occurs.
     * <p/>
     * Once the connections made, data and control request flow back and
     * forth between instances of this class and the associated Protocol
     * instance. This class provides the happy mapping between nio logic
     * and the interface requirements.
     * <p/>
     * Incoming data and commands, data that is to me sent to the remote socket
     * and commands that effect the socket, are queued for consumption by
     * the selector thread. It's the selector thread that reads from these queues
     * and sends the data or applies the commands to the socket or connection.
     * <p/>
     * The interface requires that certain commands me executed in sequence.
     * These commands are send data and close. Once a close command
     * is received, no more data is accepted. Second, the class is careful to
     * wait until the queue of data has been sent before it enqueues a close
     * command.
     */
    class SelectionKeyDataLink implements DataLink {
        private final SelectionKey key;
        private final Protocol protocol;
        private ConcurrentLinkedQueue<ByteBufferSource> writedataqueue = new ConcurrentLinkedQueue<ByteBufferSource>();
        private boolean closerequested = false;

        /**
         * Construct from a selection key binding the socket and the protocol that will interpret
         * the data stream.
         *
         * @param key      selection key that binds the remote socket and channel.
         * @param protocol protocol that will interpret the data stream.
         */
        SelectionKeyDataLink(SelectionKey key, Protocol protocol) {
            this.key = key;
            this.protocol = protocol;
        }

        /**
         * The sender sends bytes through the selection key attachment which provides
         * the binding to the correct ChamillionSocketTransport.
         *
         * @param data buffer containing data. The byte buffer underneath must have a position at the
         *             first byte to me written and the limit set after the last byte to me written, as when flip()
         *             is called after putting data into the buffer. The buffer is kept by this class even after
         *             the method return and the underlying byte array must never me used by the caller again.
         */
        public void sendToDataLink(ByteBuffer data) throws InterruptedException {
            if (closerequested) {
                protocol.asyncException(new DataLinkProblem("Close of connection pending, cannot send anymore data"));
                return;
            }

            writedataqueue.add(new ByteBufferCarrier(data));

            mayneedwrite.put(key);

            selector.wakeup();
        }

        public void sendToDataLink(ReadableByteChannel readableByteChannel) throws InterruptedException {
            if (closerequested) {
                protocol.asyncException(new DataLinkProblem("Close of connection pending, cannot send anymore data"));
                return;
            }


            writedataqueue.add(new ReadableByteChannelCarrier(readableByteChannel));

            mayneedwrite.put(key);

            selector.wakeup();
        }

        @Override
        public InetSocketAddress getRemoteInetSocketAddress() {
            SocketChannel socketChannel = (SocketChannel) key.channel();

            return new InetSocketAddress(socketChannel.socket().getInetAddress(), socketChannel.socket().getPort());
        }

        @Override
        public InetAddress getRemoteInetAddress() {
            SocketChannel socketChannel = (SocketChannel) key.channel();

            return socketChannel.socket().getInetAddress();
        }

        @Override
        public InetAddress getLocalInetAddress() {
            SocketChannel socketChannel = (SocketChannel) key.channel();

            return socketChannel.socket().getLocalAddress();
        }

        public void sendClose() {
            if (closerequested) {
                return;//close already received, not allowed to communicate with protocol again.
            }

            closerequested = true;

            needsclosing.add(key);

            selector.wakeup();
        }

        public void abortDataLink() {
            SocketChannel socketChannel = (SocketChannel) key.channel();

            try {
                socketChannel.close();
                socketChannel.socket().close();
//                key.channel().close();
            } catch (IOException e) {
                logger.error("unable to close socket during abortDataLink", e);
            }

            selector.wakeup();
        }

    }

    /**
     * During a new client connection, a selection key cannot me created through
     * selector registration except in the selector thread. A place is needed to hold
     * the socket and protocol until the selector thread runs and can do the
     * registration. This is done using this class and the needsconnect queue, which
     * is written to when a client connection is requested and read in parallel by
     * the selector thread.
     */
    static class ClientNeedingConnection {
        private SocketChannel socket;
        private Protocol client;

        ClientNeedingConnection(SocketChannel socket, Protocol client) {
            this.socket = socket;
            this.client = client;
        }
    }

    private ConcurrentLinkedQueue<ClientNeedingConnection> needsconnect = new ConcurrentLinkedQueue<ClientNeedingConnection>();

    /*
     * When this goes true, the selector thread will shutdown the whole thing the next time it runs.
     */
    private boolean shutdown = false;

    private final Brain brain;

    private Selector selector;

    /*
     * A queue of selector keys that may have data to write. "May have", because multiple
     * entries may exists for the same selector key, where one request will cause all pending
     * data to me written and meaning that the second request won't have any data to me written
     * by the time it is pulled from the queue.
     */
    private LinkedBlockingQueue<SelectionKey> mayneedwrite = new LinkedBlockingQueue<SelectionKey>(1000);

    /*
     * New server channels that need to me registered with the selector and have the new
     * selector key set to "accept" operation set. The selector thread processes these.
     */
    private ConcurrentLinkedQueue<ServerSocketChannel> needsaccept = new ConcurrentLinkedQueue<ServerSocketChannel>();

    /*
     * Selection keys that need to me closed, including the channel and the protocol. The selector
     * thread processes these.
     */
    private ConcurrentLinkedQueue<SelectionKey> needsclosing = new ConcurrentLinkedQueue<SelectionKey>();

    /*
     * These are socket channels that have not been registered with the selector, but someone
     * wants to know when that happens. The selector thread counts down the latch after registering
     * the socket. This is an optional operation for setting up a server socket.
     *
     * Lazy, yes, the weak hash map is lazy way to absolve ourselves from recording keeping and
     * avoiding memory leaks, which could happen since the thread adding elements is not the thread
     * that causes their removal.
     */
    private Map<ServerSocketChannel, CountDownLatch> waitforaccept = Collections.synchronizedMap(
            new WeakHashMap<ServerSocketChannel, CountDownLatch>());

    /**
     * Create a socket transport.
     *
     * @param brain the brain
     * @throws IOException when something goes wrong
     */
    public ChamillionSocketTransport(Brain brain) throws IOException {
        this.brain = brain;

        selector = SelectorProvider.provider().openSelector();
    }

    /**
     * Create a new connection to a remote client.
     * <p/>
     * On successful return the connection will be in progress and the protocol
     * will be informed of success through the {@link Protocol#asyncConnected} method.
     *
     * @param serverAddress address of remote server
     * @param client        adapter that is used for connecting the client protocol stack
     * @throws java.io.IOException when something bad happens
     */
    public void connectClient(InetSocketAddress serverAddress, Protocol client) throws IOException {
        if (shutdown)
            throw new IOException("Shutdown in progress");

        /*
         * standard nio stuff:
         * - make selector (already established)
         * - open socket
         * - make remote connection
         * - request op_connect interest set
         */

        SocketChannel socketChannel = SocketChannel.open();
        socketChannel.configureBlocking(false);

        socketChannel.connect(serverAddress);

        needsconnect.add(new ClientNeedingConnection(socketChannel, client));

        // wakeup so we can handle connect state change when selector is first selected
        this.selector.wakeup();
    }

    /**
     * Create a server-side socket.
     * <p/>
     * Opens a server channel and queues an asynchronous request to register the channel on the selector.
     * The channel is not immediately available for receiving connects.
     *
     * @param serverAddress address and port to bind.
     * @throws IOException when something goes wrong.
     */
    public void connectServer(InetSocketAddress serverAddress) throws IOException {
        if (shutdown)
            throw new IOException("Shutdown in progress");

        /*
         * standard nio stuff:
         * - make selector (already established)
         * - open server socket channel
         * - bind address
         * - request op_accept interest set
         */

        ServerSocketChannel serverChannel = ServerSocketChannel.open();
        serverChannel.configureBlocking(false);

        serverChannel.socket().bind(serverAddress, 200);//set to this size because of unit test, but this ought to be a config option

        needsaccept.add(serverChannel);

        this.selector.wakeup();
    }

    /**
     * Create a server-side socket and wait for it to become available.
     * <p/>
     * Opens a server channel and registers the channel on the selector. The current thread
     * will wait a period of time for the registration to complete.
     *
     * @param serverAddress address and port to bind.
     * @param timeout       time to wait for the registration to complete.
     * @param unit          The unit of measure for timeout.
     * @return true if the ready state has been reached. false if the wait times out.
     * @throws IOException          when something goes wrong.
     * @throws InterruptedException if the waiting thread is interrupted.
     */
    public boolean connectServer(InetSocketAddress serverAddress, long timeout, TimeUnit unit) throws InterruptedException, IOException {
        if (shutdown)
            throw new IOException("Shutdown in progress");

        /*
         * standard nio stuff:
         * - make selector (already established)
         * - open server socket channel
         * - bind address
         * - request op_accept interest set
         */

        ServerSocketChannel serverChannel = ServerSocketChannel.open();
        serverChannel.configureBlocking(false);

        serverChannel.socket().bind(serverAddress);

        CountDownLatch readystatelatch = new CountDownLatch(1);
        waitforaccept.put(serverChannel, readystatelatch);

        needsaccept.add(serverChannel);

        this.selector.wakeup();

        boolean r = readystatelatch.await(timeout, unit);

        waitforaccept.remove(serverChannel);//nobody else can possibly me waiting

        return r;
    }

    /**
     * Shutdown the selector thread.
     */
    public void shutdown() {
        shutdown = true;

        selector.wakeup();
    }

    public void run() {
        while (true) {
            if (shutdown) {
                /*
                 * TODO in here we have a race condition being established.
                 * The sockets are being shutdown and this does happen, but on some OSs this is
                 * asynchronous. This method will return but the bind will still exists at the OS
                 * level. Therefore, a later bind request for the same ip:port will fail, even
                 * though it is in process of being released. This problem shows up during testing,
                 * where the same ip:port is being constantly re-used by different test.
                 * The answer is to figure out a way to wait for the actual release of the ip:port.
                 * The CST is running on a thread by design and by design the owner of the thread
                 * is suppose to wait on the CST thread to end before quitting itself.
                */
                try {
                    for (SelectionKey skey : selector.keys()) {
                        SelectableChannel sc = skey.channel();

                        if (sc instanceof SocketChannel) {
                            Socket s = ((SocketChannel) sc).socket();

                            try {
                                s.close();
                            } catch (IOException e) {
                                //ignore
                            }
                        } else if (sc instanceof ServerSocketChannel) {
                            ServerSocket s = ((ServerSocketChannel) sc).socket();

                            try {
                                s.close();
                            } catch (IOException e) {
                                //ignore
                            }
                        }
                    }
                } catch (ClosedSelectorException e) {
                    shutdown = false;
                    shutdowncomplete = true;
                    //no problem, i guess we're done
                    return;
                }
                shutdown = false;
                shutdowncomplete = true;
                return;
            }

            /*
            * Here we resolve parallelism between the selector thread and
            * the higher-level protocol that is asynchronously requesting changes
            * to the selection key interest set. These higher-level protocol
            * request must me done in the selector's thread, because they effect
            * the selector. This isn't always necessary, since the API allows for
            * interest set changes asynchronously, but there is documented situations
            * where particular jre implementations fail otherwise.
            *
            * READING AND WRITING:
            *
            * When the higher-level protocol
            * needs to write data, the selector's interest ops needs to include
            * "write", which will eventually cause a write key so that the data
            * can me written.
            *
            * In full duplex mode this is simpler because the concern is simply
            * turning write interest on and off as data is available to send. In
            * full duplex mode read is always set. The only reason for turning off write
            * in full duplex mode is to avoid spin locks that exists in some operation
            * system implementations of the JRE, where a write mode without available
            * data requires a tight polling spin in the operation system (windows, for example).
            *
            * In half duplex mode this is more complex because the selection key
            * is being toggle back and forth between read and write. Idle state is
            * to have the read interest set on the selection key.
            *
            * CLIENT CONNECTIONS
            *
            * A new client connection (local-side client) contains a reference to a
            * higher-level protocol and a socket. This socket must me registered with
            * the selector which will eventually trigger an is connected selection key.
            *
            * SERVER CONNECTIONS
            *
            * A new server connection (local-side server) contains a reference to a
            * higher-level protocol and a socket. This socket must me registered with
            * the selector which will eventually trigger an is acceptable selection key.
            */

            SelectionKey ckey;

            int loopmax = mayneedwrite.size();
            while (loopmax-- > 0 && (ckey = mayneedwrite.poll()) != null) {// any type, client or server socket, with data to write

                SelectionKeyDataLink attachment = (SelectionKeyDataLink) ckey.attachment();

                if (attachment.writedataqueue.peek() != null)
                    try {
                        ckey.interestOps(SelectionKey.OP_WRITE);
                    } catch (IllegalArgumentException e) {
                        logger.error("trying to set interestOps to write", e);
                        attachment.protocol.asyncException(new DataLinkProblem(e));
                    } catch (CancelledKeyException e) {
                        logger.debug("trying to set interestOps to write", e);
                        attachment.protocol.asyncException(new DataLinkProblem(e));
                    }//it's alright, selection will remove
            }

            ClientNeedingConnection pendingclient;

            while ((pendingclient = needsconnect.poll()) != null) {//for client connections
                try {
                    ckey = pendingclient.socket.register(selector, SelectionKey.OP_CONNECT);
                } catch (Throwable e) {
                    //let the protocol know it's dead
                    //noinspection ThrowableInstanceNeverThrown
                    try {
                        pendingclient.client.asyncException(new DataLinkProblem(e));
                    } catch (RuntimeException e1) {
                        logger.error("trying to send exception to protocol", e1);
                    }
                    continue;
                }

                SelectionKeyDataLink attachment = new SelectionKeyDataLink(ckey, pendingclient.client);
                ckey.attach(attachment);
            }

            ServerSocketChannel serversocket;

            /*
            * Occasionally, maybe once, we register a server.
            */
            while ((serversocket = needsaccept.poll()) != null) {//for server connections
                try {
                    serversocket.register(selector, SelectionKey.OP_ACCEPT);

                    CountDownLatch latch = waitforaccept.get(serversocket);
                    if (latch != null)
                        latch.countDown();
                } catch (Throwable e) {
                    logger.error("trying to register server", e);
                }
            }

            /*
            * Close channels that need closing, except you can't close something that has
            * data to me written. Any of these are put back on the needsclosing queue, but
            * loops are avoided by observing if the entire queue has been traversed at least once.
            * Request that are added by other threads after the loop connectServer may not me processed
            * by this loop if there is a writeback because of this situation. However, we know
            * that the selector will not block, because it has been told to wakeup when those
            * other threads added the close request, which means that this loop will execute
            * again shortly after the selector section below.
            * <p>
            * Second point, this logic depends upon the selector doing some writing, which
            * it should, and then this logic executing after the write because the selector
            * doesn't block until these queue checks are first done.
            */
            SelectionKey firstwritebackkey = null;//first key written back is a marker for detecting loops

            while ((ckey = needsclosing.poll()) != null) {

                if ((ckey.interestOps() & SelectionKey.OP_WRITE) != 0) {
                    needsclosing.add(ckey);

                    if (firstwritebackkey == null) {
                        firstwritebackkey = ckey;
                        continue;
                    }

                    if (firstwritebackkey == ckey) {
                        break;
                    }

                    continue;
                }

                SelectableChannel sc = ckey.channel();

                if (sc instanceof SocketChannel) {
                    Socket s = ((SocketChannel) sc).socket();

                    try {
                        s.close();
                    } catch (IOException e) {
                        //ignore
                    }
                } else if (sc instanceof ServerSocketChannel) {
                    ServerSocket s = ((ServerSocketChannel) sc).socket();

                    try {
                        s.close();
                    } catch (IOException e) {
                        //ignore
                    }
                }
            }

            /*
             * block for events
             */
            int i;
            try {
                i = selector.select();//also throws closed selector
            } catch (IOException e) {
                //this is so serious, i don't know what to do.
                throw new RuntimeException(e);
            }

            if (i > 0) {
                Iterator<SelectionKey> keys;
                keys = selector.selectedKeys().iterator();

                while (keys.hasNext()) {
                    SelectionKey key = keys.next();

                    keys.remove();

                    if (!key.isValid())
                        continue;

                    try {
                        if (key.isConnectable()) // for client
                            this.finishConnection(key);
                        else if (key.isAcceptable()) // for server
                            this.accept(key);
                        else if (key.isReadable())
                            this.read(key);
                        else if (key.isWritable())
                            this.write(key);
                    } catch (CancelledKeyException e) {
                        ((SelectionKeyDataLink) key.attachment()).protocol.asyncClosed();
                    } catch (InterruptedException e) {
                        shutdown = true;
                    }
                }
            }
        }
    }

    /**
     * Accept a connection from a client.
     * <p/>
     * Assume this is a server socket channel, accept and put in non-blocking
     * mode. This method must run from the selector thread since it sets the
     * interest set to read.
     * <p/>
     * A server protocol is made the attachment to the selection key.
     *
     * @param key corresponding key
     */
    private void accept(SelectionKey key) throws InterruptedException {

        ServerSocketChannel serverSocketChannel =
                (ServerSocketChannel) key.channel();

        SocketChannel socketChannel;
        try {
            socketChannel = serverSocketChannel.accept();
        } catch (IOException e) {
            logger.error("trying to accept server socket", e);
            return;
        }

        try {
            socketChannel.configureBlocking(false);
        } catch (IOException e) {
            try {
                socketChannel.close();
            } catch (IOException e1) {
                logger.error("trying to close socket", e1);
            }
            return;
        }

        SelectionKey newkey;
        try {
            newkey = socketChannel.register(this.selector,
                    SelectionKey.OP_READ);
        } catch (ClosedChannelException e) {
            try {
                socketChannel.close();
            } catch (IOException e1) {
                logger.error("trying to close socket", e1);
            }
            return;
        }

        Protocol protocol = brain.createServerProtocol();
        SelectionKeyDataLink attachment = new SelectionKeyDataLink(newkey, protocol);
        newkey.attach(attachment);

        protocol.asyncConnected(attachment);
    }

    /*
     * Read data from the underlying channel.
     * <p>
     * Data is read, unless EOF, which then it is closed and the protocol
     * informed.
     */
    private void read(SelectionKey key) throws InterruptedException {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        int incount = 0;
        ByteBuffer bytes = ByteBuffer.allocate(IOSIZE);

        int lastcount;
        try {
            do {
                lastcount = socketChannel.read(bytes);
                incount += lastcount;
            } while (lastcount > 0 && bytes.remaining() > 0);//fill-er-up
        } catch (IOException e) {
            close(key);
            return;
        }

        /*
         * If the input-side is down then notify the client. The state of the
         * output-side is unknown (and how could it, it's tcp). Do some administration
         * to make sure that everybody knows that the input side is down. Always tell
         * the upper-level protocol of the input-side being down, even though the
         * situation may me that the whole connection is down. This makes it consistent
         * with the state that we know of at this time, which is solely that the input
         * side is down.
         */
        if (incount == -1) {
            key.interestOps( key.interestOps() & ~SelectionKey.OP_READ );//turn off read interest

            SelectionKeyDataLink attachment = (SelectionKeyDataLink) key.attachment();

            attachment.protocol.asyncInputShutdown();

            try {
                socketChannel.socket().shutdownInput();
            } catch (IOException e) {
                close(key);
                return;
            }

            return;
        }

        /*
         * Send the data to the protocol.
         */
        if (incount > 0) {
            bytes.flip();
            SelectionKeyDataLink attachment = (SelectionKeyDataLink) key.attachment();

            try {
                attachment.protocol.asyncData(bytes);
            } catch (RuntimeException e2) {
                close(key);
            }
        }
    }

    /*
     * Write to the socket.
     * <p>
     * Data is written to the socket from the buffer so long as there is
     * data to write, the last write was successful (did send something)
     * and the maximum-per-selection has not been reached. The latter limits
     * the amount of output, so that one buffer, one giant huge buffer,
     * doesn't hog all the I/O, effectively stopping communications for all
     * other channels.
     */
    private void write(SelectionKey key) throws InterruptedException {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        SelectionKeyDataLink attachment = (SelectionKeyDataLink) key.attachment();

        ByteBufferSource buffersource;
        ByteBuffer buffer;
        int tokens = IOSIZE;//taken from the idea of a TBF

        buffersource = attachment.writedataqueue.peek();

        try {
            buffer = buffersource.getByteBuffer();

            int lastcount;

            do {
                lastcount = socketChannel.write(buffer);
                tokens -= lastcount;
            } while (buffer.hasRemaining() && lastcount > 0 && tokens > 0);
        } catch (IOException e) {
            /*
             * Don't expect that this is an error caused by the outbound side the socket
             * being closed. The design of this class should not allow for queuing of
             * data to me written if the application has closed the outbound side independent
             * of closing the inbound side. This should me an exception for some other reason
             * and justifies shutting down the whole connection.
             */
            close(key);
            return;
        }

        if (buffersource.isEmpty())
            attachment.writedataqueue.poll();

        if (duplexing == Duplexing.HALF)
            read(key);

        // in full duplex mode we'd just remove the write op
        if (attachment.writedataqueue.isEmpty()) {
            if (socketChannel.socket().isInputShutdown())
                key.interestOps(0);//cannot read anymore
            else
                key.interestOps(SelectionKey.OP_READ);
        }
    }

    // for clients
    private void finishConnection(SelectionKey key) throws InterruptedException {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        // Finish the connection. If the connection operation failed
        // this will raise an IOException.
        try {
            socketChannel.finishConnect();
        } catch (IOException e) {
            try {
                key.channel().close();
            } catch (IOException e1) {
                logger.error("trying to close socket", e1);
            }
            try {
                ((SelectionKeyDataLink) key.attachment()).protocol.asyncException(new DataLinkProblem(e));
            } catch (RuntimeException e2) {
                logger.error("trying to send exception to protocol", e2);
            }

            return;
        }

        // Register an interest in reading on this channel
        key.interestOps(SelectionKey.OP_READ);

        SelectionKeyDataLink attachment = (SelectionKeyDataLink) key.attachment();

        try {
            attachment.protocol.asyncConnected(attachment);
        } catch (RuntimeException e) {
            logger.error("trying to connect to protocol", e);
            try {
                key.channel().close();
            } catch (IOException e1) {
                logger.error("trying to close socket", e1);
            }
        }
    }

    /*
     * Convenience since closing has so much stinking exception management
     * around the thing.
     */
    private void close(SelectionKey key) {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        try {
            socketChannel.close();
        } catch (IOException e1) {
            logger.error("trying to close socket", e1);
        }

        try {
            ((SelectionKeyDataLink) key.attachment()).protocol.asyncClosed();
        } catch (RuntimeException e1) {
            logger.error("trying to close protocol", e1);
        }
    }

}
