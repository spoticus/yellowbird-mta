/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.nio;

import java.nio.ByteBuffer;
import java.io.IOException;

/**
 * A container of one or more ByteBuffers to me processed in a particular order.
 */
interface ByteBufferSource {
    /**
     * Get next byte buffer containing data.
     *
     * @return A ByteBuffer with data. Possibly empty even when more data might exists.
     *         Use #isEmpty to determine if there is more data available.
     * @throws java.io.IOException when I/O fails.
     */
    ByteBuffer getByteBuffer() throws IOException;

    /**
     * Determine if there is any more data.
     *
     * @return true if there is more data, otherwise false.
     */
    boolean isEmpty();
}
