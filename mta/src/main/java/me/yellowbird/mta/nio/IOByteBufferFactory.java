/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.nio;

import java.lang.ref.SoftReference;
import java.nio.ByteBuffer;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Factory to create byte buffers
 */
public class IOByteBufferFactory {
    private final int capacity;

    private final ConcurrentLinkedQueue<SoftReference<IOByteBuffer>> availablebuffers = new ConcurrentLinkedQueue<SoftReference<IOByteBuffer>>();

    /**
     * Create factory
     *
     * @param capacity Size of byte buffers created by this factory
     */
    public IOByteBufferFactory(int capacity) {
        this.capacity = capacity;
    }

    /**
     * Allocate a buffer
     * <p>
     * Re-use a previously freed buffer or allocate a new buffer from memory if none available.
     * @return a buffer with a position of zero and a limit the size of the buffer.
     */
    public IOByteBuffer allocate() {
        SoftReference<IOByteBuffer> ref;

        //reuse
        while((ref = availablebuffers.poll()) != null) {
            IOByteBuffer buffer = ref.get();

            if (buffer != null) {
                buffer.getByteBuffer().clear();
                return buffer;
            }
        }

        //allocate
        return new IOByteBuffer(ByteBuffer.allocateDirect(capacity));
    }

    /**
     * Return a byte buffer to the pool of available buffers
     * <p>
     * After the call this byte buffer is no longer available to the thread calling
     * this method or any threads depending on a previous value.
     * @param buffer the buffer to free. If the capacity does not match the capacity of this
     * factory then the buffer is silently ignored and not re-used by this factory.
     */
    public void free(IOByteBuffer buffer) {
        if (buffer.getByteBuffer().capacity() != capacity)
            return;

        availablebuffers.add(new SoftReference<IOByteBuffer>(buffer));
    }
}
