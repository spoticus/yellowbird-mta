/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.brain;

/**
 * Configuration of an inbound connection.
 * <p>
 * Inbound connections are server sockets.
 */
public class InboundConnectionConfiguration {
    private String host;
    private int port;

    /**
     * Construct from host name and port.
     * @param host host name as in {@link java.net.InetAddress#getByName}.
     * @param port port number to bind on host.
     */
    public InboundConnectionConfiguration(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public InboundConnectionConfiguration(String host) {
        this.host = host;
        port = 25;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }
}
