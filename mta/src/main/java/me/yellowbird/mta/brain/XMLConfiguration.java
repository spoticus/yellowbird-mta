/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.brain;

import me.yellowbird.mta.Domain;
import me.yellowbird.mta.protocol.LineData;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Iterator;

/**
 * Configuration from an XML file.
 */
public class XMLConfiguration implements Configuration {

    private final me.yellowbird.mta.brain.EnvelopeBusListenerConfiguration[] envelopeBusListenerConfigurations;
    private final InboundConnectionConfiguration[] inboundConnectionConfigurations;
    private final String[] spoolerPatternConfigurations;
    private final String spoolerMessageDirectory;
    private final int retryinterval;
    private final int giveupinterval;
    private LineData welcomeMessage;
    private Domain ehloDomain;
    private LineData ehloGreeting;

    /**
     * Construct configuration from an file containing the XML.
     *
     * @param configurationxml URL of xml file containing configuration.
     * @throws ConfigurationException an error exists in the configuration.
     * @throws FileNotFoundException thrown when the configuraton file cannot me found.
     */
    public XMLConfiguration(URL configurationxml) throws ConfigurationException, IOException {
        this(configurationxml.openStream());
    }

    public XMLConfiguration(InputStream configurationxml) throws ConfigurationException {
        Document document;

        try {
// parse an XML document into a DOM tree
            DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
            dbfactory.setNamespaceAware(true);
            DocumentBuilder parser = dbfactory.newDocumentBuilder();
            document = parser.parse(configurationxml);

            // create a SchemaFactory capable of understanding WXS schemas
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

            // load a WXS schema, represented by a Schema instance
            InputStream xsd = XMLConfiguration.class.getResourceAsStream("brainconfig.xsd");
            Source schemaFile = new StreamSource(xsd);
            Schema schema = factory.newSchema(schemaFile);

            // create a Validator instance, which can me used to validate an instance document
            Validator validator = schema.newValidator();

            // validate the DOM tree
            validator.validate(new DOMSource(document));

            /*
             * Now extract the configuration data to conveniently accessible fields.
             */
            XPath xpath = XPathFactory.newInstance().newXPath();

            xpath.setNamespaceContext(new NamespaceContext() {
                @Override
                public String getNamespaceURI(String prefix) {
                    return "http://www.yellowbird.me/2008/BrainConfiguration";
                }

                @Override
                public String getPrefix(String namespaceURI) {
                    return "brain";
                }

                @Override
                public Iterator getPrefixes(String namespaceURI) {
                    return null;
                }
            });
//            Node node;
            NodeList nodelist;
            Node firstnode = document.getFirstChild();

            //find envelopebuslisteners
            nodelist = (NodeList) xpath.evaluate("/brain:mta/brain:envelopeBus/brain:envelopeBusListener", firstnode, XPathConstants.NODESET);

            envelopeBusListenerConfigurations = new EnvelopeBusListenerConfiguration[nodelist.getLength()];

            for (int i = 0; i < envelopeBusListenerConfigurations.length; ++i) {
                Node nl = nodelist.item(i);

                NamedNodeMap alltheattributes = nl.getAttributes();

                Node idattribute = alltheattributes.getNamedItem("id");

                Node classattribute = alltheattributes.getNamedItem("class");

                envelopeBusListenerConfigurations[i] = new me.yellowbird.mta.brain.EnvelopeBusListenerConfiguration(idattribute.getNodeValue(), classattribute.getNodeValue());
            }

            //find inboundconnections
            nodelist = (NodeList) xpath.evaluate("/brain:mta/brain:inboundConnections/brain:server", firstnode, XPathConstants.NODESET);

            inboundConnectionConfigurations = new InboundConnectionConfiguration[nodelist.getLength()];

            for (int i = 0; i < inboundConnectionConfigurations.length; ++i) {
                Node nl = nodelist.item(i);

                NamedNodeMap alltheattributes = nl.getAttributes();

                Node hostattribute = alltheattributes.getNamedItem("host");

                Node portattribute = alltheattributes.getNamedItem("port");

                if (portattribute != null)
                    inboundConnectionConfigurations[i] = new InboundConnectionConfiguration(hostattribute.getNodeValue(), Integer.parseInt(portattribute.getNodeValue()));
                else
                    inboundConnectionConfigurations[i] = new InboundConnectionConfiguration(hostattribute.getNodeValue());
            }

            //find spooler patterns
            nodelist = (NodeList) xpath.evaluate("/brain:mta/brain:envelopeBus/brain:spooler/brain:pattern", firstnode, XPathConstants.NODESET);

            spoolerPatternConfigurations = new String[nodelist.getLength()];

            for (int i = 0; i < spoolerPatternConfigurations.length; ++i) {
                Node nl = nodelist.item(i);

                spoolerPatternConfigurations[i] = nl.getTextContent();
            }

            //find message directory
            Node spoolernode = (Node) xpath.evaluate("/brain:mta/brain:envelopeBus/brain:spooler", firstnode, XPathConstants.NODE);

            NamedNodeMap alltheattributes = spoolernode.getAttributes();

            Node spoolerattribute = alltheattributes.getNamedItem("messageDirectory");

            spoolerMessageDirectory = spoolerattribute.getNodeValue();

            //retry and give-up intervals
            spoolerattribute = alltheattributes.getNamedItem("retryInterval");

            if (spoolerattribute != null)
                retryinterval = Integer.parseInt(spoolerattribute.getNodeValue());
            else
                retryinterval = 30;//default in minutes

            spoolerattribute = alltheattributes.getNamedItem("giveupInterval");

            if (spoolerattribute != null)
                giveupinterval = Integer.parseInt(spoolerattribute.getNodeValue());
            else
                giveupinterval = 5 * 24 * 60;//default in minutes (5 days)

            //welcome message
            Node welcomemessagenode = (Node) xpath.evaluate("/brain:mta/brain:welcomeMessage", firstnode, XPathConstants.NODE);

            if (welcomemessagenode != null)
                welcomeMessage = new LineData(welcomemessagenode.getTextContent());
            else
                welcomeMessage = new LineData("");

            //ehlo domain
            Node ehlodomainnode = (Node) xpath.evaluate("/brain:mta/brain:ehloDomain", firstnode, XPathConstants.NODE);

            ehloDomain = new Domain(ehlodomainnode.getTextContent());

            //ehlo greeting
            Node ehlogreetingnode = (Node) xpath.evaluate("/brain:mta/brain:ehloGreeting", firstnode, XPathConstants.NODE);

            if (ehlogreetingnode != null)
                ehloGreeting = new LineData(ehlogreetingnode.getTextContent());
            else
                ehloGreeting = new LineData("");
            
        } catch (Throwable e) {
            throw new ConfigurationException(e);
        }


    }

    @Override
    public EnvelopeBusListenerConfiguration[] getEnvelopeBusListenerConfigurations() throws ConfigurationException {
        return envelopeBusListenerConfigurations;
    }

    @Override
    public InboundConnectionConfiguration[] getInboundConnectionConfiguration() throws ConfigurationException {
        return inboundConnectionConfigurations;
    }

    @Override
    public String[] getSpoolerPatterns() throws ConfigurationException {
        return spoolerPatternConfigurations;
    }

    @Override
    public String getMessageDirectory() throws ConfigurationException {
        return spoolerMessageDirectory;
    }


    @Override
    public int getRetryInterval() {
        return retryinterval;
    }

    @Override
    public int getGiveupInterval() throws ConfigurationException {
        return giveupinterval;
    }

    @Override
    public LineData getWelcomeMessage() throws ConfigurationException {
        return welcomeMessage;
    }

    @Override
    public Domain getEHLODomain() throws ConfigurationException {
        return ehloDomain;
    }

    @Override
    public LineData getEHLOGreeting() throws ConfigurationException {
        return ehloGreeting;
    }
}
