/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.brain;

import me.yellowbird.mta.transport.EnvelopeBusListener;

/**
 * Configuration for an envelope bus listener.
 * <p>
 * The configuration either an instance of {@linkplain me.yellowbird.mta.transport.EnvelopeBusListener}
 * or name name of a class that implements {@linkplain me.yellowbird.mta.transport.EnvelopeBusListener}
 * or the name of a class that implements {@linkplain me.yellowbird.mta.transport.EnvelopeBusListenerFactory}.
 * <p>
 * If the configuration contains then name of a class and if that class implements the factory then
 * a static create method is used to create the instances that are added to the envelope bus. If not then it
 * must implement one of the listener interfaces and a constructor is called on the implementing class. If a
 * class implements both the factory and a listener interface then the factory interface is used to create instances.
 * <p>
 * If the configuration contains an instance of an EnvelopeBusListener then that instance is used
 * for the envelope bus. 
 */
public class EnvelopeBusListenerConfiguration {
    private final String id;
    private final String classname;
    private final EnvelopeBusListener envelopeBusListener;

    public EnvelopeBusListenerConfiguration(String id, String classname) {
        this.id = id;
        this.classname = classname;
        envelopeBusListener =  null;
    }

    public EnvelopeBusListenerConfiguration(String id, Class clazz) {
        this.id = id;
        this.classname = clazz.getCanonicalName();
        envelopeBusListener =  null;
    }

    public EnvelopeBusListenerConfiguration(String id, EnvelopeBusListener envelopeBusListener) {
        this.id = id;
        classname = null;
        this.envelopeBusListener = envelopeBusListener;
    }

    public String getId() {
        return id;
    }

    public String getClassname() {
        return classname;
    }

    public EnvelopeBusListener getEnvelopeBusListener() {
        return envelopeBusListener;
    }
}
