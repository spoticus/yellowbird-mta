/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.brain;

import me.yellowbird.mta.ASCIIException;
import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.Domain;
import me.yellowbird.mta.forwarder.SnmpForwarder;
import me.yellowbird.mta.io.Protocol;
import me.yellowbird.mta.io.ProtocolAsynchronizer;
import me.yellowbird.mta.mps.FileFormatException;
import me.yellowbird.mta.mps.FileStorage;
import me.yellowbird.mta.mps.StorageException;
import me.yellowbird.mta.nio.ChamillionSocketTransport;
import me.yellowbird.mta.protocol.ClientProtocol;
import me.yellowbird.mta.protocol.LineData;
import me.yellowbird.mta.protocol.ServerProtocol;
import me.yellowbird.mta.spooler.MPSSpooler;
import me.yellowbird.mta.spooler.SpoolerException;
import me.yellowbird.mta.transport.*;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xbill.DNS.*;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.regex.Pattern;

/**
 * The brain is concerned with MTA behavior
 * <p/>
 * See Singleton Pattern
 * <p/>
 * The brain collects statistics from the MTA and adjust it's parameters to maintain
 * optimal performance. It also watches for Exceptions and provides for degraged behavior
 * that maintains system integrity.
 * <p/>
 * The brain can:
 * <ul>
 * <li>Throttle transports
 * <li>Switch inbound message persistence modes
 * <li>Provide external systems with layer3 performance suggestions
 * <li>React to higher-level operating system performance issues
 * </ul>
 * <p/>
 * The brain also initiates processing of defered, spooled inbound messages.
 * Where an external MTA or MUA initiates processing of external inbound messages
 * the brain is the source for the periodic initiating of processing of spooled
 * messages.
 * <p/>
 * The brain also provides for thread creation for the entire system so that
 * threads are organized and managable from the brain. The number of threads in
 * the system is careful architected and thread creation should not me ad-hoc
 * outside of this architecture.
 * <p/>
 * The brain is also the source for environment configuration, such as port numbers
 * and addresses for the inbound transport tcp servers.
 * <p/>
 * A brain connects with it's environment, the operating system or container, using
 * a {@link BrainStem} that provides connectors for receiving environment messages
 * and sending messages to the environment.
 * <p/>
 * The brain can create and destroy servers and clients independent of it's life cycle,
 * allowing the brain to dynamically control tcp services during the life of the
 * MTA's executing, eliminating the need to completely shutdown the MTA for tcp
 * changes.
 * <h3>Brain State Machine</h3>
 * <pre>
 *     *
 *     V
 *  ---------
 * | STARTUP |
 *  ---------
 *     V
 *  ---------
 * | READY   |
 *  ---------
 * </pre>
 */
//TODO need aspect on brainstem to identifying saturation and switch to sckitzophrenic
//TODO java.lang.Runtime.addShutdownHook,http://java.sun.com/javase/6/webnotes/trouble/TSG-VM/html/gbzbl.html
//TODO the brain is the source for all threads and thread.connectServer calls. need to wrap each with recovery logic.

public class Brain implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger("brain");

    private LineData welcomemessage;
    private Domain ehlodomain;
    private LineData ehlogreeting;

    /*
     * Threading:
     *
     * <pre>
     * masterThreadGroup
     *   NIOThreadGroup
     *     CSTSelectorThread
     *   protocolThreadGroup
     *     asynchronizerThreads[]
     *     forwarderThreads[]
     *   spoolerThreadGroup
     *     spoolerThread
     *   brainThreadGroup
     *     thinkerThread
     * <<inherited thread for brain run()>>
     * </pre>
     */
    // A master threadgroup for all MTA threads and thread groups
    private final ThreadGroup masterThreadGroup;

    // A threadgroup for protocol threads
    private final ThreadGroup protocolThreadGroup;
    private final Thread asynchronizerThread;

    // All nio, server and client, belong to a common thread group
    private final ThreadGroup NIOThreadGroup;

    // All brain threads belong to a common thread group
    private final ThreadGroup brainThreadGroup;

    private final ThreadGroup spoolerThreadGroup;

    // tcp communication socket service for clients and servers
    private final ChamillionSocketTransport cst;
    private final Thread CSTSelectorThread;

    private final Thread spoolerThread;
    private final MPSSpooler spooler;

    /*
     * Brain stem thread
     */
//    private final Thread brainstemthread;

    private BrainStem brainstem;
    private final ProtocolAsynchronizer ioasynchronizer;
    private EnvelopeBus envelopebus;
    private final Map<String, InetSocketAddress[]> mailExchangeOverrides = new HashMap<String, InetSocketAddress[]>();

    private final Set<Thread> forwarderthreads = new HashSet<Thread>();
    private int retryInterval;
    private int giveupInterval;

    public Brain(Configuration config, BrainStem stem) throws ASCIIException, IOException, ConfigurationException, FileFormatException, SpoolerException, StorageException {
        brainstem = stem;

        welcomemessage = config.getWelcomeMessage();

        ehlodomain = config.getEHLODomain();

        ehlogreeting = config.getEHLOGreeting();

        retryInterval = config.getRetryInterval();

        giveupInterval = config.getGiveupInterval();

        ArrayList<EnvelopeBusListener> envelopebuslisteners =
                new ArrayList<EnvelopeBusListener>();

        /*
         * The thread groups and threads.
         */
        masterThreadGroup = new ThreadGroup("MTA");
        spoolerThreadGroup = new ThreadGroup(masterThreadGroup, "Spoolers");
        NIOThreadGroup = new ThreadGroup(masterThreadGroup, "ServersClientsNIO");
        brainThreadGroup = new ThreadGroup(masterThreadGroup, "BrainActivity");
        protocolThreadGroup = new ThreadGroup(masterThreadGroup, "Protocols");


        /*
         * Setup the envelopebus.
         * - all the configured listeners
         * - the spooler
         * - the default listener
         */


        EnvelopeBusListenerConfiguration[] ebls = config.getEnvelopeBusListenerConfigurations();

        for (EnvelopeBusListenerConfiguration ebl : ebls) {
            EnvelopeBusListener eblinstance;

            eblinstance = ebl.getEnvelopeBusListener();

            if (eblinstance == null) {
                try {
                    Class eblclass = Class.forName(ebl.getClassname());
                    if (EnvelopeBusListenerFactory.class.isAssignableFrom(eblclass)) {
                        Method staticinit = eblclass.getMethod("createEnvelopeBusListener", String.class);
                        if (staticinit != null) {
                            if ((staticinit.getModifiers() & 0x0008) != 0) {
                                eblinstance = (EnvelopeBusListener) staticinit.invoke(null, ebl.getId());
                                envelopebuslisteners.add(eblinstance);
                                continue;
                            }
                        }
                    }
                    Constructor<EnvelopeBusListener> eblconstruction = eblclass.getConstructor();
                    eblinstance = eblconstruction.newInstance();
                } catch (ClassNotFoundException e) {
                    throw new ConfigurationException(e);
                } catch (NoSuchMethodException e) {
                    throw new ConfigurationException(e);
                } catch (InvocationTargetException e) {
                    throw new ConfigurationException(e);
                } catch (IllegalAccessException e) {
                    throw new ConfigurationException(e);
                } catch (InstantiationException e) {
                    throw new ConfigurationException(e);
                }
            }

            envelopebuslisteners.add(eblinstance);
        }

        ArrayList<Pattern> spoolerpatterns = new ArrayList<Pattern>();
        for (String s : config.getSpoolerPatterns()) {
            spoolerpatterns.add(Pattern.compile(s));
        }
        FileStorage storage = new FileStorage(new File(config.getMessageDirectory()));

        spooler = new MPSSpooler(storage, spoolerpatterns, this);
        envelopebuslisteners.add(spooler.getEnvelopeBusPlugin());

        envelopebus = new EnvelopeBus(envelopebuslisteners);

        /*
         * Start threads (order is important to avoid running out of memory on initial high-load):
         * chamillion transport (no server sockets)
         * forwarders (can relieve outgoing pressure)
         * spooler (creates outgoing pressure, relieves incoming work queue)
         * asynchronizer (server protocol thread, relieves incoming pressure, adds to work queue)
         * <<connect server on transport>> (creates incoming pressure)
         */

        cst = new ChamillionSocketTransport(this);

        CSTSelectorThread = new Thread(NIOThreadGroup, cst, "ChamillionSocketTransport " + cst.toString());

        CSTSelectorThread.start();

        Thread t = new Thread(protocolThreadGroup, new SnmpForwarder(spooler, this), "SnmpForwarder ");
        forwarderthreads.add(t);
        t.start();

//        spooler.addForwarder(protocolThreadGroup);

        spoolerThread = new Thread(spoolerThreadGroup, spooler, "Spooler");

        spoolerThread.start();

        ioasynchronizer = new ProtocolAsynchronizer();

        asynchronizerThread = new Thread(protocolThreadGroup, ioasynchronizer, "IO ProtocolAsynchronizer");

        asynchronizerThread.start();


        /*
         * configured server and client sockets.
         */
        for (InboundConnectionConfiguration z : config.getInboundConnectionConfiguration()) {
            cst.connectServer(new InetSocketAddress(z.getHost(), z.getPort()));
        }
    }


    /**
     * Is the MTA ready to receive new connections
     *
     * @return true if the MTA can accept a new connection. The brain provides this
     *         information so that it can me communicated to the outside world, but is not
     *         used by the brain to prohibit connections through it's operating system
     *         connector.
     */
    public boolean isMTAReadyForNewConnections() {
        return cst.isServerReady();
    }

    /**
     * Get the greeting to return to a client on a new connection
     *
     * @return welcome message.
     */
    public LineData getWelcomeMessage() {
        return welcomemessage;
    }

    /**
     * Get the domain to return to client on EHLO or HELO command
     *
     * @return domain
     */
    public Domain getEHLODomain() {
        return ehlodomain;
    }

    /**
     * Get the greeting to return to client on EHLO or HELO command
     *
     * @return greeting
     */
    public LineData getEHLOGreeting() {
        return ehlogreeting;
    }

    /**
     * Create server for this MTA
     * <p/>
     * Create a server for this address.
     *
     * @param serverAddress address and socket to use for server.
     * @throws java.io.IOException when something goes wrong.
     */
//    public void createServer(InetSocketAddress serverAddress) throws IOException {
//        cst.connectServer(serverAddress);
//    }

    /**
     * Create client for this MTA
     * <p/>
     * Create a client for this address.
     *
     * @param clientaddress address of remote server
     * @throws java.io.IOException passthrough
     */
//    public void createClient(InetSocketAddress clientaddress) throws IOException {
//        sockettransport.connectClient(clientaddress, new ClientProtocol());
//    }

    /**
     * The breath of life enters here
     */
    public void run() {
//        brainstemthread.run();

        brainstem.sendToNerves(IAmAlive.class);

        try {
            while (true) {
                BrainMessage m = brainstem.take();

                if (m instanceof Shutdown) {
                    break;
                }
            }
        } catch (InterruptedException e) {
            brainstem.sendToNerves(new ThrowableHolder(e));
        } finally {
            try {
                shutdown();
            } catch (InterruptedException e) {
                //noinspection ThrowFromFinallyBlock
                throw new RuntimeException(e);//i think i want to stop the remaining "normal" process since someone is aborting this thing
            }
            brainstem.sendToNerves(new IAmDying());

        }
    }

    private void shutdown() throws InterruptedException {
        /*
         * Shutdown is reverse of startup.
         */
        logger.debug("disconnect servers");
        cst.disconnectServers();
        asynchronizerThread.interrupt();
        logger.debug("join asynchronizer thread");
        asynchronizerThread.join();
        spoolerThread.interrupt();
        logger.debug("join spooler thread");
        spoolerThread.join();

        for (Thread forwarderthread : forwarderthreads) {
            forwarderthread.interrupt();
        }

        for (Thread forwarderthread : forwarderthreads) {
            logger.debug("join forwarder thread");
            forwarderthread.join();
        }

//        spooler.shutdownForwarders();
        cst.shutdown();
        logger.debug("join cst thread");
        CSTSelectorThread.join();

        logger.debug("shutdown complete");
    }

    public InboundMessageBuilder createInboundMessageBuilder() {
        return new EnvelopeBusInboundMessageBuilder(MessageIdentifier.createMessageIdentifier(), envelopebus);
    }

    public Protocol createServerProtocol() {
        return ioasynchronizer.createProxy(new ServerProtocol(this, createInboundMessageBuilder()));
    }

    public ClientProtocol createClientProtocol(AddrSpec reversepath, AddrSpec forwardpath) {
        return new ClientProtocol(reversepath, forwardpath, ioasynchronizer, this);
    }

    /**
     * Get best transport for reaching a remote MTA.
     * <p/>
     * For a particular remote internet address, choose the transport that has the best
     * client socket for routing a message. "Best" based on subjective "Brain" thought.
     *
     * @param remoteaddress internet address of remote MTA.
     * @return the {@link ChamillionSocketTransport transport} that can make a connection to the remoteaddress or null if no client can me found.
     */
    public ChamillionSocketTransport getBestChamillionSocketTransport(InetAddress remoteaddress) {
        return cst;
    }

    public void putMailExchangeOverride(String domain, InetSocketAddress... exchanges) {
        mailExchangeOverrides.put(domain, exchanges);
    }

    /**
     * Get MX addresses for a domain in priority order.
     * <p/>
     * If the domain has an override list of mail exchangers then that list is returned. Else, the
     * MX records are retrieved from DNS for the domain and the addresses for those MX records are returned
     * in priority order according to the MX records within the SOA.
     *
     * @param domain the domain name.
     * @return a queue containing the MX addresses for the domain in the order on the queue that matches the priority order from DNS or from the forwarding overrides. The queue will be empty if there is no MX records found.
     * @throws org.xbill.DNS.TextParseException
     *          problem with domain name.
     */
    public List<InetSocketAddress> getMX(String domain) throws TextParseException {
        InetSocketAddress[] override = mailExchangeOverrides.get(domain);

        if (override != null)
            return Arrays.asList(override);

        // queue that sorts MX records by their priority
        Queue<MXRecord> mxqueue = new PriorityQueue<MXRecord>(10, new Comparator<MXRecord>() {
            public int compare(MXRecord o1, MXRecord o2) {
                return o1.getPriority() - o2.getPriority();
            }
        });

        // find ip address for forward-path MX
        Record[] records;

        records = new Lookup(domain, Type.MX).run();

        if (records != null)
            for (Record rec : records) {
                if (rec instanceof MXRecord)
                    mxqueue.add((MXRecord) rec);

            }

        LinkedList<InetSocketAddress> addresses = new LinkedList<InetSocketAddress>();

        MXRecord mx;

        while ((mx = mxqueue.poll()) != null) {
            addresses.add(new InetSocketAddress(mx.getTarget().toString(), 25));

        }

        return addresses;
    }

    /**
     * Get reverse-DNS name of host for address.
     *
     * @param address the ip address to lookup.
     * @return the host name from the PTR record or null if not available.
     */
    public String getPTR(InetAddress address) {
        return null;  //To change body of created methods use File | Settings | File Templates.
    }

    public int getRetryInterval() {
        return retryInterval;
    }

    public Period getGiveUpDuration() {
        return Period.minutes(giveupInterval);
    }
}
