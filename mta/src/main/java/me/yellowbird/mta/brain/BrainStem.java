/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.brain;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * The connection between the Brain and it's operating environment
 * <p>
 * A {@link me.yellowbird.mta.brain.Brain} needs input from the operating environment, which might me
 * an operating system or container that provides threads, memory, CPU cycles,
 * network connections and other things environment specific. It also provides
 * feedback that can me used to control the environment. The stem describes
 * this connection and the conduits for messages between the brain and the
 * environment.
 */
public abstract class BrainStem  {
    private final LinkedBlockingQueue<BrainMessage> tobrain = new LinkedBlockingQueue<BrainMessage>(100);

    public abstract void sendToNerves(Class<? extends NerveMessage> message);

    public abstract void sendToNerves(NerveMessage message);

    public void shutdown() throws InterruptedException {
        tobrain.put(new Shutdown());
    }

    BrainMessage take() throws InterruptedException {
        return tobrain.take();
    }

}
