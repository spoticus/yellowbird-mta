/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.brain;

import me.yellowbird.mta.Domain;
import me.yellowbird.mta.protocol.LineData;
import me.yellowbird.mta.transport.EnvelopeBusListener;

/**
 * Brain, ergo, system configuration.
 *
 */
public interface Configuration {
    /**
     * Return list of envelopebus listener configurations.
     * <p>
     * The names of classes that implement {@link EnvelopeBusListener} to add to the
     * {@link me.yellowbird.mta.transport.EnvelopeBus} that can me
     * resolved with {@link Class#forName). The spooler and the default listener
     * are automatically added to the end of the list.
     * 
     * @return the array of listeners in the order they attached to the envelope bus, which is the order that messages
     * are passed to the listeners. Null is an acceptable response meaning no additional listeners other than default
     * are attached.
     * @throws ConfigurationException the configuration is invalid.
     */
    public EnvelopeBusListenerConfiguration[] getEnvelopeBusListenerConfigurations() throws ConfigurationException;

    /**
     * Return list of inbound connection configurations.
     * <p>
     * @return an array of the inbound connection configurations.
     * @throws ConfigurationException the configuration is invalid.
     */
    public InboundConnectionConfiguration[] getInboundConnectionConfiguration() throws ConfigurationException;

    /**
     * Return list of regex patterns for the spooler.
     *
     * @return array of regex patterns used by spooler to match against forward-addresses.
     * @throws ConfigurationException the configuration is invalid.
     */
    public String[] getSpoolerPatterns() throws ConfigurationException;

    /**
     * Return the name of the directory where the spooler will store incoming messages.
     *
     * @return the name of a directory that can me passed to java.io.File.
     * @throws ConfigurationException the configuration is invalid.
     */
    public String getMessageDirectory() throws ConfigurationException;

    /**
     * Return the interval the spooler waits before attempting to re-deliver a mail object.
     * <p>
     * This is a per-forward-path interval.
     *
     * @return the number of minutes the spooler waits before attempting to re-deliver a mail object.
     * @throws ConfigurationException the configuration is invalid.
     */
    public int getRetryInterval() throws ConfigurationException;

    /**
     * Return the interval the spooler keeps an undeliverable mail object before giving up.
     * <p>
     * This is an interval for all forward-paths for a mail object, regardless of
     * the ability to deliver to any of the forward-paths. See RFC2821.
     *
     * @return the number of minutes the spooler keeps an undeliverable mail object before giving up.
     * @throws ConfigurationException the configuration is invalid.
     */
    public int getGiveupInterval() throws ConfigurationException;

    /**
     * Return welcome message used at start of SNMP conversation.
     *
     * @return message that server sends to client.
     * @throws ConfigurationException the configuration is invalid.
     */
    public LineData getWelcomeMessage() throws ConfigurationException;

    /**
     * Return domain used at start of SNMP conversation.
     *
     * @return the domain used in the EHLO command sent by the client to a server.
     * @throws ConfigurationException the configuration is invalid.
     */
    public Domain getEHLODomain() throws ConfigurationException;

    /**
     * Retrun greeting used at start of SNMP conversation in response to EHLO command.
     *
     * @return the message sent by the server to the client in reponse to the client's EHLO command.
     * @throws ConfigurationException the configuration is invalid.
     */
    public LineData getEHLOGreeting() throws ConfigurationException;

}
