package me.yellowbird.mta.mda;

import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.MailParameter;
import me.yellowbird.mta.protocol.LineData;
import me.yellowbird.mta.protocol.LineOfData;
import me.yellowbird.mta.protocol.MessageDataCommandReply;
import me.yellowbird.mta.transport.*;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.*;
import java.util.ArrayList;

/**
 * MDA providing Maildir format.
 */
public class Maildir implements MAILListener, RCPTListener {
    @Override
    public void doMAIL(MessageIdentifier msgid, AddrSpec addrSpec, ArrayList<MailParameter> mailparams) throws ReversePathException {

    }

    @Override
    public SinkTransport doRCPT(MessageIdentifier msgid, AddrSpec addrSpec, ArrayList<MailParameter> mailparams) throws ForwardPathException {
        if (addrSpec.isNullAddress())
            return null;

        try {
            FileSystem fileSystem = FileSystems.getFileSystem(new URI("file:///var/mta"));

            String home = addrSpec.getLocalPart();

            Path tempFile = Files.createTempFile(fileSystem.getPath(home, "Maildir", "new"), "mta", ";2");

            return new WriteSinkTransport(tempFile);
        } catch(Throwable t) {
            return null;
        }
    }

    public static class WriteSinkTransport implements SinkTransport {
        private final SeekableByteChannel channel;
        private final Path path;

        public WriteSinkTransport(Path path) throws IOException {
            this.path = path;
            channel = Files.newByteChannel(path, StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        }

        @Override
        public void prepareForData() throws MessageDataException {

        }

        @Override
        public void add(LineOfData data) throws MessageDataException {
            try {
                channel.write(data.getByteBuffer());
            } catch (IOException e) {
                throw new MessageDataException(new MessageDataCommandReply(MessageDataCommandReply.Code.E451, LineData.strip("Temporary error")), e);
            }
        }

        @Override
        public void commit() throws MessageDataException {
            try {
                channel.close();
            } catch (IOException e) {
                throw new MessageDataException(new MessageDataCommandReply(MessageDataCommandReply.Code.E451, LineData.strip("Temporary error")), e);
            }
        }

        @Override
        public void cancel() {
            try {
                channel.close();

                Files.delete(path);
            } catch (IOException e) {
                //absorb
            }

        }
    }
}
