<!--
  ~ ***** BEGIN LICENSE BLOCK *****
  ~ Version: MPL 1.1
  ~
  ~ The contents of this file are subject to the Mozilla Public License Version
  ~ 1.1 (the "License"); you may not use this file except in compliance with
  ~ the License. You may obtain a copy of the License at
  ~ http://www.mozilla.org/MPL/
  ~
  ~ Software distributed under the License is distributed on an "AS IS" basis,
  ~ WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  ~ for the specific language governing rights and limitations under the
  ~ License.
  ~
  ~ The Original Code is YellowBird Mail Transfer Agent.
  ~
  ~ The Initial Developer of the Original Code is
  ~ Clay Atkins.
  ~ Portions created by the Initial Developer are Copyright (C) 2009
  ~ the Initial Developer. All Rights Reserved.
  ~
  ~ Contributor(s):
  ~
  ~ ***** END LICENSE BLOCK *****
  -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
<head>
</head>
<body bgcolor="white">
An Mail Transport Agent ("MTA") is a component of email services that provides
for the transport of email between systems. The MTA contained in this package
implements RFC2821 and RFC2822 which are the RFCs that described the de-facto
Internet email protocol and message format, respectively.

<h2>Package Specification</h2>

The package contains all of the classes necessary for a complete MTA, but lack a
mechanism to interact with the operating system or container providing an initial
thread. Related packages provide for several different mechanism to start the
MTA within such a context.
<p>
The MTA fulfills all the requirements of the related RFCs which themselves
the behavior as it relates interacting with other MTAs and MUAs and the protocol
between them. The implementation favors some general intentions and includes
specific objects to stear the implementation details:
</p>
<ul>
    <li>Speed</li>
    <li>Efficient linking of inbound messages to outbound processes</li>
</ul>


<h3>General Architecture</h3>
<p>
The major architectural components are {@link InboundTransport} and
{@link OutboundTransport}, which represent objects or subsystems (in extension)
that receive messages and send messages, respectively. The bulk of the system
is built around efficiently coupling configured InboundTransport objects
to OutboundTransport objects through messages and devices that couple the
Transport objects either synchronously or asynchronously.
</p>
<p>
    The whole system is RFC specific and not intended to be expanded to cover
    a diverse set of messaging protocols or message formats. A {@linkplain Message}
    is a class representing an RFC2822 message. This is done to focus the
    developer away from abstraction and towards efficiency and speed.
</p>
<p>
    An InboundTransport exists to implement RFC2821 and for asynchronously
    spooling messages (reading messages from persistent storage and sending them
    to an OutboundTransport). Spooled messages are re-materialized into message
    objects. Message data coming from an external MTA or MUA is transmogriphied
    into objects with the aid of a subsystem that uses the builder pattern (see {@link InboundMessageBuilder}).
    This subsystem incrementally constructs a "message container" as envelope headers are
    received. It also forwards these headers, and other state information, onto a
    "envelope bus", that is monitored by OutboundTransport objects, filters, loggers
    and other concerns regarding the envelope. Once the complete envelope has been received, the builder
    returns the built "message container" to the InboundTransport. As the RFC2822
    message arrives, the InboundTransport then directly writes the information
    to the message container. This provides a great deal o flexibility in the implementation
    of how messages are stored and several efficiencies in the movement of the message
    data.
</p>
<p>
    As mentioned, the builder puts headers on an envelope bus that is monitored by other
    objects, including the OutboundTransport objects. The listeners of the envelope bus
    have the option of modifying or rejecting a header as well as rejecting a message.
    OutboundTransport objects also have the option of accepting a message "to:" header.
    Every "to:" header must be accepted by at least one OutboundTransport or the builder
    will automatically reject the header and inform the InboundTransport.
</p>

<p>
    The system makes strategic use of threads to facilitate the efficient and expeditious
    flow of information and decision making:
</p>

<ul>
    <li>Server I/O Thread: The system uses NIO and a single thread provides both server I/O and servers as the selector
        thread. Incoming bytes are passed to the server protocol threads on server inbound data queues. Bytes are read
        from a server outbound data queue and transmitted to the client.
    </li>
    <li>RFC2821 Thread: The RFC2821 implementation uses nio and there is a single
        selector thread. Incoming envelope data is parsed and handed to the builder synchronously
        or asynchronously, depending upon the the brain's detection of processors, system
        load and throughput. When asynchronous, there is a second thread that processes
        (parses, interprets, maintains state, passes to builder)
        the bytes received on the socket and queues bytes to send to the socket.
    </li>
    <li>Envelope Bus Thread: Information on the envelope bus is fetched from the bus and
    handed, synchronously, to each bus listener.</li>
    <li>Spooler: The spooler is another source for messasges and runs on a thread for each
    instance of the spooler. There is an instance per persistent storage.</li>
</ul>

<h3>System Startup</h3>
<p>
    The brain singleton is responsible for proper system startup. It provides all the environment
    parameters, starts the threads, the inbound and outbound transports, and the envelope bus.
    The architecture is crafted so that any part of the system can be temporarily unavailable.
    This accomodates recovering from internal failures, such as an out of memory exception that crashes the
    envelope bus. It also makes it much easier to initialize the system without too much
    synchronization between the threads.
</p>

<h3>Unit and Integration Testing</h3>
<p>
    Testing includes unit testing for basic code coverage, collaboration testing for
    stressing the interaction between collaborating classes and integration test for
    testing scenarios and exceptions. Unit test for code coverage always exists for
    the class in a corresponding test class under the test folder with a package and
    name corresponding to the class being tested. The name of the testing class
    has a name derived from the class being tested and the suffix "ITest". As an
    example, to test <code>MyClass</code> there will be a unit test class called
    <code>MyClassITest</code>. Any exceptions to this practice are denoted in the
    class being tested. Collaboration test are likewise found under the test folder
    with a similar naming convention where the suffix is "CTest". To continue our
    example, the collaboration test would be called <code>MyClassCTest</code>. It
    is always the entry point of the collaboration that controls the naming. So if
    MyClass depended on MyDependent then the test class would be called <code>MyClassCTest</code>.
    As with unit test, any exceptions are denoted in the class being tested. This
    separation of concerns is for organization and to clarify to the developer
    what is being tested and to help determine that all required test exists. Both
    these test also contain boundary test calculated by the developers internal
    knowledge of the class as well as defect test. Both types are are annotated
    on the class to futher identify the purpose of the test and to inhibit removal
    of these test by refactoring.
</p>
<p>
    Scenario and exception test are related to functional requirements and not
    specifically to a particular class. These test are collected together under
    a common class called <code>IntegrationTest</code> and particular classes
    providing integration test have a suffix of "ITest" but with a prefix that
    might not correlate to any of the classes being tested. Like the other two
    types of test, integration test also include boundary test and and defect
    test and have the corresponding annotation for the same purpose.
</p>
<ul>
    <li>Smoke (integration)</li>
    <li>Boundary (integration and unit)</li>
    <li>Logic (unit)</li>
    <li>Exception (integration and unit)</li>
    <li>Bug (integration and unit)</li>
    <li>Performance (integration and unit)</li>
</ul>
<h2>Related Documentation</h2>

For overviews, tutorials, examples, guides, and tool documentation, please see:
<ul>
  <li><a href="../../../com/yellowbird/mta">##### REFER TO NON-SPEC DOCUMENTATION HERE #####</a>
</ul>

<!-- Put @see and @since tags down here. -->
@see http://www.faqs.org/rfcs/rfc2821.html
@see http://tools.ietf.org/html/rfc2821

</body>
</html>
