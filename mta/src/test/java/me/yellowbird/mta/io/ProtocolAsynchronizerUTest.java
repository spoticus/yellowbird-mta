/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.io;

import me.yellowbird.mta.protocol.ServerProtocol;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import testhelper.OnThreadHelper;

import java.nio.ByteBuffer;

/**
 * Unit Test Asynchronizer
 */
@RunWith(JMock.class)
public class ProtocolAsynchronizerUTest {
    Mockery context = new JUnit4Mockery() {{
        setImposteriser(ClassImposteriser.INSTANCE);
    }};

    private ProtocolAsynchronizer a;

    @Before
    public void setup() {
        a = new ProtocolAsynchronizer();
    }

    /**
     * Test proxy
     *
     * @throws Throwable repeat of whatever is thrown.
     */
    @Test
    public void fulltest() throws Throwable {
        final ServerProtocol serverprotocol = context.mock(ServerProtocol.class);
        final DataLink dataLink = context.mock(DataLink.class);
        final ByteBuffer bytes = ByteBuffer.allocate(1);

        final OnThreadHelper tot = new OnThreadHelper(a);

        context.checking(new Expectations() {{
            oneOf(serverprotocol).asyncConnected(dataLink);
            oneOf(serverprotocol).asyncData(bytes);
            oneOf(serverprotocol).asyncClosed();
            will(tot.interrupt(true));
        }});

        Protocol pp = a.createProxy(serverprotocol);

        pp.asyncConnected(dataLink);
        pp.asyncData(bytes);
        pp.asyncClosed();

        tot.start();
        
        tot.sync();
    }

    // test 
}