/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.io;

import org.junit.Assert;
import org.junit.Test;

public class DataLinkProblemUTest {
    @Test
    public void construct_logic() {
        DataLinkProblem d = new DataLinkProblem("message");

        Assert.assertNull("was not constructed with throwable", d.getThrowable());

        RuntimeException e = new RuntimeException();
        d = new DataLinkProblem(e);

        Assert.assertEquals("did not get back throwable from constructor", e, d.getThrowable());
    }
}
