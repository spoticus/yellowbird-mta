/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.brain;

import org.junit.Test;
import org.junit.Assert;

import java.io.InputStream;

/**
 * Test XMLConfiguration.
 */
public class XMLConfigurationUTest {
    @Test
    public void xsd_boundary_min() throws ConfigurationException {
        InputStream xml = XMLConfigurationUTest.class.getResourceAsStream("xmlconfigurationutest_min.xml");
        XMLConfiguration c = new XMLConfiguration(xml);

        EnvelopeBusListenerConfiguration[] eblc = c.getEnvelopeBusListenerConfigurations();
        Assert.assertEquals("wrong listener list", 0, eblc.length);

        Assert.assertEquals("message directory wrong", "/", c.getMessageDirectory());

        String[] sp = c.getSpoolerPatterns();
        Assert.assertEquals("wrong spooler regex list", 0, sp.length);

        InboundConnectionConfiguration[] icc = c.getInboundConnectionConfiguration();
        Assert.assertEquals("wrong inbound connection list", 0, icc.length);
    }

    @Test
    public void xsd_boundary_max() throws ConfigurationException {
        InputStream xml = XMLConfigurationUTest.class.getResourceAsStream("xmlconfigurationutest_max.xml");
        XMLConfiguration c = new XMLConfiguration(xml);

        EnvelopeBusListenerConfiguration[] eblc = c.getEnvelopeBusListenerConfigurations();

        Assert.assertEquals("wrong value", "com.Listener1", eblc[0].getClassname());
        Assert.assertEquals("wrong value", "listener1", eblc[0].getId());
        Assert.assertEquals("wrong value", "com.Listener2", eblc[1].getClassname());
        Assert.assertEquals("wrong value", "listener2", eblc[1].getId());

        Assert.assertEquals("message directory wrong", "/", c.getMessageDirectory());
        Assert.assertEquals("retry interval wrong", 1, c.getRetryInterval());
        Assert.assertEquals("giveup interval wrong", 2, c.getGiveupInterval());
        
        String[] sp = c.getSpoolerPatterns();
        Assert.assertEquals("wrong value", "abc", sp[0]);
        Assert.assertEquals("wrong value", "def", sp[1]);

        InboundConnectionConfiguration[] icc = c.getInboundConnectionConfiguration();

        Assert.assertEquals("wrong value", "127.0.0.1", icc[0].getHost());
        Assert.assertEquals("wrong value", 25000, icc[0].getPort());
        Assert.assertEquals("wrong value", "127.0.0.2", icc[1].getHost());
        Assert.assertEquals("wrong value", 25, icc[1].getPort());

    }

    @Test(expected=ConfigurationException.class)
    public void constructor_exception_spooler() throws ConfigurationException {
        InputStream xml = XMLConfigurationUTest.class.getResourceAsStream("xmlconfigurationutest_spooler.xml");
        XMLConfiguration c = new XMLConfiguration(xml);
    }

    @Test(expected=ConfigurationException.class)
    public void constructor_exception_ehlodomain() throws ConfigurationException {
        InputStream xml = XMLConfigurationUTest.class.getResourceAsStream("xmlconfigurationutest_ehlodomain.xml");
        XMLConfiguration c = new XMLConfiguration(xml);
    }
}
