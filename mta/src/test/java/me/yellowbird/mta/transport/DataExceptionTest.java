/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.transport;

import me.yellowbird.mta.protocol.DATACommandReply;
import me.yellowbird.mta.protocol.LineData;
import me.yellowbird.mta.protocol.LineDataException;
import org.junit.Assert;
import org.junit.Test;

/**
 * Unit Test DataException.
 */
public class DataExceptionTest {
    @Test
    public void constructor_reply() throws LineDataException {
        DATACommandReply reply = new DATACommandReply(DATACommandReply.Code.E421, new LineData(""));

        @SuppressWarnings({"ThrowableInstanceNeverThrown"}) DataException e = new DataException(reply);

        Assert.assertEquals("Wrong reply stored in exception", reply, e.getReply());
    }

    @Test
    public void constructor_reply_cause() throws LineDataException {
        DATACommandReply reply = new DATACommandReply(DATACommandReply.Code.E421, new LineData(""));
        @SuppressWarnings({"ThrowableInstanceNeverThrown"}) Throwable cause = new RuntimeException();

        @SuppressWarnings({"ThrowableInstanceNeverThrown"}) DataException e = new DataException(reply, cause);

        Assert.assertEquals("Wrong reply stored in exception", reply, e.getReply());
        Assert.assertEquals("Wrong cause stored in exception", cause, e.getCause());
    }
}
