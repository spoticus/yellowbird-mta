/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.transport;

import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.InvalidMailBoxException;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

/**
 * EnvelopeBusInboundMessageBuilder unit test.
 */
@RunWith(JMock.class)
public class EnvelopeBusInboundMessageBuilderUTest {
    Mockery context = new JUnit4Mockery() {{
        setImposteriser(ClassImposteriser.INSTANCE);
    }};

    // test when there are no transports available

    @Test
    public void buildDATA_contract_no_transports() throws DataException {
        final MessageIdentifier msgid = MessageIdentifier.createMessageIdentifier();

        final EnvelopeBus eb = context.mock(EnvelopeBus.class);

        EnvelopeBusInboundMessageBuilder builder = new EnvelopeBusInboundMessageBuilder(msgid, eb);

        context.checking(new Expectations(){{
            oneOf(eb).doDATA(with(same(msgid)));
        }});

        SinkTransport sink = builder.buildDATA();

        Assert.assertNull("should not have returned any sinks", sink);
    }

    // test when there is one transport available

    @Test
    public void buildDATA_contract_one_transport() throws DataException, InvalidMailBoxException, ForwardPathException {
        final MessageIdentifier msgid = MessageIdentifier.createMessageIdentifier();
        final AddrSpec rcpt = new AddrSpec("b@b.com");

        final EnvelopeBus eb = context.mock(EnvelopeBus.class);

        EnvelopeBusInboundMessageBuilder builder = new EnvelopeBusInboundMessageBuilder(msgid, eb);

        context.checking(new Expectations(){{
            //noinspection unchecked
            oneOf(eb).doRCPT(with(same(msgid)), with(same(rcpt)), with(aNull(ArrayList.class)));
            oneOf(eb).doDATA(with(same(msgid)));
        }});

        builder.buildRCPT(rcpt, null);

        SinkTransport sink = builder.buildDATA();

        Assert.assertNotNull("should have returned a sinks", sink);
    }

    // test multiple recipients with multiple transports

    @Test
    public void buildDATA_contract_multi_recipients_and_transports() throws DataException, InvalidMailBoxException, ForwardPathException {
        final MessageIdentifier msgid = MessageIdentifier.createMessageIdentifier();
        final AddrSpec rcpt1 = new AddrSpec("b@b.com");
        final AddrSpec rcpt2 = new AddrSpec("c@c.com");

        final EnvelopeBus eb = context.mock(EnvelopeBus.class);

        EnvelopeBusInboundMessageBuilder builder = new EnvelopeBusInboundMessageBuilder(msgid, eb);

        context.checking(new Expectations(){{
            //noinspection unchecked
            oneOf(eb).doRCPT(with(same(msgid)), with(same(rcpt1)), with(aNull(ArrayList.class)));
            //noinspection unchecked
            oneOf(eb).doRCPT(with(same(msgid)), with(same(rcpt2)), with(aNull(ArrayList.class)));
            oneOf(eb).doDATA(with(same(msgid)));
        }});

        builder.buildRCPT(rcpt1, null);
        builder.buildRCPT(rcpt2, null);

        SinkTransport sink = builder.buildDATA();

        Assert.assertTrue("did not return multioutboundtransport", sink instanceof MultiOutboundTransport);
    }

    // test multiple recipients with common transport

    @Test
    public void buildDATA_contract_multi_recipients_single_transport() throws DataException, InvalidMailBoxException, ForwardPathException {
        final MessageIdentifier msgid = MessageIdentifier.createMessageIdentifier();
        final AddrSpec rcpt1 = new AddrSpec("b@b.com");
        final AddrSpec rcpt2 = new AddrSpec("c@c.com");

        final EnvelopeBus eb = context.mock(EnvelopeBus.class);

        final SinkTransport st = context.mock(SinkTransport.class);
        
        EnvelopeBusInboundMessageBuilder builder = new EnvelopeBusInboundMessageBuilder(msgid, eb);

        context.checking(new Expectations(){{
            //noinspection unchecked
            oneOf(eb).doRCPT(with(same(msgid)), with(same(rcpt1)), with(aNull(ArrayList.class)));
            will(returnValue(st));
            //noinspection unchecked
            oneOf(eb).doRCPT(with(same(msgid)), with(same(rcpt2)), with(aNull(ArrayList.class)));
            will(returnValue(st));
            oneOf(eb).doDATA(with(same(msgid)));
        }});

        builder.buildRCPT(rcpt1, null);
        builder.buildRCPT(rcpt2, null);

        SinkTransport sink = builder.buildDATA();

        Assert.assertTrue("should not return multioutboundtransport", !(sink instanceof MultiOutboundTransport));
    }
}