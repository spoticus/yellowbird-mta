/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.transport;

import me.yellowbird.mta.protocol.LineDataException;
import me.yellowbird.mta.protocol.LineOfData;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/**
 * MultiOutboundTransport unit test.
 */
@RunWith(JMock.class)
public class MultiOutboundTransportUTest {
    Mockery context = new JUnit4Mockery() {{
        setImposteriser(ClassImposteriser.INSTANCE);
    }};

    @Test
    public void prepareForData_contract() throws MessageDataException {
        final SinkTransport s1 = context.mock(SinkTransport.class, "s1");
        final SinkTransport s2 = context.mock(SinkTransport.class, "s2");

        MultiOutboundTransport mot = createTestMOT(s1, s2);

        context.checking(new Expectations(){{
            oneOf(s1).prepareForData();
            oneOf(s2).prepareForData();
        }});

        mot.prepareForData();
    }

    @Test
    public void add_contract() throws LineDataException, UnsupportedEncodingException, MessageDataException {
        final SinkTransport s1 = context.mock(SinkTransport.class, "s1");
        final SinkTransport s2 = context.mock(SinkTransport.class, "s2");

        MultiOutboundTransport mot = createTestMOT(s1, s2);

        final LineOfData data = new LineOfData("x\r\n");

        context.checking(new Expectations(){{
            oneOf(s1).add(with(same(data)));
            oneOf(s2).add(with(same(data)));
        }});

        mot.add(data);
    }

    @Test
    public void commit_contract() throws MessageDataException {
        final SinkTransport s1 = context.mock(SinkTransport.class, "s1");
        final SinkTransport s2 = context.mock(SinkTransport.class, "s2");

        MultiOutboundTransport mot = createTestMOT(s1, s2);

        context.checking(new Expectations(){{
            oneOf(s1).commit();
            oneOf(s2).commit();
        }});

        mot.commit();
    }

    @Test
    public void cancel_contract() throws MessageDataException {
        final SinkTransport s1 = context.mock(SinkTransport.class, "s1");
        final SinkTransport s2 = context.mock(SinkTransport.class, "s2");

        MultiOutboundTransport mot = createTestMOT(s1, s2);

        context.checking(new Expectations(){{
            oneOf(s1).cancel();
            oneOf(s2).cancel();
        }});

        mot.cancel();
    }

    private static MultiOutboundTransport createTestMOT(SinkTransport... sinks) {
        return new MultiOutboundTransport(Arrays.asList(sinks));
    }
}