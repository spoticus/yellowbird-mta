/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit Test AddrSpec.
 */
public class AddrSpecUTest {
    /**
     * Constructor.
     * @throws InvalidMailBoxException an invalid address should not be detected.
     */

    @Test
    public void constructor_nulladdress() throws InvalidMailBoxException {
        AddrSpec a = new AddrSpec("");
        Assert.assertTrue("did not determine null address", a.isNullAddress());

        a = new AddrSpec("a@a.com");
        Assert.assertFalse("did not determine null address", a.isNullAddress());
    }

    /**
     * Boundary test
     * @throws InvalidMailBoxException an invalid address should not be detected.
     */
    @Test
    public void pattern_boundary() throws InvalidMailBoxException {
        AddrSpec m = new AddrSpec("c@c.com");
        Assert.assertEquals("localpart mismatch", "c", m.getLocalPart());
        Assert.assertEquals("domain mismatch", "c.com", m.getDomain());
        Assert.assertEquals("linedata mismatch", "c@c.com", m.toLineData().toString());

        AddrSpec m2 = new AddrSpec("c@c.com");
        Assert.assertTrue("equals override wrong", m.sameAs(m2));

        m2 = new AddrSpec("d@c.com");
        Assert.assertFalse("equals override wrong", m.sameAs(m2));
    }

    // getLocalPart when address is null address

    @Test
    public void getLocalPart_contract_null_address() throws InvalidMailBoxException {
        AddrSpec a = new AddrSpec("");

        Assert.assertTrue("was expecting empty string for null address", a.getLocalPart().length() == 0);
    }

    // getDomain when address is null address

    @Test
    public void getDomain_contract_null_address() throws InvalidMailBoxException {
        AddrSpec a = new AddrSpec("");

        Assert.assertTrue("was expecting empty string for null address", a.getDomain().length() == 0);
    }

    // toLineData when address is null address

    @Test
    public void toLineData_contract_null_address() throws InvalidMailBoxException {
        AddrSpec a = new AddrSpec("");

        Assert.assertTrue("was expecting empty string for null address", a.toLineData().length()==0);
    }
    // sameAs when both are null address

    @Test
    public void sameAs_contract_null_addresses() throws InvalidMailBoxException {
        AddrSpec a = new AddrSpec("");
        AddrSpec b = new AddrSpec("");

        Assert.assertTrue("two null address did not match", a.sameAs(b));
    }

    // sameAs when both are the same not-null address
    @Test
    public void sameAs_contract_not_null_and_same() throws InvalidMailBoxException {
        AddrSpec a = new AddrSpec("a@a.com");
        AddrSpec b = new AddrSpec("a@a.com");

        Assert.assertTrue("two address did not match", a.sameAs(b));
    }

    // sameAs when both are not the same not-null address

    @Test
    public void sameAs_contract_not_null_not_same() throws InvalidMailBoxException {
        AddrSpec a = new AddrSpec("a@a.com");
        AddrSpec b = new AddrSpec("b@a.com");

        Assert.assertTrue("two addresses should not have matched", !a.sameAs(b));
    }

    // sameAs when one is the null address

    @Test
    public void sameAs_contract_not_and_notnull() throws InvalidMailBoxException {
        AddrSpec a = new AddrSpec("a@a.com");
        AddrSpec b = new AddrSpec("");

        Assert.assertTrue("two addresses should not have matched", !a.sameAs(b));
        Assert.assertTrue("two addresses should not have matched", !b.sameAs(a));
    }
}