/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.sal.vlp;

import me.yellowbird.mta.mps.StorageException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Test VBFFv10.
 */
//@RunWith(JMock.class)
public class FileVLFFITest {
    // constructor with directory
    @Test
    public void constructor_directory() throws StorageException {
        FileDirectory d = createStorageDirectory();

        new FileVLFF(d);
    }

    // create variable length file
    @Test
    public void createVariableLengthFile_contract() throws StorageException {
        FileVLFF fsm = new FileVLFF(createStorageDirectory());

        VariableLengthPersistence vlf = fsm.createVariableLengthFile();

//        Assert.assertEquals("length of file should be zero", 0, vlp.getLength());

//        Assert.assertEquals("current offset of file should be zero", 0, vlp.getCurrentOffset());
    }

    // listing ready files
    @Test
    public void getIsReady_contract() throws StorageException {
        FileVLFF fsm = new FileVLFF(createStorageDirectory());

        VariableLengthPersistence a = fsm.createVariableLengthFile();
        VariableLengthPersistence b = fsm.createVariableLengthFile();

        a.markReady();

        List<VariableLengthPersistence> z = fsm.getIsReady();

        Assert.assertEquals("wrong number of files found in ready state", 1, z.size());

    }

    static private FileDirectory createStorageDirectory() throws StorageException {
        try {
            File d = File.createTempFile("mta", "test");
            if (!d.delete())
                throw new RuntimeException("could not delete temporary file");
            if (!d.mkdir())
                throw new RuntimeException("could not create temporary directory");
            d.deleteOnExit();
            
            return new FileDirectory(d);
        } catch (IOException e) {
            throw new RuntimeException("could not create temporary directory", e);
        }
    }
}
