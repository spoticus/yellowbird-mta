/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.vbm;

import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.InvalidMailBoxException;
import me.yellowbird.mta.mps.BlockType;
import me.yellowbird.mta.mps.StorageException;
import me.yellowbird.mta.mps.sal.vbp.ConstantIndex;
import me.yellowbird.mta.mps.sal.vbp.VBPTestHarness;
import me.yellowbird.mta.mps.sal.vbp.VariableBlockPersistence;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

import static me.yellowbird.mta.mps.sal.vbp.LogicalIndexMatcher.aLogicalIndexHavingIndex;

/**
 * Test VBPMessage.
 */
@RunWith(JMock.class)
public class VBPMessageUTest {
    Mockery context = new JUnit4Mockery() {{
        setImposteriser(ClassImposteriser.INSTANCE);
    }};

    // able to retrieve the reverse path

    @Test
    public void getReversePath_contract_retrieve_reversepath() throws InvalidMailBoxException, StorageException {
//        VBPTestHarness vbp = new VBPTestHarness(new AddrSpec("a@a.com"));

        final VariableBlockPersistence vbp = context.mock(VariableBlockPersistence.class);

        VBPMessage msg = new VBPMessage(vbp);

        final AddrSpec path = new AddrSpec("a@a.com");

        //TODO switch to vbptestharness
        context.checking(new Expectations() {{
            oneOf(vbp).open();
            oneOf(vbp).close();
            allowing(vbp).isOpen();
            will(returnValue(false));
            allowing(vbp).get(with(BlockType.REVERSEPATH));
            will(returnValue(path.toLineData().toByteArray()));
        }});

//        vbp.open();
        Assert.assertTrue("reverse path does not match", msg.getReversePath().sameAs(path));

        context.assertIsSatisfied();
    }

    // can retrieve undelivered forward paths

    @Test
    public void getUndeliveredForwardPaths_contract_retrieve_paths() throws InvalidMailBoxException, StorageException, UnsupportedEncodingException {
        final VariableBlockPersistence vbp = context.mock(VariableBlockPersistence.class);
        VBPMessage msg = new VBPMessage(vbp);

        final AddrSpec path = new AddrSpec("b@b.com");
        final byte[] spath = "0b@b.com".getBytes("UTF-8");
        final ConstantIndex pathindex = new ConstantIndex(BlockType.FIRSTFORWARDPATH);

        //TODO switch to vbptestharness
        context.checking(new Expectations() {{
            oneOf(vbp).open();
            oneOf(vbp).close();
            allowing(vbp).isOpen();
            will(returnValue(false));
            allowing(vbp).get(with(aLogicalIndexHavingIndex(BlockType.FORWARDPATHCOUNT)));
            will(returnValue(makeIntToByte(1)));
            allowing(vbp).get(with(aLogicalIndexHavingIndex(pathindex)));
            will(returnValue(spath));
        }});

//        vbp.open();
        AddrSpec[] paths = msg.getUndeliveredForwardPaths();

        Assert.assertEquals("wrong number of undelivered paths", 1, paths.length);
        Assert.assertTrue("forward path does not match", paths[0].sameAs(path));

        context.assertIsSatisfied();
    }

    // boundary retrieves only undelivered forward paths, skipping delivered

    @Test
    @Ignore
    public void getUndeliveredForwardPaths_boundary_only_undelivered() {

    }

    // can mark forwardpath delivered

    @Test
    public void markForwardPathSent_contract_marks_delivered() throws StorageException {
        VBPTestHarness vbp = createVBPTestHarness("reverse@a.com", "forward@forward.com", "data");

        VBPMessage msg = new VBPMessage(vbp);

        Assert.assertEquals("test data wrong", 1, msg.getUndeliveredForwardPaths().length);

        try {
            msg.markForwardPathSent(new AddrSpec("forward@forward.com"));
        } catch (InvalidMailBoxException e) {
            throw new RuntimeException(e);
        }

        Assert.assertEquals("did not mark forwardpath delivered", 0, msg.getUndeliveredForwardPaths().length);
    }

    @Test
    public void markForwardPathUndeliverable_contract_mark_undeliverable() throws StorageException {
        VBPTestHarness vbp = createVBPTestHarness("reverse@a.com", "forward@forward.com", "data");

        VBPMessage msg = new VBPMessage(vbp);

        Assert.assertEquals("test data wrong", 1, msg.getUndeliveredForwardPaths().length);

        try {
            msg.markForwardPathUndeliverable(new AddrSpec("forward@forward.com"), null);
        } catch (InvalidMailBoxException e) {
            throw new RuntimeException(e);
        }

        Assert.assertEquals("did not mark forwardpath undeliverable", 0, msg.getUndeliveredForwardPaths().length);
    }

    @Test
    public void getOriginalSpoolDate_contract() throws StorageException {
        VBPTestHarness vbp = createVBPTestHarness("reverse@a.com", "forward@forward.com", "data");

        VBPMessage msg = new VBPMessage(vbp);

        DateTime dt = msg.getOriginalSpoolDate();

        Assert.assertEquals("did not get correct date", 0, dt.getMillis());
    }

    static private VBPTestHarness createVBPTestHarness(String reversePath, String forwardPath, String data) {
        try {
            VBPTestHarness vbp = new VBPTestHarness(
                    new VBPTestHarness.Block(BlockType.FORMAT, "1.0.2"),
                    new VBPTestHarness.Block(BlockType.MSGID, "123"),
                    new VBPTestHarness.Block(BlockType.RECEIVEDATE, (long) 0),
                    new VBPTestHarness.Block(BlockType.REVERSEPATH, new AddrSpec(reversePath).toLineData().toByteArray()),
                    new VBPTestHarness.Block(BlockType.FORWARDPATHCOUNT, (int) 1),
                    new VBPTestHarness.Block(BlockType.FIRSTFORWARDPATH, makeForwardPathBytes(forwardPath))
            );

            return vbp;
        } catch (InvalidMailBoxException e) {
            throw new RuntimeException(e);
        }
    }

    /*
     * Forward path in bytes with "not forwarded" prefix flag.
     */

    private static byte[] makeForwardPathBytes(String forwardPath) {
        try {
            byte[] b = new AddrSpec(forwardPath).toLineData().toByteArray();

            byte[] c = new byte[b.length + 1];

            c[0] = '0';

            System.arraycopy(b, 0, c, 1, b.length);

            return c;
        } catch (InvalidMailBoxException e) {
            throw new RuntimeException(e);
        }
    }

    private byte[] makeIntToByte(int i) {
        return ByteBuffer.allocate(4).putInt(i).array();
    }
}
