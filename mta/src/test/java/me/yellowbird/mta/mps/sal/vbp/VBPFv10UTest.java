/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.sal.vbp;

import me.yellowbird.mta.mps.StorageException;
import me.yellowbird.mta.mps.sal.vlp.VariableLengthPersistenceFactory;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Unit test VBFFv10.
 */
@RunWith(JMock.class)
public class VBPFv10UTest {
    Mockery context = new JUnit4Mockery() {{
//        setImposteriser(ClassImposteriser.INSTANCE);
    }};

    // create returns vbp
    @Test
    public void createVariableBlockFile_contract_success() throws StorageException {
        final VariableLengthPersistenceFactory vlff = context.mock(VariableLengthPersistenceFactory.class);

        VBFFv10 vbff = new VBFFv10(vlff);

        context.checking(new Expectations(){{
            oneOf(vlff).createVariableLengthFile();
        }});

        vbff.createVariableBlockPersistence();

        context.assertIsSatisfied();
    }
}
