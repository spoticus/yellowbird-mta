/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.sal.vbp;

import me.yellowbird.mta.mps.StorageException;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Unit test for VBFv10.
 */
@RunWith(JMock.class)
public class VBPv10UTest {
    Mockery context = new JUnit4Mockery() {{
//        setImposteriser(ClassImposteriser.INSTANCE);
    }};


    // open a new file
    public void open_contract_new_file() {

    }

    // open an existing file
    public void open_contract_existing_file() {

    }
    // open a file with invalid structure
    public void open_exception_FileStructureException() {

    }
    
    // set value to new block
    @Test
    public void set_contract_new_block() throws StorageException, IOException, StructureException {
        VLFTestHarness vlf = new VLFTestHarness();

        VBFv10 vbf = new VBFv10(vlf);

        Block b0 = new Block(new ConstantIndex(0), VBFv10.makeBytes("abc"));

        vbf.open();

        vbf.set(b0.getBlockIndex(), b0.getValue());

        vbf.close();

        vlf.verify(b0);
    }


    // set value to existing block
    @Test
    public void set_contract_replace_value_in_block() throws StorageException, StructureException {
        VLFTestHarness vlf = new VLFTestHarness();

        VBFv10 vbf = new VBFv10(vlf);

        Block b0 = new Block(new ConstantIndex(0), VBFv10.makeBytes("abc"));

        vbf.open();

        vbf.set(b0.getBlockIndex(), b0.getValue());

        Block b1 = new Block(new ConstantIndex(0), VBFv10.makeBytes("xyz"));

        vbf.set(b1.getBlockIndex(), b1.getValue());

        vbf.close();

        vlf.verify(b1);
    }

    // set a value in existing block so content length becomes shorter than block length.
    @Test
    public void set_boundary_replace_with_shorter_value() throws StorageException, StructureException {
        VLFTestHarness vlf = new VLFTestHarness();

        VBFv10 vbf = new VBFv10(vlf);

        Block b0 = new Block(new ConstantIndex(0), VBFv10.makeBytes("abc"));

        vbf.open();

        vbf.set(b0.getBlockIndex(), b0.getValue());

        Block b1 = new Block(b0.getBlockLength(), new ConstantIndex(0), VBFv10.makeBytes("yz"));

        vbf.set(b1.getBlockIndex(), b1.getValue());

        vbf.close();

        vlf.verify(b1);

    }

    // set a value in existing block so that new block is allocated.
    @Test
    public void set_boundary_replace_with_longer_value() throws StorageException, StructureException {
        VLFTestHarness vlf = new VLFTestHarness();

        VBFv10 vbf = new VBFv10(vlf);

        Block b0 = new Block(new ConstantIndex(0), VBFv10.makeBytes("abc"));

        vbf.open();

        vbf.set(b0.getBlockIndex(), b0.getValue());

        Block b1 = new Block(new ConstantIndex(0), VBFv10.makeBytes("wxyz"));

        vbf.set(b1.getBlockIndex(), b1.getValue());
        b0.setBlockIndex(LogicalIndex.PreDefined.UNUSED);

        vbf.close();

        vlf.verify(b0, b1);
    }

    // get content of variable block file
    @Test
    public void get_contract_variable_block_exists() throws StorageException, StructureException, ContentTooLargeException {
        VLFTestHarness vlf = new VLFTestHarness();

        VBFv10 vbf = new VBFv10(vlf);

        Block b0 = new Block(new ConstantIndex(0), VBFv10.makeBytes("abc"));

        vbf.open();

        vbf.set(b0.getBlockIndex(), b0.getValue());

        vbf.close();

        vlf.verify(b0);

        byte[] b = vbf.get(new ConstantIndex(0));

        Assert.assertArrayEquals("bytes from get do not match bytes from put", b0.getValue(), b);

    }

    // get when block does not exists
    @Test(expected = IndexOutOfBoundsException.class)
    public void get_exception_block_does_not_exists() throws StructureException, StorageException, ContentTooLargeException {
        VLFTestHarness vlf = new VLFTestHarness();

        VBFv10 vbf = new VBFv10(vlf);

        Block b0 = new Block(new ConstantIndex(0), VBFv10.makeBytes("abc"));

        vbf.open();

        vbf.set(b0.getBlockIndex(), b0.getValue());

        vbf.close();

        vlf.verify(b0);

        byte[] b = vbf.get(new ConstantIndex(1));

    }

    @Test
    public void getByteBuffer_contract() throws StorageException {
        VLFTestHarness vlf = new VLFTestHarness();

        VBFv10 vbf = new VBFv10(vlf);

        Block b0 = new Block(new ConstantIndex(0), VBFv10.makeBytes("abc"));

        vbf.open();

        vbf.set(b0.getBlockIndex(), b0.getValue());

        vbf.close();

        vlf.verify(b0);

        // test

        ByteBuffer bb = vbf.getByteBuffer(new ConstantIndex(0));

        Assert.assertTrue("content read does not match file", bb.compareTo(ByteBuffer.wrap(VBFv10.makeBytes("abc"))) == 0);

    }

    // append creates new block

    @Test
    public void append_contract_new_block() throws StorageException, IOException, StructureException {
        VLFTestHarness vlf = new VLFTestHarness();

        VBFv10 vbf = new VBFv10(vlf);

        Block b0 = new Block(new ConstantIndex(0), VBFv10.makeBytes("abc"));

        vbf.open();

        vbf.append(b0.getBlockIndex(), b0.getValue());

        vbf.close();

        vlf.verify(b0);
    }

    // append adds to existing block with sufficient space to contain new data with reallocate

    @Test
    public void append_contract_block_big_enough() throws StorageException, IOException, StructureException {
        VLFTestHarness vlf = new VLFTestHarness();

        VBFv10 vbf = new VBFv10(vlf);

        Block b0m = new Block(new ConstantIndex(0), VBFv10.makeBytes("abcxyz"));

        vbf.open();

        vbf.set(b0m.getBlockIndex(), b0m.getValue());   //make it big

        vbf.set(b0m.getBlockIndex(), VBFv10.makeBytes("abc"));   //contract size

        vbf.append(b0m.getBlockIndex(), VBFv10.makeBytes("xyz"));

        vbf.close();

        vlf.verify(b0m);
    }

    // append adds to existing block that has to be expanded and is a block in the middle of the vlf

    @Test
    public void append_contract_block_toosmall_in_middle() throws StorageException, IOException, StructureException {
        VLFTestHarness vlf = new VLFTestHarness();

        VBFv10 vbf = new VBFv10(vlf);

        Block b0a = new Block(new ConstantIndex(0), VBFv10.makeBytes("abc"));
        Block b0b = new Block(new ConstantIndex(0), VBFv10.makeBytes("abcmno"));
        Block b1 = new Block(new ConstantIndex(1), VBFv10.makeBytes("123456"));

        vbf.open();

        vbf.set(b0a.getBlockIndex(), b0a.getValue());   //first block

        vbf.set(b1.getBlockIndex(), b1.getValue());   //at end of vlb

        vbf.append(b0b.getBlockIndex(), VBFv10.makeBytes("mno"));    //should look like b0b now
        b0a.setBlockIndex(LogicalIndex.PreDefined.UNUSED); //should be unused now

        vbf.close();

        vlf.verify(b0a, b1, b0b);
    }

    // append adds to existing block that is expanded and is the last block in the vlf

    @Test
    public void append_contract_block_toosmall_at_end() throws StorageException, IOException, StructureException {
        VLFTestHarness vlf = new VLFTestHarness();

        VBFv10 vbf = new VBFv10(vlf);

        Block b0 = new Block(new ConstantIndex(0), VBFv10.makeBytes("abc"));
        Block b1a = new Block(new ConstantIndex(1), VBFv10.makeBytes("123"));
        Block b1b = new Block(new ConstantIndex(1), VBFv10.makeBytes("123456"));

        vbf.open();

        vbf.set(b0.getBlockIndex(), b0.getValue());   //first block

        vbf.set(b1a.getBlockIndex(), b1a.getValue());   //at end of vlb

        vbf.append(b1b.getBlockIndex(), VBFv10.makeBytes("456"));    //should look like b0b now
//        b0a.setBlockIndex(LogicalIndex.PreDefined.UNUSED); //should be unused now

        vbf.close();

        vlf.verify(b0, b1b);
    }
    
    static class Block {
        private long blockLength;
        private LogicalIndex logicalIndex;
        private byte[] value;

        public Block(LogicalIndex logicalIndex, byte[] value) {
            blockLength= 20 + (value==null?0:value.length);
            this.logicalIndex = logicalIndex;
            this.value = value;
        }

        public Block(long blockLength, LogicalIndex logicalIndex, byte[] value) {
            this.blockLength = blockLength;
            this.logicalIndex = logicalIndex;
            this.value = value;
        }

        public Block(ConstantIndex logicalIndex, byte[] value, int extraCapacity) {
            this.blockLength = 20 + (value==null?0:value.length) + extraCapacity;
            this.logicalIndex = logicalIndex;
            this.value = value;
        }

        public LogicalIndex getBlockIndex() {
            return logicalIndex;
        }

        public byte[] getValue() {
            return value;
        }

        public long getBlockLength() {
            return blockLength;
        }

        public long getContentLength() {
            return value==null?0:value.length;
        }

        public void setBlockIndex(LogicalIndex logicalIndex) {
            this.logicalIndex = logicalIndex;
        }

    }

//    static class BI implements LogicalIndex {
//        private int index;
//
//        public BI(int index) {
//            this.index = index;
//        }
//
//        @Override
//        public int getIndex() {
//            return index;
//        }
//    }

}
