/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.sal.vbp;

import me.yellowbird.mta.mps.StorageException;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Memory-based test harness for dependencies on VBP.
 */
public class VBPTestHarness implements VariableBlockPersistence {
    private boolean isOpen = false;
    private Map<Integer, byte[]> blocks = new HashMap<Integer, byte[]>();
    private boolean isDeleted = false;
    private boolean isReady = false;

    /**
     * Create empty VBP.
     */
    public VBPTestHarness() {

    }

    /**
     * Create VBP with specific blocks.
     *
     * @param blocks blocks to add to VBP using put.
     */
    public VBPTestHarness(Block... blocks) {
        for (Block bl : blocks) {
            this.blocks.put(bl.getLogicalIndex().getIndex(), Arrays.copyOf(bl.getContent(), bl.getContent().length));
        }
    }

    @Override
    public void open() throws StorageException, StructureException {
        if (isOpen)
            throw new StorageException("already open");

        this.isOpen = true;
    }

    @Override
    public void close() throws StorageException {
        assertIsOpen();

        this.isOpen = false;
    }

    private void assertIsOpen() throws StorageException {
        if (!isOpen)
            throw new StorageException("not open");
    }

    @Override
    public void set(LogicalIndex index, byte[] value) throws StorageException {
        assertIsOpen();

        blocks.put(index.getIndex(), Arrays.copyOf(value, value.length));
    }

    @Override
    public byte[] get(LogicalIndex index) throws IndexOutOfBoundsException, ContentTooLargeException, StorageException, StructureException {
        assertIsOpen();

        if (!blocks.containsKey(index.getIndex()))
            throw new IndexOutOfBoundsException();

        return blocks.get(index.getIndex());
    }

    @Override
    public void delete() throws StorageException {
        isDeleted = true;
    }

    @Override
    public void markReady() throws StorageException {
        isReady = true;
    }

    @Override
    public void append(LogicalIndex logicalIndex, byte[] value) throws StorageException {
        assertIsOpen();

        if (blocks.containsKey(logicalIndex.getIndex())) {
            byte[] b = blocks.get(logicalIndex.getIndex());

            byte[] n = Arrays.copyOf(b, b.length + value.length);

            System.arraycopy(value, 0, n, b.length, value.length);

            blocks.put(logicalIndex.getIndex(), n);
        } else {
            set(logicalIndex, value);
        }
    }

    @Override
    public ByteBuffer getByteBuffer(LogicalIndex logicalIndex) throws IndexOutOfBoundsException, StorageException, ContentTooLargeException, StructureException {
        assertIsOpen();

        if (!blocks.containsKey(logicalIndex.getIndex()))
            throw new IndexOutOfBoundsException();

        return ByteBuffer.wrap(blocks.get(logicalIndex.getIndex()));
    }

    public boolean contains(int index, String value) {
        if (!blocks.containsKey(index))
            return false;

        byte[] b = VBFv10.makeBytes(value);

        return Arrays.equals(blocks.get(index), b);
    }

    public boolean contains(int index, byte[] value) {
        if (!blocks.containsKey(index))
            return false;

        return Arrays.equals(blocks.get(index), value);
    }

    public boolean contains(int index, Pattern pattern) {
        if (!blocks.containsKey(index))
            return false;

        Charset utf8 = Charset.forName("UTF-8");

        CharBuffer s = utf8.decode(ByteBuffer.wrap(blocks.get(index)));

        return pattern.matcher( s.toString() ).matches();
    }

    public boolean contains(int index, Class<Long> longClass) {
        if (!blocks.containsKey(index))
            return false;

        return blocks.get(index).length == 8;
    }

    public boolean contains(int index, Integer integer) {
        if (!blocks.containsKey(index))
            return false;

        if (blocks.get(index).length != 4)
            return false;

        int v = ByteBuffer.wrap(blocks.get(index)).getInt();

        return v == integer;

    }

    public boolean contains(int index, ByteBuffer byteBuffer) {
        if (!blocks.containsKey(index))
            return false;

        return byteBuffer.compareTo(ByteBuffer.wrap(blocks.get(index))) == 0;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public boolean isReady() {
        return isReady;
    }

    public static class Block {
        private LogicalIndex logicalIndex;
        private byte[] bytes;

        public Block(LogicalIndex logicalIndex, String value) {
            this.logicalIndex = logicalIndex;

            try {
                this.bytes = value.getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
        }

        public Block(LogicalIndex logicalIndex, Long value) {
            this.logicalIndex = logicalIndex;

            this.bytes = ByteBuffer.allocate(8).putLong(value).array();
        }

        public Block(LogicalIndex logicalIndex, byte[] bytes) {
            this.logicalIndex = logicalIndex;

            this.bytes = bytes;
        }

        public Block(LogicalIndex logicalIndex, int value) {
            this.logicalIndex = logicalIndex;

            this.bytes = ByteBuffer.allocate(4).putInt(value).array();
        }

        public LogicalIndex getLogicalIndex() {
            return logicalIndex;
        }

        public byte[] getContent() {
            return bytes;
        }
    }
}
