/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.sal.vlp;

import me.yellowbird.mta.mps.StorageException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Arrays;

/**
 * Test VBFFv10.
 */
//@RunWith(JMock.class)
public class FileVLFITest {
    // constructor with non-existing file

    @Test
    public void constructor_file() throws StorageException {
        File f = createNonExistingFile();

        new FileVLF(f);

        Assert.assertTrue("file was created when should not have been", !f.exists());
    }

    // can open the file

    @Test
    public void open_contract_new_file() throws StorageException {
        File f = createNonExistingFile();

        FileVLF vlf = new FileVLF(f);

        vlf.open();

        vlf.close();

        Assert.assertEquals("new file was not zero length after open", 0, f.length());
    }

    // can close the file

    @Test
    public void close_contract() throws StorageException {
        FileVLF vlf = new FileVLF(createNonExistingFile());

        vlf.open();

        vlf.close();
    }

    // put starts at specified offset and all bytes written

    @Test
    public void put_contract_at_offset_all_bytes() throws StorageException {
        File f = createNonExistingFile();

        FileVLF vlf = new FileVLF(f);

        vlf.open();

        vlf.put(0, makeBytes("abc"));

        vlf.close();

        verify(f, 0, makeBytes("abc"));
    }

    // put replaces existing content

    @Test
    public void put_contract_replace_at_offset() throws StorageException {
        File f = createNonExistingFile();

        FileVLF vlf = new FileVLF(f);

        // build test content
        vlf.open();

        vlf.put(0, makeBytes("abc"));

        vlf.close();

        verify(f, 0, makeBytes("abc"));

        // replace content
        vlf.open();

        vlf.put(1, makeBytes("z"));

        vlf.close();

        verify(f, 0, makeBytes("azc"));
    }

    // put extends length of file

    @Test
    public void put_contract_extends_file() throws StorageException {
        File f = createNonExistingFile();

        FileVLF vlf = new FileVLF(f);

        // build test content
        vlf.open();

        vlf.put(0, makeBytes("abc"));

        vlf.close();

        Assert.assertEquals("file length with test data wrong", 3, f.length());

        // extend file
        vlf.open();

        vlf.put(3, makeBytes("z"));

        vlf.close();

        Assert.assertEquals("file length not extended", 4, f.length());
    }

    // offset must be a whole number

    @Test(expected = IllegalArgumentException.class)
    public void put_contract_offset_whole_number() throws StorageException {
        File f = createNonExistingFile();

        FileVLF vlf = new FileVLF(f);

        vlf.open();

        vlf.put(-1, makeBytes("abc"));//bad offset

        Assert.fail("should not have accepted negative offset");
    }

    // boundary case of zero length byte array

    @Test
    public void put_boundary_zero_length_data() throws StorageException {
        File f = createNonExistingFile();

        FileVLF vlf = new FileVLF(f);

        // build test content
        vlf.open();

        vlf.put(0, makeBytes("abc"));

        vlf.close();

        verify(f, 0, makeBytes("abc"));

        // write zero length data

        vlf.open();

        vlf.put(1, makeBytes(""));

        vlf.close();

        verify(f, 0, makeBytes("abc"));
    }

    @Test
    // fills byte array with file content
    public void get_contract_fills_byte_array() throws StorageException, UnderflowException {
        byte[] original = makeBytes("abc");

        File f = createExistingFile(original);

        FileVLF vlf = new FileVLF(f);

        vlf.open();

        byte[] b = new byte[3];

        vlf.get(0, b);

        vlf.close();

        Assert.assertTrue("content read does not match file", Arrays.equals(b, original));
    }

    // underflow while filling byte array causes error

    @Test
    public void get_contract_underflow_causes_error() throws StorageException {
        byte[] original = makeBytes("abc");

        File f = createExistingFile(original);

        FileVLF vlf = new FileVLF(f);

        try {
            vlf.open();

            byte[] b = new byte[4];

            try {
                vlf.get(0, b);
            } catch (UnderflowException e) {
                return;
            }

            Assert.fail("did not detect underflow");
        } finally {
            vlf.close();
        }

    }

    // empty file detected

    @Test
    public void isEmpty_contract_empty_file() throws StorageException {
        File f = createNonExistingFile();

        FileVLF vlf = new FileVLF(f);

        Assert.assertTrue("file should have been empty", vlf.isEmpty());
    }

    // not empty file detected

    @Test
    public void isEmpty_contract_not_empty_file() throws StorageException {
        byte[] original = makeBytes("abc");

        File f = createExistingFile(original);

        FileVLF vlf = new FileVLF(f);

        Assert.assertTrue("file should not have been empty", !vlf.isEmpty());
    }

    // copy bytes

    @Test
    public void copy_contract() throws StorageException, UnderflowException {
        File f = createNonExistingFile();

        FileVLF vlf = new FileVLF(f);

        // build test content
        vlf.open();

        vlf.put(0, makeBytes("abc"));

        vlf.copy(0, 3, 3);

        vlf.close();

        verify(f, 0, makeBytes("abcabc"));
    }

    // underflow reported when copying too many bytes

    @Test(expected = UnderflowException.class)
    public void copy_exception_UnderflowException() throws StorageException, UnderflowException {
        File f = createNonExistingFile();

        FileVLF vlf = new FileVLF(f);

        // build test content
        vlf.open();

        vlf.put(0, makeBytes("abc"));

        try {
            vlf.copy(0, 3, 4);
        } finally {
            vlf.close();
        }
    }

    // copy bytes testing internal implementation limits

    @Test
    public void copy_boundary_buffer_limits() throws StorageException, UnderflowException {
        File f = createNonExistingFile();

        FileVLF vlf = new FileVLF(f);

        // build test content
        vlf.open();

        byte[] manybytes = makeBytes("x", 5000);

        vlf.put(0, manybytes);

        vlf.copy(0, 5000, 5000);

        vlf.close();

        verify(f, 0, makeBytes(manybytes, 2));

    }

    // copy where it does not erase unaffected areas before and after the target area

    @Test
    public void copy_logic_does_not_erase_adjacent_areas() throws StorageException, UnderflowException {
        File f = createNonExistingFile();

        FileVLF vlf = new FileVLF(f);

        // build test content
        vlf.open();

        vlf.put(0, makeBytes("abcdef"));

        vlf.copy(1, 3, 2);

        vlf.close();

        verify(f, 0, makeBytes("abcbcf"));
    }

    // bytebuffer is valid

    @Test
    public void getByteBuffer_contract() throws StorageException, UnderflowException {
        byte[] original = makeBytes("abc");

        File f = createExistingFile(original);

        FileVLF vlf = new FileVLF(f);

        vlf.open();

        ByteBuffer bb = vlf.getByteBuffer(0, 3);

        Assert.assertTrue("content read does not match file", bb.compareTo(ByteBuffer.wrap(original)) == 0);

        vlf.close();
    }

    @Test
    public void isOpen_contract() throws StorageException {
        FileVLF vlf = new FileVLF(createNonExistingFile());

        vlf.open();

        try {
            Assert.assertTrue("should have been open", vlf.isOpen());
        } finally {
            vlf.close();
        }

        Assert.assertTrue("should have been closed", !vlf.isOpen());
    }
    /*
    * Create new file containing exact bytes starting at offset 0.
    */

    static private File createExistingFile(byte[] bytes) throws StorageException {
        File f = createNonExistingFile();

        FileVLF vlf = new FileVLF(f);

        vlf.open();

        vlf.put(0, bytes);

        vlf.close();

        verify(f, 0, makeBytes("abc"));

        return f;
    }


    static private File createNonExistingFile() throws StorageException {
        try {
            File d = File.createTempFile("mta", "test");
            if (!d.delete())
                throw new RuntimeException("could not delete temporary file");
            d.deleteOnExit();

            return d;
        } catch (IOException e) {
            throw new RuntimeException("could not create temporary file", e);
        }
    }

    static private byte[] makeBytes(String s) {
        try {
            return s.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    static private byte[] makeBytes(String s, int repeat) {
        try {
            byte[] b1 = s.getBytes("UTF-8");

            byte[] b2 = new byte[b1.length * repeat];

            for (int i = 0; i < repeat; ++i) {
                System.arraycopy(b1, 0, b2, i * b1.length, b1.length);
            }

            return b2;
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    static private byte[] makeBytes(byte[] b1, int repeat) {
        byte[] b2 = new byte[b1.length * repeat];

        for (int i = 0; i < repeat; ++i) {
            System.arraycopy(b1, 0, b2, i * b1.length, b1.length);
        }

        return b2;
    }

    // verify that the content of the file at the offset matches exactly the byte array

    static private void verify(File file, long offset, byte[] bytes) {
        Assert.assertTrue("length too short", offset + bytes.length <= file.length());

        try {
            RandomAccessFile raf = new RandomAccessFile(file, "r");

            FileChannel channel = raf.getChannel();

            ByteBuffer buffer = ByteBuffer.allocate(bytes.length);

            int count = channel.read(buffer, offset);
            buffer.flip();

            Assert.assertEquals("underflow when reading data", bytes.length, count);

            Assert.assertTrue("data not same in file", buffer.compareTo(ByteBuffer.wrap(bytes)) == 0);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}