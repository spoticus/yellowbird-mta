/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.sal.vbp;

import me.yellowbird.mta.mps.StorageException;
import me.yellowbird.mta.mps.sal.vlp.UnderflowException;
import me.yellowbird.mta.mps.sal.vlp.VariableLengthPersistence;
import org.junit.Assert;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * In-memory implementation of VariableLengthPersistence for VariableBlockPersistence testing.
 */
class VLFTestHarness implements VariableLengthPersistence {
    private boolean opened = false;
    private byte[] content = new byte[0];
    private boolean isDelete = false;
    private boolean isReady = false;

    @Override
    public void open() throws StorageException {
        if (opened)
            throw new RuntimeException("VLF harness already open");

        opened = true;
    }

    @Override
    public void close() throws StorageException {
        if (!opened)
            throw new RuntimeException("VLF harness not open");

        opened = false;
    }

    @Override
    public void put(long offset, byte[] bytes) throws IllegalArgumentException, StorageException {
        if (content.length < offset + bytes.length) {
            content = Arrays.copyOf(content, (int)offset + bytes.length);
        }

        System.arraycopy(bytes, 0, content, (int)offset, bytes.length);
    }

    @Override
    public void get(long offset, byte[] bytes) throws StorageException, UnderflowException {
        if (content.length < offset + bytes.length)
            throw new UnderflowException();

        System.arraycopy(content, (int)offset, bytes, 0, bytes.length);
    }

    @Override
    public boolean isEmpty() throws StorageException {
        return content.length == 0;
    }

    @Override
    public void delete() throws StorageException {
        isDelete = true;
    }

    @Override
    public void markReady() throws StorageException {
        isReady = true;
    }

    @Override
    public void copy(long src, long dst, long length) throws StorageException {
        if (content.length < dst + length)
            content = Arrays.copyOf(content, (int) (dst + length));

        System.arraycopy(content, (int)src, content, (int)dst, (int) length);
    }

    @Override
    public ByteBuffer getByteBuffer(long offset, long length) throws StorageException {
        return ByteBuffer.wrap(content, (int)offset, (int)length);
    }

    @Override
    public boolean isOpen() {
        return opened;
    }

    /**
     * Verify that this VLF contains a VBF.
     *
     * @param blocks Blocks that should exists in the VLF.
     */
    public void verify(VBPv10UTest.Block... blocks) {
        Assert.assertTrue("header incorrect", contentEquals(0, VBFv10.makeBytes("mtavbf0100      ")) );

        int currentblockoffset = 16;

        for (VBPv10UTest.Block b : blocks) {
            ByteBuffer bb = ByteBuffer.wrap(content, currentblockoffset, 20);

            long blocklength = bb.getLong();
            long logicalindex = bb.getInt();
            long contentlength = bb.getLong();

            Assert.assertEquals("block length incorrect for block " + b.getBlockIndex().getIndex(), b.getBlockLength(), blocklength);
            Assert.assertEquals("block logical index incorrect for block " + b.getBlockIndex().getIndex(), b.getBlockIndex().getIndex(), logicalindex);
            Assert.assertEquals("block content length incorrect for block " + b.getBlockIndex().getIndex(), b.getContentLength(), contentlength);
            Assert.assertArrayEquals("block content incorrect for block " + b.getBlockIndex().getIndex(), b.getValue(), Arrays.copyOfRange(content, currentblockoffset+20, (int)(currentblockoffset+20+contentlength)));

            currentblockoffset += blocklength;
        }

        Assert.assertEquals("size of file does not match lengths of blocks", currentblockoffset, content.length);
    }

    /*
     * Compare content at offset with bytes.
     */
    private boolean contentEquals(int offset, byte[] bytes) {
        if (content.length < offset + bytes.length)
            return false;

        for (int i = 0; i < bytes.length; ++i)
            if (content[offset+i] != bytes[i])
                return false;

        return true;
    }
}
