/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps;

import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Unit Test TimedDelayedMessage.
 */
@RunWith(JMock.class)
public class TimedDelayedMessageUTest {
    Mockery context = new JUnit4Mockery() {{
        setImposteriser(ClassImposteriser.INSTANCE);
    }};

    @Test
    public void logic_returnsmessage() throws IOException, FileFormatException {
        SpooledMessage f = context.mock(SpooledMessage.class);

        TimedDelayedMessage d = new TimedDelayedMessage(f);

        Assert.assertEquals("file not returned", f, d.getMessage());

        d = new TimedDelayedMessage(f, 0, TimeUnit.SECONDS);

        Assert.assertEquals("file not returned", f, d.getMessage());
    }

    @Test
    public void logic_getdelay() throws IOException, InterruptedException, FileFormatException {
        SpooledMessage f = context.mock(SpooledMessage.class);

        TimedDelayedMessage d = new TimedDelayedMessage(f);

        TimeUnit.SECONDS.sleep(1);

        Assert.assertTrue("default not set to zero", d.getDelay(TimeUnit.SECONDS) < 0);

        d = new TimedDelayedMessage(f, 0, TimeUnit.SECONDS);

        TimeUnit.SECONDS.sleep(1);

        Assert.assertTrue("delay not saved properly", d.getDelay(TimeUnit.SECONDS) < 0);
    }

    @Test
    public void logic_compare() throws IOException, InterruptedException, FileFormatException {
        SpooledMessage f = context.mock(SpooledMessage.class);

        TimedDelayedMessage before = new TimedDelayedMessage(f);

        TimeUnit.SECONDS.sleep(1);

        TimedDelayedMessage after = new TimedDelayedMessage(f);

        Assert.assertTrue("calculating wrong way", before.compareTo(after) < 0);
        Assert.assertTrue("calculating wrong way", after.compareTo(before) > 0);
    }
}
