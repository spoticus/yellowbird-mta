/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.sal.vlp;

import me.yellowbird.mta.mps.StorageException;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;

/**
 * Test FileDirectory.
 */
@RunWith(JMock.class)
public class FileDirectoryITest {
    Mockery context = new JUnit4Mockery() {{
        setImposteriser(ClassImposteriser.INSTANCE);
    }};

    // constructor with valid pathname
    @Test
    public void constructor_pathname_valid() throws StorageException {
        File d = createDirectory();

        new FileDirectory(d.getAbsolutePath());

        //noinspection ResultOfMethodCallIgnored
        d.delete();
    }

    // constructor with unwritable pathname
    @Test(expected=StorageException.class)
    @Ignore //File.setWritable not working properly on linux ubuntu 8.04
    public void constructor_pathname_notwritable() throws StorageException {
        File d = createDirectory();

        if (!d.setWritable(false, false))
            throw new RuntimeException("could not setup a non-writable directory for testing");

        d.deleteOnExit();
        
        new FileDirectory(d.getAbsolutePath());
    }

    // constructor with valid directory
    @Test
    public void constructor_directory_valid() throws StorageException {
        File d = createDirectory();

        new FileDirectory(d.getAbsolutePath());

        //noinspection ResultOfMethodCallIgnored
        d.delete();
    }

    // constructor with unwritable directory
    @Test(expected=StorageException.class)
    @Ignore //File.setWritable not working properly on linux ubuntu 8.04
    public void constructor_directory_notwritable() throws StorageException {
        File d = createDirectory();

        if (!d.setWritable(false, false))
            throw new RuntimeException("could not setup a non-writable directory for testing");

        d.deleteOnExit();

        new FileDirectory(d.getAbsolutePath());

    }

    static private File createDirectory() {
        try {
            File d = File.createTempFile("mta", "test");
            if (!d.delete())
                throw new RuntimeException("could not delete temporary file");
            if (!d.mkdir())
                throw new RuntimeException("could not create temporary directory");
            return d;
        } catch (IOException e) {
            throw new RuntimeException("could not create temporary directory", e);
        }
    }
}
