/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.sal.vbp;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

/**
 * JMock matcher for LogicalIndex.
 */
public class LogicalIndexMatcher extends TypeSafeMatcher<LogicalIndex> {
    @Factory
    public static Matcher<LogicalIndex> aLogicalIndexHavingIndex( LogicalIndex logicalIndex ) {
        return new LogicalIndexMatcher(logicalIndex);
    }

    private LogicalIndex expectation;

    public LogicalIndexMatcher(LogicalIndex expectation) {
        this.expectation = expectation;
    }

    public boolean matchesSafely(LogicalIndex compareTo) {
        return compareTo.getIndex() == expectation.getIndex();
    }

//    public StringBuffer describeTo(Description description) {
//        return description.appendText("a LogicalIndex having index value of ").appendValue(expectation.getIndex());
//    }


    @Override
    public void describeTo(Description description) {
        description.appendText("a LogicalIndex having index value of ").appendValue(expectation.getIndex());
    }
}
