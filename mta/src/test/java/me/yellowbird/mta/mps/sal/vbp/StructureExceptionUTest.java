/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.sal.vbp;

import org.junit.Assert;
import org.junit.Test;

/**
 * StructureException unit test.
 */
public class StructureExceptionUTest {
    @Test(expected = StructureException.class)
    public void constructor_message() {
        StructureException e = new StructureException("message");

        Assert.assertEquals("wrong reply saved", "message", e.getMessage());

        throw e;
    }

    @Test(expected = StructureException.class)
    public void constructor_message_cause() {
        @SuppressWarnings({"ThrowableInstanceNeverThrown"}) RuntimeException cause = new RuntimeException();

        StructureException e = new StructureException("message", cause);

        Assert.assertEquals("wrong reply saved", "message", e.getMessage());
        Assert.assertEquals("wrong cause saved", cause, e.getCause());

        throw e;
    }
}