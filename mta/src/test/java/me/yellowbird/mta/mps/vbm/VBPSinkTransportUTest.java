/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.mps.vbm;

import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.InvalidMailBoxException;
import me.yellowbird.mta.mps.PersistedMessageQueue;
import me.yellowbird.mta.mps.StorageException;
import me.yellowbird.mta.mps.TimedDelayedMessage;
import me.yellowbird.mta.mps.sal.vbp.VBPTestHarness;
import me.yellowbird.mta.mps.sal.vbp.VariableBlockPersistenceFactory;
import me.yellowbird.mta.protocol.LineDataException;
import me.yellowbird.mta.protocol.LineOfData;
import me.yellowbird.mta.transport.MessageDataException;
import me.yellowbird.mta.transport.MessageIdentifier;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.UnsupportedEncodingException;

/**
 * Unit Test VBPSinkTransport.
 */
@RunWith(JMock.class)
public class VBPSinkTransportUTest {
    Mockery context = new JUnit4Mockery() {{
        setImposteriser(ClassImposteriser.INSTANCE);
    }};

    PersistedMessageQueue queue = context.mock(PersistedMessageQueue.class);
    VariableBlockPersistenceFactory storage = context.mock(VariableBlockPersistenceFactory.class);
    MessageIdentifier msgid = MessageIdentifier.createMessageIdentifier();
    AddrSpec reversepath;

    public VBPSinkTransportUTest() throws InvalidMailBoxException {
        reversepath = new AddrSpec("a@a.com");
    }

    // end-to-end test of class verifying that it creates a message

    @Test
    public void class_contract_creates_message() throws InvalidMailBoxException, StorageException, LineDataException, UnsupportedEncodingException, MessageDataException {
        final VBPTestHarness vbp = new VBPTestHarness();

        context.checking(new Expectations(){{
            oneOf(storage).createVariableBlockPersistence();
            will(returnValue(vbp));
            oneOf(queue).relay(with(any(TimedDelayedMessage.class)));
        }});

        VBPSinkTransport t = new VBPSinkTransport(queue, storage, msgid, reversepath);

        t.addRecipient(new AddrSpec("b@b.com"));

        t.prepareForData();

        LineOfData data = new LineOfData("data\r\n");
        t.add(data);

        t.commit();

        context.assertIsSatisfied();

        Assert.assertTrue("vbp not closed properly", !vbp.isOpen());
        Assert.assertTrue("vbp not marked ready", vbp.isReady());
        Assert.assertTrue("missing signature", vbp.contains(0, "1.0.2"));
        Assert.assertTrue("missing timestamp", vbp.contains(1, Long.class));
        Assert.assertTrue("missing msgid", vbp.contains(2, msgid.toLineData().toByteArray()));
        Assert.assertTrue("missing reversepath", vbp.contains(3, "a@a.com"));
        Assert.assertTrue("missing forwardpath count", vbp.contains(4, new Integer(1)));
        Assert.assertTrue("missing forwardpath", vbp.contains(5, "0b@b.com"));
        Assert.assertTrue("missing data", vbp.contains(6, data.getByteBuffer()));
    }
}