/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta;

import org.junit.Test;
import org.junit.Assert;

import java.nio.charset.Charset;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;

/**
 * Unit Test ASCIIString
 */
public class ASCIIStringUTest {

    /**
     * Test java encoding/decoding
     * <p>
     * Wouldn't normally test a framework, but there is a big assumption that ascii encoder/decoder
     * converts 7-bit data properly.
     */
    @Test
    public void boundary_EncoderDecoder() {
        byte[] set = new byte[128];

        for(int i=0; i < 128; ++i)
            set[i] = (byte) i;

        Charset ascii = Charset.forName("ASCII");

        CharBuffer cb = ascii.decode(ByteBuffer.wrap(set));

        int i = 0;
        for (char c : cb.array()) {
            Assert.assertTrue("decoding failed", c == i);
            ++i;
        }

        ByteBuffer bb = ascii.encode(cb);

        i = 0;
        for (byte b : bb.array()) {
            Assert.assertTrue("encoding failed", b == i);
            ++i;
        }
    }

    @Test
    public void boundary_asciipattern() throws ASCIIException {
        new ASCIIString("");//empty

        //all ascii char
        byte[] set = new byte[128];

        for(int i=0; i < 128; ++i)
            set[i] = (byte) i;

        Charset ascii = Charset.forName("ASCII");

        CharBuffer cb = ascii.decode(ByteBuffer.wrap(set));

        new ASCIIString(cb.toString());

        // embedded nonascii
        byte[] b = set.clone();

        b[1] = (byte) 128;

        cb = ascii.decode(ByteBuffer.wrap(b));

        try {
            new ASCIIString(cb.toString());
            Assert.fail("expected to fail constructor for invalid character");
        } catch(ASCIIException e) {}
        
    }

    
}
