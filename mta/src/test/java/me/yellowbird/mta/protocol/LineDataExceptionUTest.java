/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.protocol;

import me.yellowbird.mta.ASCIIString;
import org.junit.Assert;
import org.junit.Test;

/**
 * LineDataException unit test.
 */
public class LineDataExceptionUTest {
    @Test(expected = LineDataException.class)
    public void constructor_message_asciistring() throws LineDataException {
        LineDataException e = new LineDataException("message", new ASCIIString("a"));

        Assert.assertEquals("wrong message in exception", "message", e.getMessage());

        throw e;
    }

    @Test(expected = LineDataException.class)
    public void constructor_message_string() throws LineDataException {
        LineDataException e = new LineDataException("message", "a");

        Assert.assertEquals("wrong message in exception", "message", e.getMessage());

        throw e;
    }

    @Test(expected = LineDataException.class)
    public void constructor_message_bytes() throws LineDataException {
        LineDataException e = new LineDataException("message", new byte[1], 0);

        Assert.assertEquals("wrong message in exception", "message", e.getMessage());

        throw e;
    }
}