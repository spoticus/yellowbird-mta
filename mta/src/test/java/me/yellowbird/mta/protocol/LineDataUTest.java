/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.protocol;

import org.junit.Test;
import org.junit.Assert;
import me.yellowbird.mta.protocol.LineDataException;
import me.yellowbird.mta.protocol.LineData;

/**
 * Unit Test for LineData
 */
public class LineDataUTest {
    @Test
    public void boundary_crlfpattern() throws LineDataException {
        new LineData("");
        new LineData("a");
        new LineData("\r");
        new LineData("\n");
        try {
            new LineData("\r\n");
            Assert.fail("should have matched CRLF");
        } catch(LineDataException e) {}
        try {
            new LineData("a\r\n");
            Assert.fail("should have matched CRLF");
        } catch(LineDataException e) {}
        try {
            new LineData("\r\na");
            Assert.fail("should have matched CRLF");
        } catch(LineDataException e) {}
        try {
            new LineData("a\r\na");
            Assert.fail("should have matched CRLF");
        } catch(LineDataException e) {}
    }
}
