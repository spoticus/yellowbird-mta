package me.yellowbird.mta.protocol;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for MAILCommandReply
 */
public class MAILCommandReplyUTest {
    @Test
    public void boundary() throws LineDataException {
        MAILCommandReply cr = new MAILCommandReply(MAILCommandReply.Code.E421, new LineData("text"));
        Assert.assertTrue("wrong linedata generated", cr.toLineData().contentEquals("421 text"));
    }
}
