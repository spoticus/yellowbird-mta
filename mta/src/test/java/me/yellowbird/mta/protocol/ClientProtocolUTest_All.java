/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.protocol;

import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.InvalidMailBoxException;
import me.yellowbird.mta.brain.Brain;
import me.yellowbird.mta.io.Protocol;
import me.yellowbird.mta.io.ProtocolAsynchronizer;
import me.yellowbird.mta.nio.ChamillionSocketTransport;
import me.yellowbird.mta.transport.SourceTransport;
import org.hamcrest.Description;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.api.Action;
import org.jmock.api.Invocation;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;

/**
 * Test ClientProtocol
 *
 * - Happy path
 * - Failure for each expected reply
 * - Boundaries for the public methods
 *
 */
@RunWith(JMock.class)
public class ClientProtocolUTest_All {
    Mockery context = new JUnit4Mockery() {{
        setImposteriser(ClassImposteriser.INSTANCE);
    }};

    private Brain brain;
    private SourceTransport messagetosend;
    private ClientProtocol cp;

    @Before
    public void setup() {
        brain = context.mock(Brain.class);
        messagetosend = context.mock(SourceTransport.class);
    }

    @Test
    @Ignore
    public void logic_send_without_errors() throws InvalidMailBoxException, ClientProtocolException, IOException, InterruptedException {
        final ProtocolAsynchronizer pa = context.mock(ProtocolAsynchronizer.class);
        final Protocol p = context.mock(Protocol.class);
        final InetSocketAddress destaddr = context.mock(InetSocketAddress.class);
        final ChamillionSocketTransport cst = context.mock(ChamillionSocketTransport.class);

        context.checking(new Expectations() {{
//            try {
            one(pa).createProxy(with(aNonNull(ClientProtocol.class)));
            will(returnValue(p));
            one(brain).getMX(with(aNonNull(String.class)));
            will(returnValue(Arrays.asList(destaddr)));
            one(brain).getBestChamillionSocketTransport(with(any(InetAddress.class)));
            will(returnValue(cst));
//            will(new doit());
//                one(brain).createClientProtocol(with(aNonNull(AddrSpec.class)), with(aNonNull(AddrSpec.class)));
//                will(returnValue(new ClientProtocol()));
//                one(brain).getWelcomeMessage();
//                will(returnValue(new LineData("hello")));
//
//                oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("220 ")));
//                inSequence(responses);
//                oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("501 ")));
//                inSequence(responses);
//                oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("221 ")));
//                inSequence(responses);
//                oneOf(inboundtransport).sendClose();
//                inSequence(responses);
//            } catch (Throwable t) {
//            }
        }});

        cp = new ClientProtocol(new AddrSpec("from@a"), new AddrSpec("to@a"), pa, brain);

        context.checking(new Expectations() {{
            one(cst).connectClient(destaddr, p);

        }});

        cp.send(messagetosend);
    }

    class doit implements Action {
        @Override
        public void describeTo(Description description) {
            description.appendText("runs command");
        }

        @Override
        public Object invoke(Invocation invocation) throws Throwable {
            return null;  //To change body of implemented methods use File | Settings | File Templates.
        }
    }
}

