/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.protocol;

import me.yellowbird.mta.ASCIIException;
import me.yellowbird.mta.ASCIIString;
import me.yellowbird.mta.ByteBufferMatchers;
import me.yellowbird.mta.brain.Brain;
import me.yellowbird.mta.io.DataLink;
import me.yellowbird.mta.transport.InboundMessageBuilder;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.Sequence;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.nio.ByteBuffer;

/**
 * Test protocol from quit wait state -- commands, responses and state changes
 */
@RunWith(JMock.class)
public class ServerProtocol_QuitWaitStateUTest {
    Mockery context = new JUnit4Mockery() {{
        setImposteriser(ClassImposteriser.INSTANCE);
    }};

    private InboundMessageBuilder mb;
    private Brain brain;
    private ServerProtocol testserver ;
    private DataLink inboundtransport;
    private Sequence responses;

    @Before
    public void setup() {
        mb = context.mock(InboundMessageBuilder.class);
        brain = context.mock(Brain.class);
        inboundtransport = context.mock(DataLink.class);
        testserver = new ServerProtocol(brain, mb);
        responses = context.sequence("responses");
    }

    @Test
    public void logic_quitwait_to_quitwait() throws ASCIIException {

        context.checking(new Expectations() {{
            one(brain).isMTAReadyForNewConnections();
            will(returnValue(false));

            try {
                oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("554 ")));
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            inSequence(responses);
            try {
                oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("501 ")));
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            inSequence(responses);
        }});

//        testserver.expect("^554 .*","^501 .*");

        testserver.asyncConnected(inboundtransport);//mta not ready
        testserver.asyncData(ByteBuffer.wrap(new ASCIIString("BLAH\r\n").toByteArray()));

//        testserver.assertExpect();
    }

    @Test
    public void logic_quitwait_to_exit() throws ASCIIException {

        context.checking(new Expectations() {{
            one(brain).isMTAReadyForNewConnections();
            will(returnValue(false));

            try {
                oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("554 ")));
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            inSequence(responses);
            try {
                oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("221 ")));
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            inSequence(responses);
            oneOf(inboundtransport).sendClose();
            inSequence(responses);
        }});


//        testserver.expect("^554 .*","^221 .*");

        testserver.asyncConnected(inboundtransport);//quitwait

        testserver.asyncData(ByteBuffer.wrap(new ASCIIString("QUIT\r\n").toByteArray()));

//        testserver.assertExpect();

//        Assert.assertTrue("Expected shutdownConnection to me called", testserver.shutdown);
    }


}