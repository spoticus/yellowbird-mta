/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.protocol;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Unit test for ClientProtocolException.
 */
public class ClientProtocolExceptionUTest {
    /**
     * Test constructor.
     */
    @Test
    public void constructor_save_parameters() {
        RuntimeException e = new RuntimeException();

        ClientProtocolException c = new ClientProtocolException(true, e);
        Assert.assertTrue("saved wrong recoverable option", c.recoverable());
        Assert.assertEquals("saved wrong cause", e, c.getCause());

        c = new ClientProtocolException(false, e);
        Assert.assertFalse("saved wrong recoverable option", c.recoverable());
        Assert.assertEquals("saved wrong cause", e, c.getCause());

//        List<ClientProtocolException> reasons = Arrays.asList(new ClientProtocolException(true));
//        c = new ClientProtocolException(true, reasons);
//        Assert.assertEquals("did not save causes", reasons, c.);
    }
}
