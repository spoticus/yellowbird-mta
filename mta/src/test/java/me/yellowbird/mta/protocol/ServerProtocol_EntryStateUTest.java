/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.protocol;

import me.yellowbird.mta.ASCIIException;
import me.yellowbird.mta.ByteBufferMatchers;
import me.yellowbird.mta.brain.Brain;
import me.yellowbird.mta.io.DataLink;
import me.yellowbird.mta.transport.InboundMessageBuilder;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.Sequence;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.InetSocketAddress;

/**
 * Test protocol from entry state -- commands, responses and state changes
 */
@RunWith(JMock.class)
public class ServerProtocol_EntryStateUTest {
    Mockery context = new JUnit4Mockery() {{
        setImposteriser(ClassImposteriser.INSTANCE);
    }};

    private InboundMessageBuilder mb;
    private Brain brain;
    private ServerProtocol testserver ;
    private DataLink inboundtransport;
    private Sequence responses;

    @Before
    public void setup() {
        mb = context.mock(InboundMessageBuilder.class);
        brain = context.mock(Brain.class);
        inboundtransport = context.mock(DataLink.class);
        testserver = new ServerProtocol(brain, mb);
        responses = context.sequence("responses");
    }

    @Test
    public void logic_entry_to_quitwait() {

        context.checking(new Expectations() {{
            one(brain).isMTAReadyForNewConnections();
            will(returnValue(false));

            try {
                oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("554 ")));
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            inSequence(responses);
        }});

//        testserver.expect("^554 .*");

        testserver.asyncConnected(inboundtransport);//mta not ready

//        testserver.assertExpect();
    }

    @Test
    public void logic_entry_to_conn() throws ASCIIException {
        context.checking(new Expectations() {{
            try {
                one(brain).isMTAReadyForNewConnections();
                will(returnValue(true));
                allowing(inboundtransport).getRemoteInetSocketAddress();
                will(returnValue(new InetSocketAddress(44444)));
                one(brain).getWelcomeMessage();
                will(returnValue(new LineData("hello")));

                oneOf(mb).buildConnection(with(any(InetSocketAddress.class)));
                oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("220 ")));
                inSequence(responses);
            } catch (Throwable t) {
            }
        }});

//        testserver.expect("^220 .*");

        testserver.asyncConnected(inboundtransport);//should work

//        testserver.assertExpect();
    }
}