/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.protocol;

import me.yellowbird.mta.*;
import me.yellowbird.mta.brain.Brain;
import me.yellowbird.mta.io.DataLink;
import me.yellowbird.mta.transport.DataException;
import me.yellowbird.mta.transport.InboundMessageBuilder;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.Sequence;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Test protocol from transaction state -- commands, responses and state changes
 */
@RunWith(JMock.class)
public class ServerProtocol_TransactionStateUTest {
    Mockery context = new JUnit4Mockery() {
        {
            setImposteriser(ClassImposteriser.INSTANCE);
        }
    };

    static class TestableServerProtocol extends ServerProtocol {

        public TestableServerProtocol(Brain brain, InboundMessageBuilder messagebuilder) {
            super(brain, messagebuilder);
        }

        public boolean isStateReady() {
            return state == State.READY;
        }

        public boolean isStateData() {
            return state == State.DATA;
        }
    }
    
    private InboundMessageBuilder mb;
    private Brain brain;
    private TestableServerProtocol testserver;
    private DataLink inboundtransport;
    private Sequence responses;

    @Before
    public void setup() {
        mb = context.mock(InboundMessageBuilder.class);
        brain = context.mock(Brain.class);
        inboundtransport = context.mock(DataLink.class);
        testserver = new TestableServerProtocol(brain, mb);
        responses = context.sequence("responses");

        context.checking(new Expectations() {{
            allowing(inboundtransport).getRemoteInetSocketAddress();
            will(returnValue(new InetSocketAddress(44444)));
        }});

        //get server to transaction state
        context.checking(new Expectations() {
            {
                try {
                    one(brain).isMTAReadyForNewConnections();
                    will(returnValue(true));
                    oneOf(mb).buildConnection(with(any(InetSocketAddress.class)));
                    one(brain).getWelcomeMessage();
                    will(returnValue(new LineData("hello")));
                    allowing(brain).getEHLODomain();
                    will(returnValue(new Domain("localhost")));
                    one(brain).getEHLOGreeting();
                    will(returnValue(new LineData("hello")));

                    oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("220 ")));
                    inSequence(responses);
                    oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("250 ")));
                    inSequence(responses);

                    oneOf(mb).buildMAIL(with(any(AddrSpec.class)), with(any(ArrayList.class)));
                    inSequence(responses);
                    oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("250")));
                    inSequence(responses);
                } catch (Throwable t) {
                    throw new RuntimeException(t);
                }
            }
        });

        testserver.asyncConnected(inboundtransport);//conn

        testserver.asyncData(ByteBuffer.wrap(new ASCIIString("HELO domain.com\r\n").toByteArray()));
        testserver.asyncData(ByteBuffer.wrap(new ASCIIString("MAIL FROM:<j@c.com>\r\n").toByteArray()));

    }

    @Test
     public void logic_transaction_rcpt() {
         context.checking(new Expectations() {
             {
                 try {
                     oneOf(mb).buildRCPT(with(any(AddrSpec.class)), with(any(ArrayList.class)));
                     inSequence(responses);
                     oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("250")));
                     inSequence(responses);
                 } catch (Throwable t) {
                     throw new RuntimeException(t);
                 }
             }
         });
         testserver.asyncData(ByteBuffer.wrap(new ASCIIString("RCPT TO:<j@c.com>\r\n").toByteArray()));

     }

    @Test
    public void logic_transaction_rset() throws ASCIIException {

        context.checking(new Expectations() {
            {
                try {
                    oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("250 ")));
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                inSequence(responses);
            }
        });

        testserver.asyncData(ByteBuffer.wrap(new ASCIIString("RSET\r\n").toByteArray()));

        Assert.assertTrue("state did not change correctly", testserver.isStateReady());
    }

    @Test
    public void logic_transaction_vrfy() throws ASCIIException {

        context.checking(new Expectations() {
            {
                try {
                    oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("550 ")));
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                inSequence(responses);
            }
        });

        testserver.asyncData(ByteBuffer.wrap(new ASCIIString("VRFY\r\n").toByteArray()));

    }

    @Test
    public void logic_transaction_expn() throws ASCIIException {

        context.checking(new Expectations() {
            {
                try {
                    oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("550")));
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                inSequence(responses);
            }
        });

        testserver.asyncData(ByteBuffer.wrap(new ASCIIString("EXPN\r\n").toByteArray()));

    }

    @Test
    public void logic_transaction_help() throws ASCIIException {

        context.checking(new Expectations() {
            {
                try {
                    oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("214")));
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                inSequence(responses);
            }
        });

        testserver.asyncData(ByteBuffer.wrap(new ASCIIString("HELP\r\n").toByteArray()));

    }

    @Test
    public void logic_transaction_noop() throws ASCIIException {

        context.checking(new Expectations() {
            {
                try {
                    oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("250")));
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                inSequence(responses);
            }
        });

        testserver.asyncData(ByteBuffer.wrap(new ASCIIString("NOOP\r\n").toByteArray()));

    }

    @Test
    public void logic_transaction_helo() throws ASCIIException {

        context.checking(new Expectations() {
            {
                try {
//                    one(brain).getEHLODomain();
//                    will(returnValue(new Domain("localhost")));
                    one(brain).getEHLOGreeting();
                    will(returnValue(new LineData("hello")));

                    oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("501 ")));
                    inSequence(responses);
                    oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("250 ")));
                    inSequence(responses);
                } catch (Throwable t) {
                }
            }
        });


        testserver.asyncData(ByteBuffer.wrap(new ASCIIString("HELO\r\n").toByteArray()));
        testserver.asyncData(ByteBuffer.wrap(new ASCIIString("HELO abc.com\r\n").toByteArray()));

        Assert.assertTrue("state did not change correctly", testserver.isStateReady());
    }

    @Test
    public void logic_transaction_ehlo() throws ASCIIException {

        context.checking(new Expectations() {
            {
                try {
//                    oneOf(brain).getEHLODomain();
//                    will(returnValue(new Domain("localhost")));
                    oneOf(brain).getEHLOGreeting();
                    will(returnValue(new LineData("hello")));

                    oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("501 ")));
                    inSequence(responses);
                    oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.regex(Pattern.compile("^250-.*\\r\\n250 .*\\r\\n"))));
                    inSequence(responses);
                } catch (Throwable t) {
                }
            }
        });

        testserver.asyncData(ByteBuffer.wrap(new ASCIIString("EHLO\r\n").toByteArray()));
        testserver.asyncData(ByteBuffer.wrap(new ASCIIString("EHLO abc.com\r\n").toByteArray()));

        Assert.assertTrue("state did not change correctly", testserver.isStateReady());
    }

    @Test
    public void logic_transaction_data() throws ASCIIException, DataException {

        context.checking(new Expectations() {
            {
                oneOf(mb).buildDATA();
                try {
                    oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("354")));
                inSequence(responses);
                oneOf(inboundtransport).getRemoteInetAddress();
                will(returnValue(InetAddress.getByName("127.0.0.1")));
                allowing(brain).getPTR(with(any(InetAddress.class)));
                will(returnValue(null));
                    oneOf(inboundtransport).getLocalInetAddress();
                    will(returnValue(InetAddress.getByName("127.0.0.1")));
                } catch (Throwable e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }

            }
        });

        testserver.asyncData(ByteBuffer.wrap(new ASCIIString("DATA\r\n").toByteArray()));

        Assert.assertTrue("state did not change correctly", testserver.isStateData());
    }

}