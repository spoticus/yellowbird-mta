/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.protocol;

import org.hamcrest.Matcher;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import me.yellowbird.mta.protocol.LineData;
import me.yellowbird.mta.protocol.MultiLineReply;

/**
 * JMock Matchers for testing ASCIIString
 */
public class MultiLineReplyMatchers {
    static Matcher<MultiLineReply> startsWith(final String ... params ) {

        return new BaseMatcher<MultiLineReply>() {
            public boolean matches(Object o) {
                if (o instanceof MultiLineReply) {
                    MultiLineReply r = (MultiLineReply) o;

                    if (r.size() != params.length)
                        return false;

                    int i = 0;

                    for (LineData d : r) {
                        if (!d.toString().startsWith(params[i]))
                            return false;
                        ++i;
                    }

                    return true;
                }
                
                return false;
            }

            public void describeTo(Description description) {
                description.appendText("multiple lines starting with:");

                for (String s : params) {
                    description.appendText("\"" + s + "\" ");
                }
            }
        };

    }
}