/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.protocol;

import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.regex.Matcher;

@RunWith(JMock.class)
public class ServerProtocol_PatternUTest {
    Mockery context = new JUnit4Mockery() {{
        setImposteriser(ClassImposteriser.INSTANCE);
    }};

    @Test
    public void boundary_cmd_parm_patterns () {
        Matcher m = ServerProtocol.CMD_PARM_PATTERN.matcher("EHLO");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        m.reset("ehlo");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        m.reset("helo");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        m.reset("mail");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        m.reset("rcpt");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        m.reset("data");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        m.reset("rset");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        m.reset("vrfy");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        m.reset("expn");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        m.reset("help");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        m.reset("noop");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        m.reset("quit");
        Assert.assertTrue("failed " + m.toString(), m.matches());

        //any cmd for parm test
        m.reset("rset  ");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        m.reset("rset parm");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        m.reset("");
        Assert.assertFalse("failed " + m.toString(), m.matches());

    }

    @Test
    public void boundary_path_pattern() {
        Matcher m = ServerProtocol.PATH_PATTERN.matcher("from: <>");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        Assert.assertTrue("Expected null path", m.group(2).length()==0);
        Assert.assertTrue("Did not expect esmtp parms", m.group(3) == null);
        
        m.reset("FROM: <>");
        Assert.assertTrue("failed " + m.toString(), m.matches());

        m.reset("FROM:<>");//abutted to from: keyword
        Assert.assertTrue("failed " + m.toString(), m.matches());

        m.reset("from: <> blah");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        Assert.assertTrue("Expected null path", m.group(2).length()==0);
        Assert.assertTrue("Expected esmpt parm=blah got " + m.group(3), m.group(3).equals("blah"));

        m.reset("from: <d@d.com>");
        Assert.assertTrue("failed " + m.toString(), m.matches());

        m.reset("from: <d@d.com> blah");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        Assert.assertTrue("Did not expected null path", m.group(2).length() > 0);
        Assert.assertTrue("Expect mailbox=d@d.com got " + m.group(2), m.group(2).equals("d@d.com"));
        Assert.assertTrue("Expected esmpt parm=blah got " + m.group(3), m.group(3).equals("blah"));

        //rfc4281 4.1.2,Path,Mailbox,Local-part,Dot-string,Atom
        m.reset("from:<-@d.com>");

        //rfc4281 4.1.2,Path,Mailbox,Domain,(sub-domain,Let-dig,[Ldh-str]
        m.reset("from:<a@aa");
        m.reset("from:<a@a-a.com>");
        Assert.assertTrue("failed " + m.toString(), m.lookingAt());

        m.reset("from: <@c.com:d@d.com>");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        m.reset("from: <@c.com,@e.com:d@d.com>");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        m.reset("from: <@[127.0.0.1]:d@d.com>");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        m.reset("from: <@c.com,@[127.0.0.1]:d@d.com>");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        m.reset("from: <@[IPv6:1050:0:0:0:5:600:300c:326b:d]:d@d.com>");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        m.reset("from: <@[generic:data]:d@d.com>");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        m.reset("from: <d.d@d.com>");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        m.reset("from: <@d.com:d.d@d.com>");
        Assert.assertTrue("failed " + m.toString(), m.matches());
        m.reset("from: <\"me\"@d.com>");
        Assert.assertTrue("failed " + m.toString(), m.matches());
    }
}