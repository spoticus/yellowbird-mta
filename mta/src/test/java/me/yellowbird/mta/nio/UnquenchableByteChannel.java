/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.nio;

import java.nio.channels.ReadableByteChannel;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.io.IOException;

/**
 * A ReadableByteChannel that never runs out of data.
 */
public class UnquenchableByteChannel implements ReadableByteChannel {
    private static final Charset ascii = Charset.forName("US-ASCII");

    int counter;
    NumberFormat formatter = new DecimalFormat("000000");
    private boolean close = false;

    @Override
    public int read(ByteBuffer dst) throws IOException {
        if (!close) {
            dst.put(ascii.encode(formatter.format(counter++)));
            return 1;
        }
        return 0;
    }

    @Override
    public boolean isOpen() {
        return !close;
    }

    @Override
    public void close() throws IOException {
        close = true;
    }

}
