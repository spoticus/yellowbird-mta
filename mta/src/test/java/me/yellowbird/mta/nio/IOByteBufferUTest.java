/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.nio;

import org.junit.Test;
import org.junit.Assert;

import java.nio.ByteBuffer;

/**
 */
public class IOByteBufferUTest {
    @Test
    public void accessors() {
        ByteBuffer b = ByteBuffer.allocateDirect(100);

        IOByteBuffer iob = new IOByteBuffer(b);

        ByteBuffer b2 = iob.getByteBuffer();

        Assert.assertSame("byte buffers did not match", b, b2);

        Assert.assertTrue("position not the same", b.position() == b2.position());

        Assert.assertTrue("limit not the same", b.limit() == b2.limit());
    }
}
