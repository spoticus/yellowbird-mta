/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.nio;

import me.yellowbird.mta.brain.Brain;
import me.yellowbird.mta.io.DataLink;
import me.yellowbird.mta.io.DataLinkProblem;
import me.yellowbird.mta.io.Protocol;
import me.yellowbird.mta.io.ProtocolAsynchronizer;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.api.Invocation;
import org.jmock.lib.action.CustomAction;
import org.jmock.lib.legacy.ClassImposteriser;
import testhelper.OnThreadHelper;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Simple harness of transport.
 * <p/>
 * Provide all the threading startup and shutdown for testing server and
 * client protocols and recording results. Under the hood is a ChamillionSocketTransport.
 * The test harness uses the ChamillionSocketTransport to create servers and
 * clients with the general usage pattern that the servers and clients are
 * used together by test to communicate with each other.
 * <p>
 * However, it's not necessary that the harness have both servers and clients and
 * that they talk to each other. It's possible for a harness to only have servers
 * or only have clients.
 * <p>
 * The harness creates the ChamillionSocketTransport when
 * needed along with it's thread. When client protocols are connect the harness creates
 * a socket connect for the client protocol. Communication will ensue according
 * to the Protocol, which starts with a connected message to each. The test
 * is responsible for shutting the thing down. A convenience method shutdown()
 * is provided. A sync() method is also available to wait for the shutdown to complete.
 * <p>
 * Server protocols are either populated before they are needed by a client
 * connection, or a factory can me defined that will me used to create
 * them as needed. If both are provided then the pre-built protocols are used up
 * then the factory is used.
 * <p>
 * <h5>Usage #1 -  A test of a client communicating with a server</h5>
 * <pre>
 *     SimpleTransportHarness ssh = new SimpleTransportHarness("127.0.0.100");
 *
 *     Protocol server = ssh.new ProtocolAdapter() {
 *         ...
 *     }
 *
 *     Protocol client = ssh.new ProtocolAdapter() {
 *         ...
 *     }
 *
 *     ssh.startAndSync(server, client);
 * </pre>
 *
 * {@link #startAndSync} is a shortcut for:
 *
 * <pre>
 *     addServerProtocol(server);
 *     connectClient(client);
 *     sync();
 * </pre>
 *
 * </h5>Usage #2 - A test of multiple clients communicating with a server</h5>
 * <pre>
 *     SimpleTransportHarness ssh = new SimpleTransportHarness("127.0.0.100");
 *
 *     Protocol server_1 = ssh.new ProtocolAdapter() {
 *         ...
 *     }
 *
 *     Protocol server_2 = ssh.new ProtocolAdapter() {
 *         ...
 *     }
 *
 *     Protocol client_a = ssh.new ProtocolAdapter() {
 *         ...
 *     }
 *
 *     Protocol client_b = ssh.new ProtocolAdapter() {
 *         ...
 *     }
 *
 *     addServerProtocol(server_1);
 *     addServerProtocol(server_2);
 *     connectClient(client_a);
 *     connectClient(client_b);
 *     sync();
 * </pre>
 *
 * There is no guarantee in the ordering of clients to servers. If the servers performance
 * the same behavior then a factory can be provided instead that will produce a new server protocol
 * for every client request:
 *
 * <pre>
 *     ssh.setServerProtocolFactory(new SimpleTransportHarness.ProtocolFactory(){
 *         public Protocol createProtocol() {
 *             return ssh.new ProtocolAdapter() {
 *                 ...
 *             }
 *         }
 *     });
 * </pre>
 *
 * <h5>Protocol Helpers</h5>
 * {@link EchoProtocol} is a protocol adapter that echos back whatever data it receives.
 *
 * {@link ChatteringProtocol} is a protocol adapter that creates a constant transmission stream. 
 */
//TODO This bad boy needs to verify that the
public class SimpleTransportHarness {
    private static final int DEFAULT_SERVER_PORT = 25000;

    /*
     * Can create server protocols from factory.
     */
    private ProtocolFactory serverprotocolfactory;

    public static interface ProtocolFactory {
        public Protocol createProtocol();
    }

    public void setServerProtocolFactory(ProtocolFactory factory) throws IOException {
        if (cst == null)
            initCST();

        bindIfNotDefaultServer();

        this.serverprotocolfactory = factory;
    }

    /**
     * Adapter for Protocol that does nothing special.
     */
    public class ProtocolAdapter implements Protocol {

        public void asyncConnected(DataLink dataLink) throws InterruptedException {
        }

        public void asyncClosed() {
        }

        public void asyncData(ByteBuffer bytes) throws InterruptedException {
        }

        public void asyncException(DataLinkProblem problem) {
        }

        public void asyncInputShutdown() {
        }

        protected void shutdown() {
            SimpleTransportHarness.this.shutdown();
        }
    }

    /**
     * Echo everything back to the peer protocol.
      */
    public class EchoProtocol extends ProtocolAdapter {
        private DataLink dataLink;

        @Override
        public void asyncConnected(DataLink dataLink) throws InterruptedException {
            this.dataLink = dataLink;
        }

        @Override
        public void asyncData(ByteBuffer bytes) throws InterruptedException {
            dataLink.sendToDataLink(bytes);
        }
    }

    /**
     * Send all the available data provided.
     */
    public class ChatteringProtocol extends ProtocolAdapter {
        private final ReadableByteChannel channel;
        private int receivecount;

        public ChatteringProtocol(ReadableByteChannel channel) {
            this.channel = channel;
        }
        @Override
        public void asyncConnected(DataLink dataLink) throws InterruptedException {
            dataLink.sendToDataLink(channel);
        }

        @Override
        public void asyncData(ByteBuffer bytes) throws InterruptedException {
            receivecount += bytes.remaining();
        }

        public int receiveCount() {
            return receivecount;
        }
    }

    private final InetAddress sharedhost;

    private ChamillionSocketTransport cst;
    private OnThreadHelper chamtest;
    private OnThreadHelper ioasynchronizertestserver;
    private OnThreadHelper ioasynchronizertestclient;
    private ProtocolAsynchronizer ioasynchronizerserver;
    private ProtocolAsynchronizer ioasynchronizerclient;

    //queue of protocols for client connections.
    ConcurrentLinkedQueue<Protocol> serverprotocols = new ConcurrentLinkedQueue<Protocol>();

    // the chamillionsockettransport has bound a server to an address
    private boolean defaultserverbound = false;

    /**
     * Create a test harness that will run on the specified address and a default port.
     * <p/>
     * The server(s) and client(s) will use the host address specified as an argument.
     * A default port number will be used for the server, unless overridden during
     * the server startup.
     *
     * @param serverandclienthost host name or address used that is compatible with InetAddress.getByName(String).
     * @throws UnknownHostException something wrong.
     */
    /*
     * TODO must add logic that verifies that the serverandclienthost provided is available for testing.
     * Sometimes multiple runs of the test don't release bindings properly and that causes subsequent runs
     * to fail badly. This is really a problem for the test harness and not the class under
     * testing. It needs to be fixed, but the first step is to collect data as to why its not
     * releasing the bindings and the precursor to finding out why is to identify when.
     */
    public SimpleTransportHarness(String serverandclienthost) throws UnknownHostException {
        this.sharedhost = InetAddress.getByName(serverandclienthost);
    }

    /**
     * Start the CST, it's thread and the protocol threads.
     * @throws java.io.IOException something wrong.
     */
    private void initCST() throws IOException {
        ioasynchronizerserver = new ProtocolAsynchronizer();
        ioasynchronizertestserver = new OnThreadHelper(ioasynchronizerserver, "server ioasynchronizer");
        ioasynchronizertestserver.start();

        ioasynchronizerclient = new ProtocolAsynchronizer();
        ioasynchronizertestclient = new OnThreadHelper(ioasynchronizerclient, "client ioasynchronizer");
        ioasynchronizertestclient.start();

        Mockery context = new Mockery() {{
            setImposteriser(ClassImposteriser.INSTANCE);
        }};

        final Brain brain = context.mock(Brain.class);

        context.checking(new Expectations() {{
            allowing(brain).createServerProtocol();
            will(
                    new CustomAction("choose next server protocol") {
                        public Object invoke(Invocation invocation) throws Throwable {
                            Protocol p = serverprotocols.poll();
                            if (p == null && serverprotocolfactory != null)
                                p = serverprotocolfactory.createProtocol();
                            if (p == null)
                                throw new RuntimeException("out of server protocols");
                            return ioasynchronizerserver.createProxy(p);
                        }
                    }
            );
        }});

        cst = new ChamillionSocketTransport(brain);
        chamtest = new OnThreadHelper(cst);
        chamtest.start();

    }

    private void bindIfNotDefaultServer() throws IOException {
        if (!defaultserverbound) {
            cst.connectServer(new InetSocketAddress(sharedhost, DEFAULT_SERVER_PORT));

            defaultserverbound = true;
        }
    }

    /**
     * Start a server-side protocol on the default server.
     * <p/>
     * Start a server-side protocol using the default host and port number. It will be the default server
     * for client connections that don't specify one specifically.
     *
     * @param serverprotocol protocol to use for server-side.
     * @throws java.io.IOException when broken.
     */
    public void addServerProtocol(Protocol serverprotocol) throws IOException {
        if (cst == null)
            initCST();

        bindIfNotDefaultServer();

        serverprotocols.add(serverprotocol);
    }

    /**
     * Connect a client-side protocol to a foreign server.
     * <p>
     * Create a client connection with the provided client-side protocol where the server side
     * is not the default server, rather, is likely a server not provided by the harness's
     * ChamillionSocketTransport. This allows the harness to be used to communicate with other
     * servers even on other systems. The harness's ChamillionSocketTransport is not required to
     * have a server bound.
     * @param clientprotocol the client protocol to test.
     * @param foreignserver address and port of a remote server for the server-side of the connection.
     * @throws java.io.IOException when broken.
     */
    public void connectClient(Protocol clientprotocol, InetSocketAddress foreignserver) throws IOException {
        if (cst == null)
            initCST();

        cst.connectClient(foreignserver, ioasynchronizerclient.createProxy(clientprotocol));
    }

    /**
     * Connect a client-side protocol to the default server.
     * <p/>
     * This is typically the activity that will kick-off the test, where the server and/or client
     * reacts to protocol state changes by sending messages to the other side. The client is connected
     * to the default server using a protocol already added to the server by either {@link #addServerProtocol}
     * or {@link #setServerProtocolFactory}.
     *
     * @param clientprotocol the client protocol to test.
     * @throws IOException thrown when something goes wrong.
     */
    public void connectClient(Protocol clientprotocol) throws IOException {
        connectClient(clientprotocol, DEFAULT_SERVER_PORT);
    }

    private void connectClient(Protocol clientprotocol, int port) throws IOException {
        if (serverprotocols.size() == 0 && serverprotocolfactory == null)
            throw new RuntimeException("No server protocols have been registered and therefore no server exists for the connection");

        //use default server
        cst.connectClient(new InetSocketAddress(sharedhost, port), ioasynchronizerclient.createProxy(clientprotocol));
    }

    /**
     * Start a default server and client protocol.
     * <p/>
     * A shortcut that has the same effect as #initCST() and #connectClient() then #sync.
     * This is a common usage pattern.
     *
     * @param serverprotocol the server protocol to test.
     * @param clientprotocol the client protocol to test.
     * @throws Throwable thrown when something goes wrong.
     */
    public void startAndSync(Protocol serverprotocol, Protocol clientprotocol) throws Throwable {
        addServerProtocol(serverprotocol);
        connectClient(clientprotocol);
        sync();
    }

    public void shutdown() {
        cst.shutdown();
    }

    public void sync() throws Throwable {
        if (chamtest != null) {
            chamtest.sync(3000000);
//            System.out.println("chamtest.sync complete");
        }

        if (ioasynchronizertestserver != null) {
            ioasynchronizertestserver.shutdown(true);
            ioasynchronizertestserver.sync(30000);
//            System.out.println("ioasynchronizertestserver.sync complete");
        }

        if (ioasynchronizertestclient != null) {
            ioasynchronizertestclient.shutdown(true);
            ioasynchronizertestclient.sync(30000);
//            System.out.println("ioasynchronizertestclient.sync complete");
        }
    }
}
