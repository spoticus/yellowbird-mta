/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.nio;

import org.junit.Test;
import org.junit.Assert;

import java.nio.ByteBuffer;

/**
 * <ul>
 * <li>Allocating new physical memory
 * <li>Freeing memory
 * <li>Ignoring free memory that is wrong size
 * </ul>
 */
public class IOByteBufferFactoryUTest {

    @Test
    public void allocate_newmemory() {
        IOByteBufferFactory factory = new IOByteBufferFactory(100);

        IOByteBuffer b = factory.allocate();

        Assert.assertTrue("position not zero", b.getByteBuffer().position() == 0);
        Assert.assertTrue("limit not size of buffer", b.getByteBuffer().limit() == 100);
    }

    @Test
    public void free_memory() {
        IOByteBufferFactory factory = new IOByteBufferFactory(100);

        IOByteBuffer b = factory.allocate();

        factory.free(b);

        Assert.assertSame("freed buffer not re-used", b, factory.allocate());
    }

    @Test
    public void ignore_wrongsize_memory() {
        IOByteBufferFactory factory = new IOByteBufferFactory(100);

        IOByteBuffer b = new IOByteBuffer(ByteBuffer.allocate(10));

        factory.free(b);

        Assert.assertNotSame("buffer wrong size accepted by free", b, factory.allocate());
        
    }
    
}
