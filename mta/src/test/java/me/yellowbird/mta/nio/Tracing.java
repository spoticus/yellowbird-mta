/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.nio;

import org.aspectj.lang.annotation.Aspect;

/**
 * Advice for tracing socket I/O.
 */
@Aspect
public class Tracing {
//    @Pointcut("call(* *.*(..))")
//    void anyCall() {}

//    @Before("call(* me.yellowbird.mta..*(..)) && this(foo) && !call(* *.toString())")
//    public void traceAll(JoinPoint jp, Object foo) {
//        System.out.println("call from:" + foo + " at " + jp);
//    }

//    @AfterThrowing(pointcut = "call(* me.yellowbird.mta..*(..)) && !call(* *.toString())", throwing = "e")
//    public void traceThrow(JoinPoint jp, Throwable e) {
//        System.out.println("throwing:" + jp + ":" + e);
//    }
}
