/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta.spooler;

import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.brain.Brain;
import me.yellowbird.mta.mps.FileStorage;
import me.yellowbird.mta.mps.vbm.VBPSpooledMessage;
import me.yellowbird.mta.protocol.LineOfData;
import me.yellowbird.mta.transport.MessageIdentifier;
import me.yellowbird.mta.transport.SinkTransport;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * Test MPSSpooler.
 */
@RunWith(JMock.class)
public class FileSpoolerITest {
    Mockery context = new JUnit4Mockery() {{
        setImposteriser(ClassImposteriser.INSTANCE);
    }};

    /**
     * Test that the spooler will accept a message from the envelopebus, receive the data then provides to forwarders.
     * @throws Throwable it's broken.
     */
    @Test
    public void receive_message_logic () throws Throwable {
        final Brain brain = context.mock(Brain.class);

        final File spooldirectory = createTempDirectory();

        ArrayList<Pattern> domainpatterns = new ArrayList<Pattern>();
        domainpatterns.add(Pattern.compile(".*"));


        context.checking(new Expectations() {{
//            allowing(spooldirectory).listFiles(with(any(FileFilter.class)));
//            will(returnValue(new File[]{}));

//            one(brain).isMTAReadyForNewConnections();
//            will(returnValue(false));

//            try {
//                oneOf(inboundtransport).sendToDataLink(with(ByteBufferMatchers.startsWith("554 ")));
//            } catch (InterruptedException e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//            }
//            inSequence(responses);
        }});

        FileStorage storage = new FileStorage(spooldirectory);
        MPSSpooler fs = new MPSSpooler(storage, domainpatterns, brain);

        MessageIdentifier mid = MessageIdentifier.createMessageIdentifier();

        fs.getEnvelopeBusPlugin().doMAIL(mid, new AddrSpec("mybox@localhost"), null);

        SinkTransport ot = fs.getEnvelopeBusPlugin().doRCPT(mid, new AddrSpec("to@localhost"), null);

        ot.prepareForData();

        String message = "test data\r\n";

        ot.add(new LineOfData(ByteBuffer.wrap(message.getBytes("US-ASCII"))));

        ot.commit();

        VBPSpooledMessage f = (VBPSpooledMessage) fs.takeFileToForward();

        byte[] b = f.getData();

        Assert.assertArrayEquals("message data does not match", ByteBuffer.wrap(message.getBytes("US-ASCII")).array(), b);
//        Scanner scanner = new Scanner(f.getFile(), "US-ASCII");

//        byte[] b = new byte[8];

//        if (scanner.findWithinHorizon(message, 0) == null) {
//            HexDump.hexDump(f.getFile(), System.out);

//            Assert.assertTrue("message data not found in message", false);
//        }
    }

    /*
     * Test that the spooler will not accept messages for domains that don't match.
     * @throws Throwable it's broken.
     */
    @Test
    public void domainmatch_not_match_boundary() throws Throwable {
        final Brain brain = context.mock(Brain.class);

        final File spooldirectory = createTempDirectory();

        ArrayList<Pattern> domainpatterns = new ArrayList<Pattern>();
        domainpatterns.add(Pattern.compile("nodomain"));

        FileStorage storage = new FileStorage(spooldirectory);
        MPSSpooler fs = new MPSSpooler(storage, domainpatterns, brain);

        MessageIdentifier mid = MessageIdentifier.createMessageIdentifier();

        fs.getEnvelopeBusPlugin().doMAIL(mid, new AddrSpec("mybox@localhost"), null);

        SinkTransport ot = fs.getEnvelopeBusPlugin().doRCPT(mid, new AddrSpec("to@localhost"), null);

        Assert.assertNull("spooler should not have accepted forward address", ot);
    }

    /*
     * Test that spooler looks at all the patterns for domains it is given at construction.
     * @throws Throwable it's broken.
     */
    @Test
    public void domainmatch_multipatterns_boundary() throws Throwable {
        final Brain brain = context.mock(Brain.class);

        final File spooldirectory = createTempDirectory();

        ArrayList<Pattern> domainpatterns = new ArrayList<Pattern>();
        domainpatterns.add(Pattern.compile("nodomain"));
        domainpatterns.add(Pattern.compile("localhost"));

        FileStorage storage = new FileStorage(spooldirectory);
        MPSSpooler fs = new MPSSpooler(storage, domainpatterns, brain);

        MessageIdentifier mid = MessageIdentifier.createMessageIdentifier();

        fs.getEnvelopeBusPlugin().doMAIL(mid, new AddrSpec("mybox@localhost"), null);

        SinkTransport ot = fs.getEnvelopeBusPlugin().doRCPT(mid, new AddrSpec("to@localhost"), null);

        Assert.assertNotNull("spooler should have accepted forward address", ot);
    }

    static private File createTempDirectory() {
        File systempdir = null;
        try {
            systempdir = File.createTempFile("aaa", "").getParentFile();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        File tempdir = new File(systempdir, UUID.randomUUID().toString());

        if (!tempdir.mkdir())
            throw new RuntimeException("could not create a private temporary directory");

        return tempdir;
    }


}
