/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.mta;

import org.hamcrest.Matcher;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharacterCodingException;
import java.util.regex.Pattern;

/**
 * JMock Matchers for testing ByteBuffer
 * <p>
 * non-destructive test of data at current position.
 */
public class ByteBufferMatchers {
    private static final Charset ASCII = Charset.forName("ASCII");

    //assume ascii string
    public static Matcher<ByteBuffer> startsWith(final String param) {

        return new BaseMatcher<ByteBuffer>() {
            public boolean matches(Object o) {
                if (o instanceof ByteBuffer) {
                    ByteBuffer b = (ByteBuffer) o;

                    if (b.remaining() < param.length())
                        return false;

                    int pos = b.position();

                    CharBuffer cb;

                    try {
                        cb = ASCII.newDecoder().decode(b);
                    } catch (CharacterCodingException e) {
                        b.position(pos);
                        return false;
                    }

                    b.position(pos);

                    for (int i=0; i < param.length(); ++i)
                        if (cb.get(i) != param.charAt(i))
                            return false;

                }
                return true;
            }

            public void describeTo(Description description) {
                description.appendText("string starting with \"" + param + "\"");
            }
        };

    }

    //assume ascii string
    public static Matcher<ByteBuffer> regex(final Pattern pattern) {

        return new BaseMatcher<ByteBuffer>() {
            public boolean matches(Object o) {
                if (o instanceof ByteBuffer) {
                    ByteBuffer b = (ByteBuffer) o;

                    int pos = b.position();

                    CharBuffer cb;

                    try {
                        cb = ASCII.newDecoder().decode(b);
                    } catch (CharacterCodingException e) {
                        b.position(pos);
                        return false;
                    }

                    b.position(pos);

                    java.util.regex.Matcher m = pattern.matcher(cb);
                    
                    if(m.matches())
                        return true;

                }
                return false;
            }

            public void describeTo(Description description) {
                description.appendText("string matching \"" + pattern.pattern() + "\"");
            }
        };
    }
}