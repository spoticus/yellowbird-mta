/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package testhelper;

import me.yellowbird.mta.AddrSpec;
import me.yellowbird.mta.Domain;
import me.yellowbird.mta.MailParameter;
import me.yellowbird.mta.brain.Brain;
import me.yellowbird.mta.brain.ConfigurationException;
import me.yellowbird.mta.brain.EnvelopeBusListenerConfiguration;
import me.yellowbird.mta.brain.InboundConnectionConfiguration;
import me.yellowbird.mta.mps.FileFormatException;
import me.yellowbird.mta.mps.StorageException;
import me.yellowbird.mta.protocol.LineData;
import me.yellowbird.mta.protocol.LineDataException;
import me.yellowbird.mta.protocol.LineOfData;
import me.yellowbird.mta.spooler.SpoolerException;
import me.yellowbird.mta.transport.*;
import org.xbill.DNS.TextParseException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * A harness that contains multiple MTAs.
 */
public class MultiMTATestHarness {
    private TreeMap<String,MTA> mtas = new TreeMap<String,MTA>();//the test MTAs

    private int lastsuffix = 99;//last suffix used for allocating ip addresses to mtas

    /**
     * Create an MTA with specific MDAs.
     * <p>
     * Create a harness with one MTA that has specific MDAs on the
     * envelope bus and it's spooler will reject everything that is
     * not handled by one of the MDAs.
     */

    /**
     * Create multiple MTAs that accept messages distinguished by a domain name.
     *
     * As if constructing with MultiMTATestHarness(true, mtadomains)
     *
     * @param mtadomains the domain names for the test MTAs that are also the keys for referencing the MTAs.
     */
    public MultiMTATestHarness(String... mtadomains) {
        this(true, mtadomains);
    }

    /**
     * Create multiple MTAs that accept messages distinguished by a domain name.
     * <p>
     * Each MTA will bind a server to a unique socket. The configuration for each
     * MTA will include an MDA to sink mail that the MTA receives for it's domain
     * and will have MX overrides configured in their brains so that the MTA knows
     * how to forward email destined for one of the other domains.
     *
     * @param openrelay configure MTAs as openrelays, otherwise, restrict relay to each other.
     * @param mtadomains the domain names for the test MTAs that are also the keys for referencing the MTAs.
     */
    public MultiMTATestHarness(boolean openrelay, String... mtadomains) {

        for (String domain : mtadomains) {
            ++lastsuffix;

            if (openrelay)
                mtas.put(domain, new MTA("127.0.0." + lastsuffix, 25000, domain));
            else
                mtas.put(domain, new MTA("127.0.0." + lastsuffix, 25000, domain, mtadomains));//this works, matching names of all MTAs

            //tell the mtas how to route to each other
            for (MTA destination : mtas.values()) {
                for (MTA originator : mtas.values()) {
                    if (destination != originator) //not self
                        originator.putMailExchangeOverride(destination.getDomain(), destination.getServerAddress());
                }
            }
        }
    }

    /**
     * Send a message through a domain to a recipient.
     * <p>
     * Create a generic email message and send it to the MTA identified as a parameter.
     *
     * @param ehlodomain domain name to use in ehlo command
     * @param mtadomainkey Key to MTA that will originate the message.
     * @param messagetext Text to send in message. Must containg crlf line ending.
     * @param rcptaddress Recipient(s) that will receive the message.
     * @throws SendAMessageException message could not be sent by client helper.
     */
    public void sendToRcpt(Domain ehlodomain, String mtadomainkey, String messagetext, String... rcptaddress) throws SendAMessageException {
        MTA relaymta = mtas.get(mtadomainkey);

        for (String rcpt : rcptaddress) {
            SendAMessage sam;
//            try {
                sam = new SendAMessage(relaymta.getServerIP(), relaymta.getServerPort(), ehlodomain, "me@[" + relaymta.getServerIP() + "]", rcpt, new String[]{messagetext});

                sam.send();
//            } catch (SendAMessageException e) {
//                throw new RuntimeException(e);
//            }
        }
    }

    /**
     * Wait for an MTA to receive a message then return that message.
     *
     * @param mtadomainkey Key to MTA that will originate the message.
     * @return The next message received by the MTA.
     * @throws java.util.concurrent.TimeoutException no message arrived.
     */
    public Message waitForMessage(String mtadomainkey) throws TimeoutException {
        try {
            return mtas.get(mtadomainkey).waitForMessage();
        } catch (InterruptedException e) {
            throw new TimeoutException();
        }
    }

    /**
     * Properly shutdown all MTAs and release resources.
     */
    public void shutdown() {
        for (MTA mta : mtas.values()) {
            try {
                mta.shutdown();
            } catch (Throwable throwable) {
                throw new RuntimeException(throwable);
            }
        }
    }

    /**
     * Add an MTA to the harness.
     *
     * @param mtadomainkey Key to MTA that will originate the message.
     * @param retryInterval retry interval in seconds.
     * @param giveupInterval giveup interval in seconds.
     */
    public void addMTA(String mtadomainkey, int retryInterval, int giveupInterval) {
        mtas.put(mtadomainkey, new MTA("127.0.0." + ++lastsuffix, 25000, mtadomainkey, new NullConfiguration.RetryInterval(retryInterval), new NullConfiguration.GiveupInterval(giveupInterval)));

        verifyRoutability();
    }

    /**
     * Add an MTA to the harness.
     *
     * @param mtadomainkey Key to MTA that will originate the message.
     * @param envelopeBusListener envelope bus listener added to being of envelope bus.
     */
    public void addMTA(String mtadomainkey, EnvelopeBusListener envelopeBusListener) {
        mtas.put(mtadomainkey, new MTA("127.0.0." + ++lastsuffix, 25000, mtadomainkey, envelopeBusListener));

        verifyRoutability();
    }

    public void addMTA(String mtadomainkey) {
        MTA mta = new MTA("127.0.0." + ++lastsuffix, 25000, mtadomainkey);

        mtas.put(mtadomainkey, mta);

        verifyRoutability();
    }

    /*
     * Verify that every MTA knows how to route to the other MTAs.
     * An MTA may have special routing for another, so don't blinding replace the routing.
     */
    private void verifyRoutability() {
        for (MTA destination : mtas.values()) {
            for (MTA originator : mtas.values()) {
                //not self and route not already present
                try {
                    if (destination != originator && originator.brain.getMX(destination.getDomain()).size() == 0)
                        originator.putMailExchangeOverride(destination.getDomain(), destination.getServerAddress());
                } catch (TextParseException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    public void putMailExchangeOverride(String mtadomainkey, String destinationdomain, String routethroughdomain) {
        MTA overridemta = mtas.get(mtadomainkey);

        MTA destinationmta = mtas.get(destinationdomain);

        MTA routethroughmta = mtas.get(routethroughdomain);

        overridemta.brain.putMailExchangeOverride(destinationdomain, routethroughmta.getServerAddress());
    }

    /**
     * Internal test MTA
     */
    private static class MTA {

        private final String serverIP;
        private final int serverPort;
        private final Brain brain;
        private final OnThreadHelper mtathreadhelper;
        private final MemoryMDA mda;
        private final String localdeliverydomain;

        MTA(String serverIP, int serverPort, String localdeliverydomain) {
            this(serverIP, serverPort, localdeliverydomain, new String[]{".*"});
        }

        MTA(String serverIP, int serverPort, String localdeliverydomain, String[] spoolerPatterns) {
            this(serverIP, serverPort, localdeliverydomain, spoolerPatterns, null, null, null);
        }

        public MTA(String serverIP, int serverPort, String localdeliverydomain, EnvelopeBusListener envelopeBusListener) {
            this(serverIP, serverPort, localdeliverydomain, new String[]{".*"}, null, null, envelopeBusListener);
        }

        /**
         * Construct MTA.
         *
         * @param serverIP ip address for server.
         * @param serverPort port number for server.
         * @param localdeliverydomain tld domain for selecting message to route to internal mda.
         * @param retryInterval retry interval in minutes.
         * @param giveupInterval giveup interval in minutes.
         */
        public MTA(String serverIP, int serverPort, String localdeliverydomain, NullConfiguration.RetryInterval retryInterval, NullConfiguration.GiveupInterval giveupInterval) {
            this(serverIP, serverPort, localdeliverydomain, new String[]{".*"}, retryInterval, giveupInterval, null);
        }

        MTA(String serverIP, int serverPort, String localdeliverydomain, String[] spoolerPatterns, NullConfiguration.RetryInterval retryInterval, NullConfiguration.GiveupInterval giveupInterval, EnvelopeBusListener envelopeBusListener) {
            this.serverIP = serverIP;
            this.serverPort = serverPort;
            this.localdeliverydomain = localdeliverydomain;

            mda = new MemoryMDA(localdeliverydomain);

            EnvelopeBusListenerConfiguration[] envelopeBusListenerConfiguration;
            if (envelopeBusListener != null) {
                envelopeBusListenerConfiguration = new EnvelopeBusListenerConfiguration[2];
                envelopeBusListenerConfiguration[0] = new EnvelopeBusListenerConfiguration("manual", envelopeBusListener);
                envelopeBusListenerConfiguration[1] = new EnvelopeBusListenerConfiguration("memorymda", mda);
            } else {
                envelopeBusListenerConfiguration = new EnvelopeBusListenerConfiguration[1];
                envelopeBusListenerConfiguration[0] = new EnvelopeBusListenerConfiguration("memorymda", mda);
            }

            try {
                brain = new Brain(
                        new NullConfiguration(
                                envelopeBusListenerConfiguration,
                                new InboundConnectionConfiguration[]{new InboundConnectionConfiguration(serverIP, serverPort)},
                                spoolerPatterns,
                                new Domain(localdeliverydomain),
                                retryInterval==null?new NullConfiguration.RetryInterval(1):retryInterval, giveupInterval==null?new NullConfiguration.GiveupInterval(2):giveupInterval),
                        new NullBrainStem());
            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (ConfigurationException e) {
                throw new RuntimeException(e);
            } catch (LineDataException e) {
                throw new RuntimeException(e);
            } catch (FileFormatException e) {
                throw new RuntimeException(e);
            } catch (SpoolerException e) {
                throw new RuntimeException(e);
            } catch (StorageException e) {
                throw new RuntimeException(e);
            }

            mtathreadhelper = new OnThreadHelper(brain, "Brain for " + localdeliverydomain);

             mtathreadhelper.start();

             if (!brain.isMTAReadyForNewConnections()) {
                 synchronized (this) {
                     try {
                         this.wait(6000);
                     } catch (InterruptedException e) {
                         throw new RuntimeException(e);
                     }
                 }

                 if (!brain.isMTAReadyForNewConnections()) {
                     throw new RuntimeException("MTA never started");
                 }
             }
         }


        String getServerIP() {
            return serverIP;
        }

        int getServerPort() {
            return serverPort;
        }

        public void shutdown() throws Throwable {
            mtathreadhelper.shutdown(false);
            mtathreadhelper.sync(30000);
        }

        public Message waitForMessage() throws InterruptedException {
            return mda.waitForMessage();
        }

        public String getDomain() {
            return localdeliverydomain;
        }

        public InetSocketAddress getServerAddress() {
            return new InetSocketAddress(serverIP, serverPort);
        }

        public void putMailExchangeOverride(String domain, InetSocketAddress serverAddress) {
            brain.putMailExchangeOverride(domain, serverAddress);
        }

        public class MemoryMDA implements RCPTListener {
            private BlockingQueue<Message> receivedmessages = new LinkedBlockingQueue<Message>();
            private final String localdeliverydomain;

            public MemoryMDA(String localdeliverydomain) {
                this.localdeliverydomain = localdeliverydomain;
            }

            @Override
            public SinkTransport doRCPT(MessageIdentifier msgid, AddrSpec addrSpec, ArrayList<MailParameter> mailparams) throws ForwardPathException {
                if (addrSpec.getDomain().equalsIgnoreCase(localdeliverydomain))
                    return new MemoryOutboundTransport(receivedmessages, new Message(addrSpec.toLineData()));
                return null;
            }

            public Message waitForMessage() throws InterruptedException {
                return receivedmessages.poll(6, TimeUnit.HOURS);
            }
        }

        public static class MemoryOutboundTransport implements SinkTransport {
            private BlockingQueue<Message> receivedmessages;
            private Message message;

            /**
             *
             * @param receivedmessages blocking queue to hold completed message.
             * @param message stubbed message to be filled with data.
             */
            public MemoryOutboundTransport(BlockingQueue<Message> receivedmessages, Message message) {
                this.receivedmessages = receivedmessages;
                this.message = message;
            }

            @Override
            public void prepareForData() throws MessageDataException {
                message.clearData();
            }

            @Override
            public void add(LineOfData data) throws MessageDataException {
                message.add(data);
            }

            @Override
            public void commit() throws MessageDataException {
                receivedmessages.offer(message);
            }

            @Override
            public void cancel() {
            }
        }
    }

    /**
     * Message objects inside of MTAs in the harness.
     */
    public static class Message {
        private ArrayList<LineOfData> lineofdatas = new ArrayList<LineOfData>();
        private final LineData forwardpath;

        public Message(LineData forwardpath) {
            this.forwardpath = forwardpath;
        }

        public boolean isData(String data) {
            return getText().compareTo(data) == 0;
        }

        public boolean isForwardPath(String forwardpath) {
            String bb = null;
            try {
                bb = new String(this.forwardpath.toByteArray(), "US-ASCII");
            } catch (UnsupportedEncodingException e) {
                return false;
            }

            return forwardpath.compareTo(bb) == 0;
        }

        public void clearData() {
            lineofdatas = new ArrayList<LineOfData>();
        }

        public void add(LineOfData data) {
            lineofdatas.add(data);
        }

        public String getText() {
            StringBuffer sb = new StringBuffer();

            ByteBuffer bb;
            for (LineOfData line : lineofdatas) {
                try {
                    bb = line.getByteBuffer();
                    sb.append(new String(Arrays.copyOfRange(bb.array(), bb.arrayOffset(), bb.arrayOffset()+bb.remaining()), "US-ASCII"));
                } catch (UnsupportedEncodingException e) {
                    throw new RuntimeException(e);//should not have happened
                }
            }

            return sb.toString();
        }

        public LineOfData[] getLinesOfData() {
            return lineofdatas.toArray(new LineOfData[lineofdatas.size()]);
        }
    }

    static public class MailExchangeOverride implements EnvelopeBusListener {
        public MailExchangeOverride(String destinationdomain, String relaythrough) {
        }
    }
}
