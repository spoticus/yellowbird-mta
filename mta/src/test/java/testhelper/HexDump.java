/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package testhelper;

import me.yellowbird.mta.protocol.LineOfData;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Formatter;


public class HexDump {
    private Formatter formatter;

    private HexDump(PrintStream stream) {
        formatter = new Formatter(stream);
    }

    private void dump(byte[] b, int offset, int len) {
        char[] p = replaceWithPrintable(b, offset, len);

        for (int l=offset; l < p.length; ++l)
            formatter.format("%02x ", b[l]);

        formatter.format("%s", "  ");

        for (int l=0; l < p.length; ++l)
            formatter.format("%c", p[l]);

        formatter.format("%c", '\n');
    }

    private void dump(LineOfData lod) {
        dump(lod.getByteBuffer().array(), lod.getByteBuffer().arrayOffset(), lod.getByteBuffer().remaining());
    }

    static public void hexDump(File file, PrintStream stream) throws IOException {
        HexDump hd = new HexDump(stream);

        FileInputStream fis = new FileInputStream(file);

        byte[] b = new byte[8];

        int i;
        while((i=fis.read(b)) > 0) {
            hd.dump(b, 0, i);
        }
    }

    static private char[] replaceWithPrintable(byte[] b, int offset, int len) {
        char[] c = new char[len];

        for (int i=offset; i < c.length; ++i)
            c[i] = b[i] < 32 ? ' ' : (char)b[i];

        return c;
    }

    public static void hexDump(LineOfData[] linesOfData, PrintStream stream) {
        HexDump hd = new HexDump(stream);

        for (LineOfData lod : linesOfData) {
            hd.dump(lod);
        }
    }

}
