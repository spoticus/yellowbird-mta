/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package testhelper;

import me.yellowbird.mta.Domain;
import me.yellowbird.mta.brain.Configuration;
import me.yellowbird.mta.brain.ConfigurationException;
import me.yellowbird.mta.brain.EnvelopeBusListenerConfiguration;
import me.yellowbird.mta.brain.InboundConnectionConfiguration;
import me.yellowbird.mta.protocol.LineData;
import me.yellowbird.mta.protocol.LineDataException;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * Null Pattern configuration for the brain that allows default overrides.
 */
public class NullConfiguration implements Configuration {
    private final InboundConnectionConfiguration[] inboundConnectionConfigurations;
    private final String messageDirectory;
    private final EnvelopeBusListenerConfiguration[] envelopeBusListenerConfigurations;
    private final String[] spoolerPatterns;
    private final Domain ehloDomain;
    private final RetryInterval retryInterval;
    private final GiveupInterval giveupInterval;

    public NullConfiguration(InboundConnectionConfiguration[] inboundConnectionConfigurations, String[] spoolerPatterns) throws IOException {
        this.envelopeBusListenerConfigurations = new EnvelopeBusListenerConfiguration[0];
        this.inboundConnectionConfigurations = inboundConnectionConfigurations;
        this.spoolerPatterns = spoolerPatterns;
        messageDirectory = createTemporaryDirectory();
        try {
            this.ehloDomain = new Domain("localhost");
        } catch (LineDataException e) {
            throw new RuntimeException(e);
        }
        retryInterval = new RetryInterval(30);
        giveupInterval = new GiveupInterval(5 * 24 * 60);
    }

    public NullConfiguration(EnvelopeBusListenerConfiguration[] envelopeBusListenerConfigurations,
                             InboundConnectionConfiguration[] inboundConnectionConfigurations,
                             String[] spoolerPatterns, Domain ehloDomain) throws IOException {
        this.envelopeBusListenerConfigurations = envelopeBusListenerConfigurations;
        this.inboundConnectionConfigurations = inboundConnectionConfigurations;
        this.spoolerPatterns = spoolerPatterns;
        messageDirectory = createTemporaryDirectory();
        this.ehloDomain = ehloDomain;
        retryInterval = new RetryInterval(30);
        giveupInterval = new GiveupInterval(5 * 24 * 60);
    }

    public NullConfiguration(EnvelopeBusListenerConfiguration[] envelopeBusListenerConfigurations,
                             InboundConnectionConfiguration[] inboundConnectionConfigurations,
                             String[] spoolerPatterns) throws IOException {
        this.envelopeBusListenerConfigurations = envelopeBusListenerConfigurations;
        this.inboundConnectionConfigurations = inboundConnectionConfigurations;
        this.spoolerPatterns = spoolerPatterns;
        messageDirectory = createTemporaryDirectory();
        try {
            this.ehloDomain = new Domain("localhost");
        } catch (LineDataException e) {
            throw new RuntimeException(e);
        }
        retryInterval = new RetryInterval(30);
        giveupInterval = new GiveupInterval(5 * 24 * 60);
    }

    public NullConfiguration(EnvelopeBusListenerConfiguration[] envelopeBusListenerConfigurations, InboundConnectionConfiguration[] inboundConnectionConfigurations, String[] spoolerPatterns, Domain ehloDomain, RetryInterval retryInterval, GiveupInterval giveupInterval) throws IOException {
        this.envelopeBusListenerConfigurations = envelopeBusListenerConfigurations;
        this.inboundConnectionConfigurations = inboundConnectionConfigurations;
        this.spoolerPatterns = spoolerPatterns;
        messageDirectory = createTemporaryDirectory();
        this.ehloDomain = ehloDomain;
        this.retryInterval = retryInterval;
        this.giveupInterval = giveupInterval;
    }


    private String createTemporaryDirectory() throws IOException {
        File tempfile = File.createTempFile("configuration",null);
        File tempdir = tempfile.getParentFile();

        File f = new File(tempdir, UUID.randomUUID().toString());
        if (!f.mkdir())
            throw new RuntimeException("could not create directory " + f.getAbsolutePath());

        return f.getAbsolutePath();
    }

    @Override
    public EnvelopeBusListenerConfiguration[] getEnvelopeBusListenerConfigurations() throws ConfigurationException {
        return envelopeBusListenerConfigurations;
    }

    @Override
    public InboundConnectionConfiguration[] getInboundConnectionConfiguration() throws ConfigurationException {
        return inboundConnectionConfigurations;
    }

    @Override
    public String[] getSpoolerPatterns() throws ConfigurationException {
        return spoolerPatterns;
    }

    @Override
    public String getMessageDirectory() throws ConfigurationException {
        return messageDirectory;
    }

    @Override
    public int getRetryInterval() throws ConfigurationException {
        return retryInterval.getMinutes();
    }

    @Override
    public int getGiveupInterval() throws ConfigurationException {
        return giveupInterval.getMinutes();
    }

    @Override
    public LineData getWelcomeMessage() throws ConfigurationException {
        try {
            return new LineData("connected");
        } catch (LineDataException e) {
            throw new ConfigurationException(e);
        }
    }

    @Override
    public Domain getEHLODomain() throws ConfigurationException {
        return ehloDomain;
    }

    @Override
    public LineData getEHLOGreeting() throws ConfigurationException {
        try {
            return new LineData("welcome");
        } catch (LineDataException e) {
            throw new ConfigurationException(e);
        }
    }

    public static class RetryInterval {
        private int retryInterval;

        public RetryInterval(int retryInterval) {
            this.retryInterval = retryInterval;
        }

        public int getMinutes() {
            return retryInterval;
        }
    }

    public static class GiveupInterval {
        private int giveupInterval;

        public GiveupInterval(int giveupInterval) {
            this.giveupInterval = giveupInterval;
        }

        public int getMinutes() {
            return giveupInterval;
        }
    }



}
