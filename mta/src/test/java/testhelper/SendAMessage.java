/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package testhelper;

import me.yellowbird.mta.Domain;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
                      
/**
 * Simple Client for Sending a Message.
 * <p/>
 * Provides for sending a simple message to a remote MTA and hooks for monitoring the progress and
 * results.
 */
public class SendAMessage implements Runnable {

    private static final Charset ASCII = Charset.forName("ASCII");

    private SocketChannel socketChannel;
    private String savebuffer; //incoming data not yet consumed
    private String host;
    private int port;
    private String from;
    private String to;
    private String[] message;
    private Domain ehlodomain;

    public SendAMessage(String host, int port, String from, String to, String[] message) throws SendAMessageException {
        this(host, port, null, from, to, message);
    }

    public SendAMessage(String host, int port, Domain ehlodomain, String from, String to, String[] message) {
        this.host = host;
        this.port = port;
        this.ehlodomain = ehlodomain;
        this.from = from;
        this.to = to;
        this.message = message;
    }

    /**
     * To allow message to me sent on a separate thread.
     */
    public void run() {
        try {
            send();
        } catch (SendAMessageException e) {
            throw new RuntimeException(e);
        }
    }

    public void send() throws SendAMessageException {
//        try {
        final InetSocketAddress serverAddress;
        try {
            serverAddress = new InetSocketAddress(InetAddress.getByName(host), port);
        } catch (UnknownHostException e) {
            throw new SendAMessageException(e);
        }

        try {
            socketChannel = SocketChannel.open();

        if (!socketChannel.connect(serverAddress))
                throw new SendAMessageException("connection failed:" + host + "/" + port);

            //wait for welcome 220
            waitFor220();

            //send helo
            sendHelo();

            //wait for 250
            waitFor250();

            //start transaction
            sendMail(from);

            //wait for 250 transaction started
            waitFor250();

            //send recipient
            sendRcpt(to);

            //wait for 250 recipient accepted
            waitFor250();

            //send data
            sendData();

            //wait for 354 ready
            waitFor354();

            //send data
            sendMessage(message);

            //wait for 250 data accepted
            waitFor250();

            socketChannel.close();

        } catch (IOException e) {
            throw new SendAMessageException(e);
        }

//        } catch (Throwable e) {
//            throw new SendAMessageException(e);
//        }
    }

    private void send(String text) throws IOException {
        ByteBuffer data = ASCII.encode(text + "\r\n");

        socketChannel.write(data);
    }

    private void sendHelo() throws IOException {
        if (ehlodomain != null)
            send("HELO " + ehlodomain);
        else
            send("HELO mydomain.com");
    }

    private void sendMail(String from) throws IOException {
        send("MAIL FROM:<" + from + ">");
    }

    private void sendRcpt(String to) throws IOException {
        send("RCPT TO:<" + to + ">");
    }

    private void sendData() throws IOException {
        send("DATA");
    }

    private void sendMessage(String[] message) throws IOException {
        for (String s : message) {
            if (s.startsWith("."))//transparency
                s = "." + s;

            ByteBuffer data = ASCII.encode(s);

            socketChannel.write(data);
        }

        send(".");//EOM
    }

    private String readLine() throws IOException {
        int index;

        do {
            ByteBuffer dst = ByteBuffer.allocate(1001);

            socketChannel.read(dst);

            dst.flip();

            CharBuffer cb = ASCII.decode(dst);

            if (savebuffer == null)
                savebuffer = cb.toString();
            else
                savebuffer += cb.toString();

            index = savebuffer.indexOf("\r\n");

        } while (index == -1);

        String r = savebuffer.substring(0, index);

        savebuffer = savebuffer.substring(index+2, savebuffer.length());

        return r;
    }

    private void waitForResponse(String response) throws IOException, SendAMessageException {
        String s = readLine();

        if (!s.startsWith(response))
            throw new UnexpectedReplyException("waited for response " + response + " but got " + s, response, s);
    }

    private void waitFor220() throws IOException, SendAMessageException {
        waitForResponse("220");
    }

    private void waitFor250() throws IOException, SendAMessageException {
        waitForResponse("250");
    }

    private void waitFor354() throws IOException, SendAMessageException {
        waitForResponse("354");
    }


}
