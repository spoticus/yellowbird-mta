/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.cmd;

import java.io.OutputStream;
import java.io.IOException;

/**
 * Tee outputstream
 */
public class OutTee extends OutputStream {
    private OutputStream[] out;

    public OutTee(OutputStream... out) {
        this.out = out;
    }

    @Override
    public void write(byte[] b) throws IOException {
        for (OutputStream o : out)
            o.write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        for (OutputStream o : out)
            o.write(b, off, len);
    }

    @Override
    public void flush() throws IOException {
        for (OutputStream o : out)
            o.flush();
    }

    @Override
    public void close() throws IOException {
        for (OutputStream o : out)
            o.close();
    }

    public void write(int b) throws IOException {
        for (OutputStream o : out)
            o.write(b);
    }
}
