/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.cmd;

import org.junit.Ignore;
import org.junit.Test;
import testhelper.OnThreadHelper;

import java.io.*;
import java.net.URL;

/**
 * Test YellowBird
 * <ul>
 * <li>Start and stop server
 */
public class YellowBirdITest {

    @Test
    @Ignore
    public void startup_shutdown() throws Throwable {
        final URL rsc = this.getClass().getResource("xmlconfigurationitest.xml");

        /*
         * Pipe all output from MTA to a blocking queue so it can be read from
         * but also timed out in case MTA hangs.
         */

        PrintStream savedsystemout = System.out;

        PipedOutputStream appstemout = new PipedOutputStream();
        PrintStream appstemoutprint = new PrintStream(new OutTee(System.out, appstemout));
        System.setOut(appstemoutprint);

        final PipedInputStream fromappstem = new PipedInputStream(appstemout);

        /*
         * Pipe all input from a writer so the test can send commands
         */

        InputStream savedsystemin = System.in;

        PipedInputStream appstemin = new PipedInputStream();
        final PrintWriter appsteminwriter = new PrintWriter(new OutputStreamWriter(new PipedOutputStream(appstemin)));
        System.setIn(appstemin);

        /*
         * Run the MTA then tell it to quit.
         */

        OnThreadHelper mta = new OnThreadHelper(new Runnable() {
            public void run() {
                try {
                    YellowBird.main(new String[]{"--configuration", rsc.toExternalForm()});
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        }, "YellowBird MTA");

        mta.start();

        /*
         * catch hangs
         */
        final Thread me = Thread.currentThread();

        /*
        * Wait for the message that the app has started
        */
        OnThreadHelper controller = new OnThreadHelper(new Runnable() {
            public void run() {

                StringBuffer sb = new StringBuffer();

                while (true) {
                    int i;
                    try {
                        i = fromappstem.read();
                    } catch (IOException e) {
                        e.printStackTrace();
                        me.interrupt();
                        return;//timeout will cause failure
                    }

                    sb.append((char) i);

                    if (sb.lastIndexOf("IAmAlive") >= 0) {
                        sb = new StringBuffer();
                        appsteminwriter.println("shutdown");
                        appsteminwriter.flush();
                    }

                    if (sb.lastIndexOf("IAmDying") >= 0) {
                        break;
                    }

                    if (sb.lastIndexOf("Exception") >= 0) {
                        break;
                    }

                    if (sb.lastIndexOf(">>>unittest>>>quit") >= 0) {
                        break;
                    }
                }
            }
        }, "Controller");

        controller.start();

        mta.sync(6000);

        appstemoutprint.println(">>>unittest>>>quit");

//        appstemout.close();
        controller.sync(6000);

            /*
            * restore i/o
            */
            System.setOut(savedsystemout);
            System.setIn(savedsystemin);
    }
}
