/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */

package me.yellowbird.cmd;

import me.yellowbird.mta.brain.Brain;
import me.yellowbird.mta.brain.ConfigurationException;
import me.yellowbird.mta.brain.XMLConfiguration;
import me.yellowbird.mta.mps.FileFormatException;
import me.yellowbird.mta.mps.StorageException;
import me.yellowbird.mta.spooler.SpoolerException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;

/**
 * This is an application that is an MTA
 */
public class YellowBird {
    /**
     *
     * @param args --configuration path-to-xml
     * @throws me.yellowbird.mta.brain.ConfigurationException when something is wrong with the configuration.
     * @throws YellowBirdException when the application fails unexpectedly.
     */
    public static void main (String[] args) throws ConfigurationException, YellowBirdException {
        Parameters p = new Parameters(args);

        XMLConfiguration config;

        Brain brain;

        try {
            String s = p.getConfiguration();
            config = new XMLConfiguration(new URL(p.getConfiguration()));

            brain = new Brain(config, new ApplicationStem());
        } catch (FileNotFoundException e) {
            throw new YellowBirdException(e);
        } catch (IOException e) {
            throw new YellowBirdException(e);
        } catch (FileFormatException e) {
            throw new YellowBirdException(e);
        } catch (SpoolerException e) {
            throw new YellowBirdException(e);
        } catch (StorageException e) {
            throw new YellowBirdException(e);
        }

        // Give brain our thread
        Thread.currentThread().setName("Brain");
        brain.run();
    }
}
