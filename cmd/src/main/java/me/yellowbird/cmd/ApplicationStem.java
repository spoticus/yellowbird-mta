/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is YellowBird Mail Transfer Agent.
 *
 * The Initial Developer of the Original Code is
 * Clay Atkins.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * ***** END LICENSE BLOCK *****
 */
package me.yellowbird.cmd;

import me.yellowbird.mta.brain.BrainStem;
import me.yellowbird.mta.brain.NerveMessage;

import java.io.IOException;

/**
 * Brain stem supporting standalone application MTA
 * <p>
 * This brain stem understands that the MTA is an application running in the operating system.
 */
public class ApplicationStem extends BrainStem {

    public ApplicationStem() {
        Thread inthread = new Thread(new Runnable(){
            public void run() {
                try {
                    StringBuffer sb = new StringBuffer();

                    while(true) {
                        int i = System.in.read();

                        System.out.print((char)i);

                        if (i == 0x0d || i == 0x0a) {
                            //process sb
                            if (sb.length() > 0) {
                                interpret(sb.toString());
                                sb = new StringBuffer();
                            }
                        } else {
                            sb.append((char)i);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, "ApplicationStem Command Input");

        inthread.start();
    }

    private void interpret(String cmd) {
        if (cmd.equalsIgnoreCase("shutdown")) {
            try {
                shutdown();
            } catch (InterruptedException e) {
                e.printStackTrace();  
            }
        } else {
            System.out.println("invalid command " + cmd);
        }
    }

    public void sendToNerves(Class<? extends NerveMessage> message) {
        System.out.println(message.toString());
    }

    public void sendToNerves(NerveMessage message) {
        System.out.println(message.getClass().toString() + ":" + message.toString());
    }
}
